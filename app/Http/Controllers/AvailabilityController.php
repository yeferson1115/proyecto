<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Log\LogSistema;
use App\Models\Availability;
use App\Models\User;
use App\Models\Quote;

class AvailabilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $log = new LogSistema();
        $log->user_id = auth()->user()->id;
        $log->tx_descripcion = 'El usuario: '.auth()->user()->display_name.' Ha ingresado a ver la disponibilidad: '
        . date('H:m:i').' del día: '.date('d/m/Y');
        $log->save();

        if(auth()->user()->hasRole('Administrador') ){
            $availability = Availability::with('user')->where('campuse_id',auth()->user()->campus->id)->get();
        }else{
            $availability = Availability::with('user')->where('campuse_id',auth()->user()->campus->id)->where('user_id',auth()->user()->id)->get();

        }

        return view ('admin.availability.index', compact('availability'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $relacionEloquent = 'roles';
        if(auth()->user()->hasRole('Administrador')){
            $usersprofesional = User::where('campuse_id',auth()->user()->campus->id)->whereHas($relacionEloquent, function ($query) {
                return $query->where('name', '=', 'Profesional');
            })->get();

        }else{
            $usersprofesional = User::where('id',auth()->user()->id)->get();
        }


        return view ('admin.availability.create',compact('usersprofesional'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $services = Availability::create(array_merge($request->all(),['campuse_id' => auth()->user()->campus->id]));

        $log = new LogSistema();

        $log->user_id = auth()->user()->id;
        $log->tx_descripcion = 'El usuario: '.auth()->user()->display_name.' Ha guardado una nueva disponibilidad en el sistema: '.$request->name.' a las: '
        . date('H:m:i').' del día: '.date('d/m/Y');
        $log->save();

        return json_encode(['success' => true, 'user_id' => $services->encode_id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $availability = Availability::find(\Hashids::decode($id)[0]);

        $log = new LogSistema();
        $log->user_id = auth()->user()->id;
        $log->tx_descripcion = 'El usuario: '.auth()->user()->display_name.' Ha ingresado a editar los datos de la disponibilidad : '.$availability->id.' a las: '
        . date('H:m:i').' del día: '.date('d/m/Y');
        $log->save();

        $relacionEloquent = 'roles';
        if(auth()->user()->hasRole('Administrador')){
            $usersprofesional = User::where('campuse_id',auth()->user()->campus->id)->whereHas($relacionEloquent, function ($query) {
                return $query->where('name', '=', 'Profesional');
            })->get();

        }else{
            $usersprofesional = User::where('id',auth()->user()->id)->get();
        }

        return view('admin.availability.edit', ['availability' => $availability,'usersprofesional'=>$usersprofesional]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $availability = Availability::find(\Hashids::decode($id)[0]);
        $availability->date = $request->date;
        $availability->user_id = $request->user_id;
        $availability->start_time = $request->start_time;
        $availability->end_time = $request->end_time;


        $availability->save();

        return json_encode(['success' => true, 'availability_id' => $availability->encode_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $availability = Availability::find(\Hashids::decode($id)[0]);
        $availabilitydelete = Availability::find(\Hashids::decode($id)[0])->delete();
        $Quotes = Quote::where('date',$availability['date'])->whereBetween('start_time', [$availability['start_time'], $availability['end_time']])->get();
        foreach($Quotes as $item){
            Quote::find($item['id'])->delete();
        }
        return json_encode(['success' => true]);
    }
}
