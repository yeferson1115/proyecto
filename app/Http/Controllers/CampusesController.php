<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Log\LogSistema;
use App\Models\Campuses;

class CampusesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $log = new LogSistema();
        $log->user_id = auth()->user()->id;
        $log->tx_descripcion = 'El usuario: '.auth()->user()->display_name.' Ha ingresado a ver las sedes: '
        . date('H:m:i').' del día: '.date('d/m/Y');
        $log->save();
        $campuses = Campuses::get();
        return view ('admin.campuses.index', compact('campuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin.campuses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campuses = Campuses::create($request->all());

        $log = new LogSistema();

        $log->user_id = auth()->user()->id;
        $log->tx_descripcion = 'El usuario: '.auth()->user()->display_name.' Ha guardado una nueva sede en el sistema: '.$request->name.' a las: '
        . date('H:m:i').' del día: '.date('d/m/Y');
        $log->save();

        return json_encode(['success' => true, 'user_id' => $campuses->encode_id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Campuses = Campuses::find(\Hashids::decode($id)[0]);

        $log = new LogSistema();
        $log->user_id = auth()->user()->id;
        $log->tx_descripcion = 'El usuario: '.auth()->user()->display_name.' Ha ingresado a editar los datos de la sede: '.$Campuses->display_name.' a las: '
        . date('H:m:i').' del día: '.date('d/m/Y');
        $log->save();
        return view('admin.campuses.edit', ['campuses' => $Campuses]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $campuses = Campuses::find(\Hashids::decode($id)[0]);
        $campuses->name = $request->name;
        $campuses->phone = $request->phone;
        $campuses->email = $request->email;
        $campuses->address = $request->address;
        if($request->token!='' && $request->token!=null && $request->accountid!='' && $request->accountid!=null && $request->number_invoice!='' && $request->number_invoice!=null && $request->prefix!='' && $request->prefix!=null && $request->number_note_credit!='' && $request->number_note_credit!=null){
            $campuses->token=$request->token;
            $campuses->accountid=$request->accountid;
            $campuses->number_invoice=$request->number_invoice;
            $campuses->prefix=$request->prefix;
            $campuses->resolution_number=$request->resolution_number;
            $campuses->number_note_credit=$request->number_note_credit;
        }


        $campuses->save();

        return json_encode(['success' => true, 'campuse_id' => $campuses->encode_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = Campuses::find(\Hashids::decode($id)[0])->delete();
        return json_encode(['success' => true]);
    }
}
