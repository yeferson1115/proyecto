<?php

namespace App\Http\Controllers;

use App\Models\ClinicHistory;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Models\Quote;
use App\Models\Customers;
use App\Models\Departaments;
use App\Models\Cities;
use App\Models\QuoteAnamnesis;
use App\Models\QuoteExtraOralExam;
use App\Models\QuoteDentalExam;
use App\Models\User;

class ClinicHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $histories=ClinicHistory::with('customer','customer.departaments','customer.cities')->get();
        return view ('admin.clinichistory.list', compact('histories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ClinicHistory  $clinicHistory
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $history = ClinicHistory::with('customer','customer.quotes','customer.quotes.profesional','customer.quotes.type','customer.quotes.state','customer.departaments','customer.cities')
        ->with('anamnesis','dentalexam','extraoralexam')->find(\Hashids::decode($id)[0]);
        $depto = new Departaments();
        $deptos=$depto->all();

        $date_current = Carbon::now()->toDateTimeString();

        $prev_date1 = $this->getPrevDate(1);
        $prev_date2 = $this->getPrevDate(2);
        $prev_date3 = $this->getPrevDate(3);
        $prev_date4 = $this->getPrevDate(4);

        //$prev_date12 = $this->getPrevDate(12);

        //dd($prev_date0);
        $emp_count_1  = User::whereBetween('created_at',[$prev_date1,$date_current])->count();
        $emp_count_2  = User::whereBetween('created_at',[$prev_date2,$prev_date1])->count();
        $emp_count_3  = User::whereBetween('created_at',[$prev_date3,$prev_date2])->count();
        $emp_count_4  = User::whereBetween('created_at',[$prev_date4,$prev_date3])->count();


        return view ('admin.clinichistory_show.index', compact('history','deptos','emp_count_1',
        'emp_count_2',
        'emp_count_3',
        'emp_count_4'));    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ClinicHistory  $clinicHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(ClinicHistory $clinicHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ClinicHistory  $clinicHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClinicHistory $clinicHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ClinicHistory  $clinicHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClinicHistory $clinicHistory)
    {
        //
    }
    
    public function byidcustomer($id){
        $id=\Hashids::decode($id)[0];
        /**Consultamos Cita */
        $quote = Quote::find($id);   
        /**Consultamos Cliente */    
        $customer = Customers::find($quote->customer_id);
        /**Consultamos Historia clinica */
        $history=ClinicHistory::where('customer_id',$quote->customer_id)->first();
        if($history===null){
            ClinicHistory::create([
                'customer_id' => $quote->customer_id,
                
            ]);
            $history=ClinicHistory::where('customer_id',$quote->customer_id)->first();
        }
        /**Consultamos anamnesis */
        $anamnesis=QuoteAnamnesis::where('quote_id',$quote->id)->first();
        /**Sino existe se crea uno base */
        if($anamnesis===null){
            QuoteAnamnesis::create([
                'campuse_id' => auth()->user()->campus->id,
                'profesional_id'=>auth()->user()->id,
                'customer_id'=>$quote->customer_id,
                'quote_id'=>$quote->id,
                'clinichistory_id'=>$history->id
            ]);
            $anamnesis=QuoteAnamnesis::where('quote_id',$quote->id)->first();
        }
        /**Consultamos examen extraoral */
        $extraoral=QuoteExtraOralExam::where('quote_id',$quote->id)->first();
         /**Sino existe se crea uno base */
         if($extraoral===null){
            QuoteExtraOralExam::create([
                'campuse_id' => auth()->user()->campus->id,
                'profesional_id'=>auth()->user()->id,
                'customer_id'=>$quote->customer_id,
                'quote_id'=>$quote->id,
                'clinichistory_id'=>$history->id
            ]);
            $extraoral=QuoteExtraOralExam::where('quote_id',$quote->id)->first();
        }
        /**Consultamos examen dental */
        $dentalexam=QuoteDentalExam::where('quote_id',$quote->id)->first();
         /**Sino existe se crea uno base */
         if($dentalexam===null){
            QuoteDentalExam::create([
                'campuse_id' => auth()->user()->campus->id,
                'profesional_id'=>auth()->user()->id,
                'customer_id'=>$quote->customer_id,
                'quote_id'=>$quote->id,
                'clinichistory_id'=>$history->id
            ]);
            $dentalexam=QuoteDentalExam::where('quote_id',$quote->id)->first();
        }

        
        /**Se consulta departamentos */
        $depto = new Departaments();
        $deptos=$depto->all();

        $date_current = Carbon::now()->toDateTimeString();

        $prev_date1 = $this->getPrevDate(1);
        $prev_date2 = $this->getPrevDate(2);
        $prev_date3 = $this->getPrevDate(3);
        $prev_date4 = $this->getPrevDate(4);

        //$prev_date12 = $this->getPrevDate(12);

        //dd($prev_date0);
        $emp_count_1  = User::whereBetween('created_at',[$prev_date1,$date_current])->count();
        $emp_count_2  = User::whereBetween('created_at',[$prev_date2,$prev_date1])->count();
        $emp_count_3  = User::whereBetween('created_at',[$prev_date3,$prev_date2])->count();
        $emp_count_4  = User::whereBetween('created_at',[$prev_date4,$prev_date3])->count();


        return view ('admin.clinichistory.index', compact('customer','deptos','quote','anamnesis','extraoral','dentalexam','emp_count_1',
        'emp_count_2',
        'emp_count_3',
        'emp_count_4'));       

    }

    private function getPrevDate($num){
        return Carbon::now()->subMonths($num)->toDateTimeString();
    }
}
