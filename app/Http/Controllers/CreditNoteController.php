<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Models\Log\LogSistema;
use App\Models\CreditNote;
use App\Models\CreditNote_Items;

use Illuminate\Support\Facades\DB;
use App\Models\Invoice;


use App\Models\Departaments;
use App\Models\Cities;
use App\Models\Customers;
use App\Models\Services;
use App\Models\Invoice_Items;

use App\Models\Campuses;



use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;

class CreditNoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $log = new LogSistema();
        $log->user_id = auth()->user()->id;
        $log->tx_descripcion = 'El usuario: '.auth()->user()->display_name.' Ha ingresado a ver las notas creditos: '
        . date('H:m:i').' del día: '.date('d/m/Y');
        $log->save();
        $sql = "SELECT i.*,c.first_name,c.last_name, c.identification,c.phone FROM note_credit i
         inner join customers c on i.customer_id=c.id";
        $invoices = DB::select($sql);
        return view ('admin.notacredit.index', compact('invoices'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $log = new LogSistema();
        $log->user_id = auth()->user()->id;
        $log->tx_descripcion = 'El usuario: '.auth()->user()->display_name.' Ha ingresado a crear una factura: '
        . date('H:m:i').' del día: '.date('d/m/Y');
        $log->save();
        $customers = Customers::get();
        $services = Services::where('campuse_id',auth()->user()->campus->id)->get();

        $depto = new Departaments();
        $deptos=$depto->all();
        $invoices=Invoice::where('campuse_id',auth()->user()->campus->id)->get();
        return view ('admin.notacredit.create',compact('invoices','services','deptos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->input();
        $invoice = Invoice::with('items','items.service')->where('id', $data['idinvoice'])->get();
        $campus = Campuses::find(auth()->user()->campus->id);
         /**Consultamos si la sede tiene datos de conexion a daitaco */
         if($campus['token']==null || $campus['token']==''){
            return json_encode(['success' => false,'error' => 'Debes Configurar el Token de Dataico en la sede']);
         }
         if($campus['accountid']==null || $campus['accountid']==''){
            return json_encode(['success' => false,'error' => 'Debes Configurar el Id de cuenta de Dataico en la sede']);
        }
        if($campus['number_note_credit']==null || $campus['number_note_credit']==''){
            return json_encode(['success' => false,'error' => 'Debes Configurar el número actual de nota crédito de Dataico en la sede']);
        }

        $CreditNote = new CreditNote;
        $data = $request->input();

        $items=array();

        $CreditNote->issue_date = $invoice[0]->issue_date;
        $CreditNote->payment_date = $invoice[0]->payment_date;
        $CreditNote->order_reference = $invoice[0]->order_reference;
        $CreditNote->invoice_type_code = $invoice[0]->invoice_type_code;
        $CreditNote->payment_means = $invoice[0]->payment_means;
        $CreditNote->payment_means_type = $invoice[0]->payment_means_type;
        $CreditNote->customer_id = $invoice[0]->customer_id;
        $CreditNote->campuse_id=$invoice[0]->campuse_id;
        $CreditNote->pdf_url=null;
        $CreditNote->number_invoice=$invoice[0]->number_invoice;
        $CreditNote->dian_status=null;
        $CreditNote->email_status=null;
        $CreditNote->uuid=$invoice[0]->uuid;
		$CreditNote->save();
        $CreditNote->id;
        $services=$data['idservice'];
        $subtotal=0;
        $total=0;
        $descuento=0;
        $retencion=0;
        $impuesto=0;
        foreach($services as $key=>$item){
            $service = Services::find($item);
            if($service!=null){

                    $taxes = explode("-", $data['taxes'][$key]);
                    $CreditNote_Items = new CreditNote_Items;
                    $CreditNote_Items->sku = $service['sku'];
                    $CreditNote_Items->quantity = $data['quantity'][$key];
                    $CreditNote_Items->description = $data['description'][$key];
                    $CreditNote_Items->price = $data['price'][$key];
                    $CreditNote_Items->discount_rate = $data['discount'][$key];
                    $CreditNote_Items->note_credit_id =  $CreditNote->id;
                    $CreditNote_Items->service_id = $service['id'];
                    $CreditNote_Items->tax_rate = $taxes[1];
                    $CreditNote_Items->type_tax = $taxes[0];
                    $CreditNote_Items->ret_fuente = $data['ret_fuente'][$key];
                    $CreditNote_Items->save();
                    $CreditNote_Items->id;





                $sub=$data['price'][$key]*$data['quantity'][$key];
                $desc=($sub*$data['discount'][$key]);
                if($desc>0){
                    $desctotal=$desc/100;
                }else{
                    $desctotal=$desc;
                }
                $sub1=($data['price'][$key]*$data['quantity'][$key])-$desctotal;
                $ret=($sub1*$data['ret_fuente'][$key]);
                if($ret>0){
                    $rettotal=$ret/100;
                }else{
                    $rettotal=$ret;
                }
                $taxesc=($sub1*$taxes[1]);
                if($taxesc>0){
                    $taxetotal=$taxesc/100;
                }else{
                    $taxetotal=$taxesc;
                }
                $total=((($total+$sub)-$desctotal)-$rettotal)+$taxetotal;
                $retencion=$retencion+$rettotal;
                $impuesto=$impuesto+$taxetotal;




                if($taxes[1]=='0'){
                    $taxesimpuesto=array();
                }else{

                    $taxesimpuesto=array(
                        array(
                            'tax_category' => $taxes[0],
                            'tax_rate' => floatval($taxes[1])
                        )
                    );
                }

                if($data['ret_fuente'][$key]=='0'){
                    $retfuente=array();
                }else{
                    $retfuente=array(
                        array(
                            'tax_category' => 'RET_FUENTE',
                            'tax_rate' => floatval($data['ret_fuente'][$key])
                        )
                    );
                }

                $itemnew=array(
                    'sku' => $service['sku'],
                    'quantity' => (int)$data['quantity'][$key],
                    'description' => $data['description'][$key],
                    //'measuring_unit' =>'94',
                    'price' => (int)$data['price'][$key],
                    //'discount_rate' => $data['discount'][$key],
                    'taxes' => $taxesimpuesto,
                    'retentions' => $retfuente
                );
                if($data['discount'][$key]>'0'){
                    $itemnew['discount_rate']=(int)$data['discount'][$key];
                }
                array_push($items,$itemnew);
            }

        }
        $CreditNote = CreditNote::find($CreditNote->id);
        $CreditNote->subtotal = $subtotal;
        $CreditNote->discount = $descuento;
        $CreditNote->ret_fuente=$retencion;
        $CreditNote->taxe=$impuesto;
        $CreditNote->total = $total;
        $CreditNote->save();

        $number=$campus['number_note_credit']+1;


            $response = Http::accept('application/json')->withHeaders([
                'Content-Type'=>'application/json',
                'auth-token' => $campus['token']
            ])->post('https://api.dataico.com/direct/dataico_api/v2/credit_notes', [
                'actions' => array(
                    'send_dian' =>false,
                    'send_email' =>false
                ),
                'credit_note' => array(
                        'env' => 'PRODUCCION',
                        'dataico_account_id' => $campus['accountid'],
                        'invoice_id'=> $invoice[0]->uuid,
                        'issue_date' => date('d/m/Y'),
                        "reason"=>$data['reason'],
                        'number' =>"$number",
                        'numbering' => array(
                            'prefix' => $campus['prefix'],
                            'flexible' => false
                         ),
                        'items' => $items,
                        'charges'=>array()
                   )
            ])->json();
            if(isset($response['errors'])){



                //eliminartodo
                CreditNote::find($CreditNote->id)->delete();
                CreditNote_Items::where('note_credit_id',$CreditNote->id)->delete();

                return json_encode(['success' => false,'error' => $response['errors'][0]['error']]);
            }else{
                $campus->number_note_credit=$campus['number_note_credit']+1;
                $campus->save();

                $CreditNoteupdate = CreditNote::find($CreditNote->id);
                $CreditNoteupdate->pdf_url = $response['pdf_url'];
                $CreditNoteupdate->number_invoice=$response['number'];
                $CreditNoteupdate->dian_status=$response['dian_status'];
                $CreditNoteupdate->uuid=$response['uuid'];
                $CreditNoteupdate->save();
                $factura=$response['pdf_url'];
                if($data['reason']=='DEVOLUCION' || $data['reason']=='ANULACION'){
                    $invoiceupdate = Invoice::find($data['idinvoice']);
                    $invoiceupdate->state = 0;
                    $invoiceupdate->save();
                }

            }


            $factura=null;



        $log = new LogSistema();

        $log->user_id = auth()->user()->id;
        $log->tx_descripcion = 'El usuario: '.auth()->user()->display_name.' Ha guardado una nueva factura con id: '.$CreditNote->id.' a las: '
        . date('H:m:i').' del día: '.date('d/m/Y');
        $log->save();

        return json_encode(['success' => true, 'invoce' => $CreditNote->id,'pdf' => $factura]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CreditNote  $creditNote
     * @return \Illuminate\Http\Response
     */
    public function show(CreditNote $creditNote)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CreditNote  $creditNote
     * @return \Illuminate\Http\Response
     */
    public function edit(CreditNote $creditNote)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCreditNoteRequest  $request
     * @param  \App\Models\CreditNote  $creditNote
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCreditNoteRequest $request, CreditNote $creditNote)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CreditNote  $creditNote
     * @return \Illuminate\Http\Response
     */
    public function destroy(CreditNote $creditNote)
    {
        //
    }

    public function getInvoiceById($idinvoice)
    {

        $invoice = Invoice::with('items','items.service')->where('id', $idinvoice)->get();


        return with(["invoice" => $invoice]);
    }
}
