<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Log\LogSistema;
use App\Models\Customers;
use App\Models\Services;
use App\Models\Invoice;
use App\Models\Invoice_Items;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Models\Departaments;
use App\Models\Cities;
use App\Models\Campuses;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $log = new LogSistema();
        $log->user_id = auth()->user()->id;
        $log->tx_descripcion = 'El usuario: '.auth()->user()->display_name.' Ha ingresado a ver las facturas: '
        . date('H:m:i').' del día: '.date('d/m/Y');
        $log->save();
        $sql = "SELECT i.*,c.first_name,c.last_name, c.identification,c.phone FROM invoices i
         inner join customers c on i.customer_id=c.id";
        $invoices = DB::select($sql);
        return view ('admin.invoice.index', compact('invoices'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $log = new LogSistema();
        $log->user_id = auth()->user()->id;
        $log->tx_descripcion = 'El usuario: '.auth()->user()->display_name.' Ha ingresado a crear una factura: '
        . date('H:m:i').' del día: '.date('d/m/Y');
        $log->save();
        $customers = Customers::get();
        $services = Services::where('campuse_id',auth()->user()->campus->id)->get();
        $depto = new Departaments();
        $deptos=$depto->all();
        return view ('admin.invoice.create',compact('customers','services','deptos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campus = Campuses::find(auth()->user()->campus->id);


        $idlatest = Invoice::latest('id')->first();
        if($idlatest==null){
            $ref=Str::random(7).'-1';
        }else{
            $ultimoid=$idlatest['id']+1;
            $ref=Str::random(7).'-'.$ultimoid;
        }


        $data = $request->input();
        $customer = Customers::with('departaments')->with('cities')->find($data['customer']);

        if($customer['company_name']==null || $customer['company_name']==''){
            $company_name=$customer['first_name'].'_'.$customer['last_name'];
        }else{
            $company_name=$customer['company_name'];
        }
        $invoce = new Invoice;
        $date = date('Y-m-d');
        $datetime = date('Y-m-d H:i:s');

        $items=array();

        $invoce->issue_date = $date;
        $invoce->payment_date = $datetime;
        $invoce->order_reference = null;
        $invoce->invoice_type_code = "FACTURA_VENTA";
        $invoce->payment_means = $data['payment_means'];
        $invoce->payment_means_type = $data['payment_means_type'];
        $invoce->customer_id = $data['customer'];
        $invoce->campuse_id=auth()->user()->campus->id;
        $invoce->pdf_url=null;
        $invoce->number_invoice=null;
        $invoce->dian_status=null;
        $invoce->email_status=null;
        $invoce->uuid=null;
		$invoce->save();
        $invoce->id;
        $services=$data['idservice'];
        $subtotal=0;
        $total=0;
        $descuento=0;
        $retencion=0;
        $impuesto=0;
        foreach($services as $key=>$item){
            $service = Services::find($item);
            if($service!=null){

                $taxes = explode("-", $data['taxes'][$key]);


                $invoice_items = new Invoice_Items;
                $invoice_items->sku = $service['sku'];
                $invoice_items->quantity = $data['quantity'][$key];
                $invoice_items->description = $data['description'][$key];
                $invoice_items->price = $data['price'][$key];
                $invoice_items->discount_rate = $data['discount'][$key];
                $invoice_items->invoice_id = $invoce->id;
                $invoice_items->service_id = $service['id'];
                $invoice_items->tax_rate = $taxes[1];
                $invoice_items->type_tax = $taxes[0];
                $invoice_items->ret_fuente = $data['ret_fuente'][$key];
                $invoice_items->save();
                $invoice_items->id;
                $subtotal=$subtotal+($data['price'][$key]*$data['quantity'][$key]);
                $descuento=$descuento+((($data['price'][$key]*$data['quantity'][$key])*$data['discount'][$key])/100);


                $sub=$data['price'][$key]*$data['quantity'][$key];
                $desc=($sub*$data['discount'][$key]);
                if($desc>0){
                    $desctotal=$desc/100;
                }else{
                    $desctotal=$desc;
                }
                $sub1=($data['price'][$key]*$data['quantity'][$key])-$desctotal;
                $ret=($sub1*$data['ret_fuente'][$key]);
                if($ret>0){
                    $rettotal=$ret/100;
                }else{
                    $rettotal=$ret;
                }
                $taxesc=($sub1*$taxes[1]);
                if($taxesc>0){
                    $taxetotal=$taxesc/100;
                }else{
                    $taxetotal=$taxesc;
                }
                $total=((($total+$sub)-$desctotal)-$rettotal)+$taxetotal;
                $retencion=$retencion+$rettotal;
                $impuesto=$impuesto+$taxetotal;




                if($taxes[1]=='0'){
                    $taxesimpuesto=array();
                }else{

                    $taxesimpuesto=array(
                        array(
                            'tax_category' => $taxes[0],
                            'tax_rate' => $taxes[1]
                        )
                    );
                }

                if($data['ret_fuente'][$key]=='0'){
                    $retfuente=array();
                }else{
                    $retfuente=array(
                        array(
                            'tax_category' => 'RET_FUENTE',
                            'tax_rate' => $data['ret_fuente'][$key]
                        )
                    );
                }

                $itemnew=array(
                    'sku' => $service['sku'],
                    'quantity' => $data['quantity'][$key],
                    'description' => $data['description'][$key],
                    //'measuring_unit' =>'94',
                    'price' => $data['price'][$key],
                    //'discount_rate' => $data['discount'][$key],
                    'taxes' => $taxesimpuesto,
                    'retentions' => $retfuente
                );
                if($data['discount'][$key]>'0'){
                    $itemnew['discount_rate']=$data['discount'][$key];
                }
                array_push($items,$itemnew);
            }

        }
        $invoiceupdate = Invoice::find($invoce->id);
        $invoiceupdate->subtotal = $subtotal;
        $invoiceupdate->discount = $descuento;
        $invoiceupdate->ret_fuente=$retencion;
        $invoiceupdate->taxe=$impuesto;
        $invoiceupdate->order_reference ='C-'.auth()->user()->campus->id.'-I-'.$invoce->id;
        $invoiceupdate->total = $total;
        $invoiceupdate->save();

        /**Consultamos si la sede tiene datos de conexion a daitaco */
        if($campus['token']!=null && $campus['token']!='' && $campus['accountid']!=null && $campus['accountid']!=''){

            $response = Http::accept('application/json')->withHeaders([
                'Content-Type'=>'application/json',
                'auth-token' => $campus['token']
            ])->post('https://api.dataico.com/direct/dataico_api/v2/invoices', [
                'actions' => array(
                        'send_dian' =>false,
                        'send_email' =>false
                ),
                'invoice' => array(
                        'env' => 'PRODUCCION',
                        'dataico_account_id' => $campus['accountid'],
                        'number' => $campus['number_invoice']+1,
                        'issue_date' => date('d/m/Y H:i:s'),
                        //'payment_date' => '23/09/2022 13:22:43',
                        'order_reference' => 'C-'.auth()->user()->campus->id.'-I-'.$invoce->id,
                        'invoice_type_code' => 'FACTURA_VENTA',
                        'payment_means' => $data['payment_means'],
                        'payment_means_type' => $data['payment_means_type'],
                        'numbering' => array(
                            'resolution_number' =>$campus['resolution_number'],
                            'prefix' => $campus['prefix'],
                            'flexible' => false
                         ),
                        'notes' => array(),
                        'customer' => array(
                            'email' => $customer['email'],
                            'phone' =>  $customer['phone'],
                            'party_identification_type' => $customer['identification_type'],
                            'party_identification' => $customer['identification'],
                            'party_type' => $customer['type'],
                            'tax_level_code' => 'COMUN',
                            'regimen' => $customer['regimen'],
                            'department' => strtoupper($customer['departaments']['name']),
                            'city' => strtoupper($customer['cities']['name']),
                            'address_line' => $customer['address_line'],
                            'country_code' => 'CO',
                            'company_name' => $company_name,
                            'first_name' => $customer['first_name'],
                            'family_name' => $customer['last_name']
                        ),
                        'items' => $items
                   )
            ])->json();
            if(isset($response['errors'])){
                //eliminartodo
                Invoice::find($invoce->id)->delete();
                Invoice_Items::where('invoice_id',$invoce->id)->delete();
                return json_encode(['success' => false,'error' => $response['errors'][0]['error']]);
            }else{
                $campus->number_invoice=$campus['number_invoice']+1;
                $campus->save();

                $invoiceupdate = Invoice::find($invoce->id);
                $invoiceupdate->pdf_url = $response['pdf_url'];
                $invoiceupdate->number_invoice=$response['number'];
                $invoiceupdate->dian_status=$response['dian_status'];
                $invoiceupdate->email_status=$response['email_status'];
                $invoiceupdate->uuid=$response['uuid'];

                $invoiceupdate->save();
                $factura=$response['pdf_url'];

            }

        }else{
            $factura=null;
        }


        $log = new LogSistema();

        $log->user_id = auth()->user()->id;
        $log->tx_descripcion = 'El usuario: '.auth()->user()->display_name.' Ha guardado una nueva factura con id: '.$invoce->id.' a las: '
        . date('H:m:i').' del día: '.date('d/m/Y');
        $log->save();

        return json_encode(['success' => true, 'invoce' => $invoce->id,'pdf' => $factura]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        dd($id);
        //$user = User::find(\Hashids::decode($id)[0])->delete();

        return json_encode(['success' => true]);
    }

    public function getcustomer($idcustomer)
    {

        $customer = Customers::find($idcustomer);

        return with(["customer" => $customer]);
    }

    public function getservice($idservice)
    {
        $service = Services::find($idservice);
        return with(["service" => $service]);
    }

    public function cities($departament)
    {
        $Departaments = Departaments::find($departament);

        $cities = Cities::where('code_departament', $Departaments['code'])->get();

        return with(["cities" => $cities]);
    }
}
