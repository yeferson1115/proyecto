<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Log\LogSistema;
use App\Models\Customers;
use App\Models\Services;
use App\Models\Pos;
use App\Models\Pos_Items;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Models\Departaments;
use App\Models\Cities;
use App\Models\Campuses;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;

use Barryvdh\DomPDF\Facade as PDF;

class PosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*$data = array(
            'url'=>public_path('images/logo/logo.png'),
            'namecampuse'=>auth()->user()->campus->name
        );
       return view ('admin.pos.tirilla',compact('data'));*/

        $log = new LogSistema();
        $log->user_id = auth()->user()->id;
        $log->tx_descripcion = 'El usuario: '.auth()->user()->display_name.' Ha ingresado a ver las facturas: '
        . date('H:m:i').' del día: '.date('d/m/Y');
        $log->save();
        $sql = "SELECT i.*,c.first_name,c.last_name, c.identification,c.phone FROM pos i
         inner join customers c on i.customer_id=c.id order by i.payment_date DESC";
        $pos = DB::select($sql);
        return view ('admin.pos.index', compact('pos'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $log = new LogSistema();
        $log->user_id = auth()->user()->id;
        $log->tx_descripcion = 'El usuario: '.auth()->user()->display_name.' Ha ingresado a crear una factura POS: '
        . date('H:m:i').' del día: '.date('d/m/Y');
        $log->save();
        $customers = Customers::get();
        $services = Services::where('campuse_id',auth()->user()->campus->id)->get();
        $depto = new Departaments();
        $deptos=$depto->all();
        return view ('admin.pos.create',compact('customers','services','deptos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $campus = Campuses::find(auth()->user()->campus->id);


        $idlatest = Pos::latest('id')->first();
        if($idlatest==null){
            $ref=Str::random(7).'-1';
        }else{
            $ultimoid=$idlatest['id']+1;
            $ref=Str::random(7).'-'.$ultimoid;
        }


        $data = $request->input();
        $customer = Customers::with('departaments')->with('cities')->find($data['customer']);

        if($customer['company_name']==null || $customer['company_name']==''){
            $company_name=$customer['first_name'].'_'.$customer['last_name'];
        }else{
            $company_name=$customer['company_name'];
        }
        $invoce = new Pos;
        $date = date('Y-m-d');
        $datetime = date('Y-m-d H:i:s');

        $items=array();

        $invoce->issue_date = $date;
        $invoce->payment_date = $datetime;
        $invoce->order_reference = null;
        $invoce->invoice_type_code = "FACTURA_VENTA";
        $invoce->payment_means = $data['payment_means'];
        $invoce->payment_means_type = $data['payment_means_type'];
        $invoce->customer_id = $data['customer'];
        $invoce->campuse_id=auth()->user()->campus->id;
        $invoce->pdf_url=null;
        $invoce->state=1;
		$invoce->save();
        $invoce->id;
        $services=$data['idservice'];
        $subtotal=0;
        $total=0;
        $descuento=0;
        $retencion=0;
        $impuesto=0;
        foreach($services as $key=>$item){
            $service = Services::find($item);
            if($service!=null){

                $taxes = explode("-", $data['taxes'][$key]);


                $Pos_items = new Pos_Items;
                $Pos_items->sku = $service['sku'];
                $Pos_items->quantity = $data['quantity'][$key];
                $Pos_items->description = $data['description'][$key];
                $Pos_items->price = $data['price'][$key];
                $Pos_items->discount = $data['discount'][$key];
                $Pos_items->pos_id = $invoce->id;
                $Pos_items->service_id = $service['id'];
                $Pos_items->tax_rate = $taxes[1];
                $Pos_items->type_tax = $taxes[0];
                $Pos_items->ret_fuente = $data['ret_fuente'][$key];
                $Pos_items->save();
                $Pos_items->id;
                $subtotal=$subtotal+($data['price'][$key]*$data['quantity'][$key]);
                $descuento=$descuento+((($data['price'][$key]*$data['quantity'][$key])*$data['discount'][$key])/100);


                $sub=$data['price'][$key]*$data['quantity'][$key];
                $desc=($sub*$data['discount'][$key]);
                if($desc>0){
                    $desctotal=$desc/100;
                }else{
                    $desctotal=$desc;
                }
                $sub1=($data['price'][$key]*$data['quantity'][$key])-$desctotal;
                $ret=($sub1*$data['ret_fuente'][$key]);
                if($ret>0){
                    $rettotal=$ret/100;
                }else{
                    $rettotal=$ret;
                }
                $taxesc=($sub1*$taxes[1]);
                if($taxesc>0){
                    $taxetotal=$taxesc/100;
                }else{
                    $taxetotal=$taxesc;
                }
                $total=((($total+$sub)-$desctotal)-$rettotal)+$taxetotal;
                $retencion=$retencion+$rettotal;
                $impuesto=$impuesto+$taxetotal;




                if($taxes[1]=='0'){
                    $taxesimpuesto=array();
                }else{

                    $taxesimpuesto=array(
                        array(
                            'tax_category' => $taxes[0],
                            'tax_rate' => $taxes[1]
                        )
                    );
                }

                if($data['ret_fuente'][$key]=='0'){
                    $retfuente=array();
                }else{
                    $retfuente=array(
                        array(
                            'tax_category' => 'RET_FUENTE',
                            'tax_rate' => $data['ret_fuente'][$key]
                        )
                    );
                }

                $itemnew=array(
                    'sku' => $service['sku'],
                    'quantity' => $data['quantity'][$key],
                    'description' => $data['description'][$key],
                    //'measuring_unit' =>'94',
                    'price' => $data['price'][$key],
                    //'discount_rate' => $data['discount'][$key],
                    'taxes' => $taxesimpuesto,
                    'retentions' => $retfuente
                );

                array_push($items,$itemnew);
            }

        }
        $Posupdate = Pos::find($invoce->id);
        $Posupdate->subtotal = $subtotal;
        $Posupdate->discount = $descuento;
        $Posupdate->ret_fuente=$retencion;
        $Posupdate->taxe=$impuesto;
        $Posupdate->order_reference ='C'.auth()->user()->campus->id.'I'.$invoce->id;
        $Posupdate->total = $total;
        $Posupdate->save();

        $referencia='C'.auth()->user()->campus->id.'I'.$invoce->id;

        switch ($data['payment_means']) {
            case "CREDIT_CARD":
                $payment_means='Tarjeta Crédito';
                break;
            case "DEBIT_CARD":
                $payment_means='Tarjeta Débito';
                break;
            case "CASH":
                $payment_means='Efectivo';
                break;
            case "CREDIT_ACH":
                $payment_means='Crédito ACH';
                break;
            case "DEBIT_ACH":
                $payment_means='Débito ACH';
                break;
            case "CHEQUE":
                $payment_means='Cheque';
                break;
            case "CREDIT_TRANSFER":
                $payment_means='Transferencia Crédito';
                break;
            case "DEBIT_TRANSFER":
                $payment_means='Transferencia Débito';
                break;
        }
        $customPaper = array(0,0,567.00,185.80);
        $data = array(
            'url'=>public_path('images/logo/logo.png'),
            'namecampuse'=>auth()->user()->campus->name,
            'nit'=>'445555-5',
            'phone'=>auth()->user()->campus->phone,
            'email'=>auth()->user()->campus->email,
            'address'=>auth()->user()->campus->address,
            'order_reference'=>'C'.auth()->user()->campus->id.'I'.$invoce->id,
            'date_creation'=>$datetime,
            'payment_means'=> $payment_means,
            'payment_means_type'=>$data['payment_means_type'],
            'customer'=>$customer['first_name'].' '.$customer['last_name'],
            'identification'=>$customer['identification'],
            'email_customer'=>$customer['email'],
            'address_line'=>$customer['address_line'],
            'items'=>$items,
            'subtotal'=>$subtotal,
            'descuento'=>$descuento,
            'impuesto'=>$impuesto,
            'total'=>$total,
        );
        $pdf = PDF::loadView('admin.pos.tirilla',compact('data'))->setPaper($customPaper, 'landscape')
        ->save(public_path('facturas/factura'.$referencia.'.pdf'));


        $factura=$request->root().'/facturas/factura'.$referencia.'.pdf';

        $invoiceupdate = Pos::find($invoce->id);
        $invoiceupdate->pdf_url = $factura;
        $invoiceupdate->save();



        $log = new LogSistema();

        $log->user_id = auth()->user()->id;
        $log->tx_descripcion = 'El usuario: '.auth()->user()->display_name.' Ha guardado una nueva factura con id: '.$invoce->id.' a las: '
        . date('H:m:i').' del día: '.date('d/m/Y');
        $log->save();

        return json_encode(['success' => true, 'invoce' => $invoce->id,'pdf' => $factura]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


        $pos = Pos::find(\Hashids::decode($id)[0]);
        $pos->state = 0;

        $pos->save();

        return json_encode(['success' => true]);


    }

    public function getcustomer($idcustomer)
    {

        $customer = Customers::find($idcustomer);

        return with(["customer" => $customer]);
    }

    public function getservice($idservice)
    {
        $service = Services::find($idservice);
        return with(["service" => $service]);
    }

    public function cities($departament)
    {
        $Departaments = Departaments::find($departament);

        $cities = Cities::where('code_departament', $Departaments['code'])->get();

        return with(["cities" => $cities]);
    }
}
