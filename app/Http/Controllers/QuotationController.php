<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Log\LogSistema;
use App\Models\Quotation;
use App\Models\Quotation_Items;
use App\Models\Customers;
use App\Models\Services;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Models\Departaments;
use App\Models\Cities;
use App\Models\Campuses;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;

use Barryvdh\DomPDF\Facade as PDF;

class QuotationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $log = new LogSistema();
        $log->user_id = auth()->user()->id;
        $log->tx_descripcion = 'El usuario: '.auth()->user()->display_name.' Ha ingresado a ver las cotizaciones: '
        . date('H:m:i').' del día: '.date('d/m/Y');
        $log->save();
        $sql = "SELECT q.*,c.first_name,c.last_name, c.identification,c.phone FROM quotation q
         inner join customers c on q.customer_id=c.id order by q.payment_date DESC";
        $quotation = DB::select($sql);
        return view ('admin.quotation.index', compact('quotation'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $log = new LogSistema();
        $log->user_id = auth()->user()->id;
        $log->tx_descripcion = 'El usuario: '.auth()->user()->display_name.' Ha ingresado a crear una cotización: '
        . date('H:m:i').' del día: '.date('d/m/Y');
        $log->save();
        $customers = Customers::get();
        $services = Services::where('campuse_id',auth()->user()->campus->id)->get();
        $depto = new Departaments();
        $deptos=$depto->all();
        return view ('admin.quotation.create',compact('customers','services','deptos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campus = Campuses::find(auth()->user()->campus->id);


        $idlatest = Quotation::latest('id')->first();
       
        if($idlatest==null){
            $ref=Str::random(7).'-1';
        }else{
            $ultimoid=$idlatest['id']+1;
            $ref=Str::random(7).'-'.$ultimoid;
        }


        $data = $request->input();
        $customer = Customers::with('departaments')->with('cities')->find($data['customer']);

        if($customer['company_name']==null || $customer['company_name']==''){
            $company_name=$customer['first_name'].'_'.$customer['last_name'];
        }else{
            $company_name=$customer['company_name'];
        }
        $quotation = new Quotation;
        $date = date('Y-m-d');
        $datetime = date('Y-m-d H:i:s');

        $items=array();

        $quotation->issue_date = $date;
        $quotation->payment_date = $datetime;
        $quotation->order_reference = null;
        $quotation->invoice_type_code = "COTIZACION";
        $quotation->payment_means = $data['payment_means'];
        $quotation->payment_means_type = $data['payment_means_type'];
        $quotation->customer_id = $data['customer'];
        $quotation->campuse_id=auth()->user()->campus->id;
        $quotation->pdf_url=null;
        $quotation->state=1;
		$quotation->save();
        $quotation->id;
        $services=$data['idservice'];
        $subtotal=0;
        $total=0;
        $descuento=0;
        $retencion=0;
        $impuesto=0;
        foreach($services as $key=>$item){
            $service = Services::find($item);
            if($service!=null){

                $taxes = explode("-", $data['taxes'][$key]);


                $Quotation_items = new Quotation_Items;
                $Quotation_items->sku = $service['sku'];
                $Quotation_items->quantity = $data['quantity'][$key];
                $Quotation_items->description = $data['description'][$key];
                $Quotation_items->price = $data['price'][$key];
                $Quotation_items->discount = $data['discount'][$key];
                $Quotation_items->quotation_id = $quotation->id;
                $Quotation_items->service_id = $service['id'];
                $Quotation_items->tax_rate = $taxes[1];
                $Quotation_items->type_tax = $taxes[0];
                $Quotation_items->ret_fuente = $data['ret_fuente'][$key];
                $Quotation_items->save();
                $Quotation_items->id;
                $subtotal=$subtotal+($data['price'][$key]*$data['quantity'][$key]);
                $descuento=$descuento+((($data['price'][$key]*$data['quantity'][$key])*$data['discount'][$key])/100);


                $sub=$data['price'][$key]*$data['quantity'][$key];
                $desc=($sub*$data['discount'][$key]);
                if($desc>0){
                    $desctotal=$desc/100;
                }else{
                    $desctotal=$desc;
                }
                $sub1=($data['price'][$key]*$data['quantity'][$key])-$desctotal;
                $ret=($sub1*$data['ret_fuente'][$key]);
                if($ret>0){
                    $rettotal=$ret/100;
                }else{
                    $rettotal=$ret;
                }
                $taxesc=($sub1*$taxes[1]);
                if($taxesc>0){
                    $taxetotal=$taxesc/100;
                }else{
                    $taxetotal=$taxesc;
                }
                $total=((($total+$sub)-$desctotal)-$rettotal)+$taxetotal;
                $retencion=$retencion+$rettotal;
                $impuesto=$impuesto+$taxetotal;




                if($taxes[1]=='0'){
                    $taxesimpuesto=array();
                }else{

                    $taxesimpuesto=array(
                        array(
                            'tax_category' => $taxes[0],
                            'tax_rate' => $taxes[1]
                        )
                    );
                }

                if($data['ret_fuente'][$key]=='0'){
                    $retfuente=array();
                }else{
                    $retfuente=array(
                        array(
                            'tax_category' => 'RET_FUENTE',
                            'tax_rate' => $data['ret_fuente'][$key]
                        )
                    );
                }

                $itemnew=array(
                    'sku' => $service['sku'],
                    'quantity' => $data['quantity'][$key],
                    'description' => $data['description'][$key],
                    //'measuring_unit' =>'94',
                    'price' => $data['price'][$key],
                    //'discount_rate' => $data['discount'][$key],
                    'taxes' => $taxesimpuesto,
                    'retentions' => $retfuente
                );

                array_push($items,$itemnew);
            }

        }
        $Quotationupdate = Quotation::find($quotation->id);
        $Quotationupdate->subtotal = $subtotal;
        $Quotationupdate->discount = $descuento;
        $Quotationupdate->ret_fuente=$retencion;
        $Quotationupdate->taxe=$impuesto;
        $Quotationupdate->order_reference ='C'.auth()->user()->campus->id.'I'.$quotation->id;
        $Quotationupdate->total = $total;
        $Quotationupdate->save();

        $referencia='C'.auth()->user()->campus->id.'I'.$quotation->id;

        switch ($data['payment_means']) {
            case "CREDIT_CARD":
                $payment_means='Tarjeta Crédito';
                break;
            case "DEBIT_CARD":
                $payment_means='Tarjeta Débito';
                break;
            case "CASH":
                $payment_means='Efectivo';
                break;
            case "CREDIT_ACH":
                $payment_means='Crédito ACH';
                break;
            case "DEBIT_ACH":
                $payment_means='Débito ACH';
                break;
            case "CHEQUE":
                $payment_means='Cheque';
                break;
            case "CREDIT_TRANSFER":
                $payment_means='Transferencia Crédito';
                break;
            case "DEBIT_TRANSFER":
                $payment_means='Transferencia Débito';
                break;
        }
        $customPaper = array(0,0,567.00,185.80);
        $data = array(
            'id'=>$quotation->id,
            'url'=>public_path('images/logo/logo.png'),
            'namecampuse'=>auth()->user()->campus->name,
            'nit'=>'445555-5',
            'phone'=>auth()->user()->campus->phone,
            'email'=>auth()->user()->campus->email,
            'address'=>auth()->user()->campus->address,
            'order_reference'=>'C'.auth()->user()->campus->id.'I'.$quotation->id,
            'date_creation'=>$datetime,
            'payment_means'=> $payment_means,
            'payment_means_type'=>$data['payment_means_type'],
            'customer'=>$customer['first_name'].' '.$customer['last_name'],
            'identification'=>$customer['identification'],
            'email_customer'=>$customer['email'],
            'phone_customer'=>$customer['phone'],
            'address_line'=>$customer['address_line'],
            'items'=>$items,
            'subtotal'=>$subtotal,
            'descuento'=>$descuento,
            'impuesto'=>$impuesto,
            'total'=>$total,
        );
        $pdf = PDF::loadView('admin.quotation.pdf',compact('data'))
        ->save(public_path('cotizaciones/cotizacion'.$referencia.'.pdf'));


        $factura=$request->root().'/cotizaciones/cotizacion'.$referencia.'.pdf';

        $Quotationupdate = Quotation::find($quotation->id);
        $Quotationupdate->pdf_url = $factura;
        $Quotationupdate->save();



        $log = new LogSistema();

        $log->user_id = auth()->user()->id;
        $log->tx_descripcion = 'El usuario: '.auth()->user()->display_name.' Ha guardado una nueva factura con id: '.$quotation->id.' a las: '
        . date('H:m:i').' del día: '.date('d/m/Y');
        $log->save();

        return json_encode(['success' => true, 'quotation' => $quotation->id,'pdf' => $factura]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Quotation::find(\Hashids::decode($id)[0])->delete();
        Quotation_Items::where('quotation_id',\Hashids::decode($id)[0])->delete();
        return json_encode(['success' => true]);
    }

    public function getcustomer($idcustomer)
    {

        $customer = Customers::find($idcustomer);

        return with(["customer" => $customer]);
    }

    public function getservice($idservice)
    {
        $service = Services::find($idservice);
        return with(["service" => $service]);
    }

    public function cities($departament)
    {
        $Departaments = Departaments::find($departament);

        $cities = Cities::where('code_departament', $Departaments['code'])->get();

        return with(["cities" => $cities]);
    }
}
