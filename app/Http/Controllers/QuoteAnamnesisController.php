<?php

namespace App\Http\Controllers;

use App\Models\QuoteAnamnesis;
use Illuminate\Http\Request;
use App\Models\Log\LogSistema;

class QuoteAnamnesisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $quoteanamnesis=QuoteAnamnesis::create(array_merge($request->all(),['campuse_id' => auth()->user()->campus->id,'profesional_id'=>auth()->user()->id])); 
        $log = new LogSistema();

        $log->user_id = auth()->user()->id;
        $log->tx_descripcion = 'El usuario: '.auth()->user()->display_name.' Ha guardado una nuevo anamnesis en el sistema: '.$quoteanamnesis->id.' a las: '
        . date('H:m:i').' del día: '.date('d/m/Y');
        $log->save();

        return json_encode(['success' => true, 'user_id' => $quoteanamnesis->encode_id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\QuoteAnamnesis  $quoteAnamnesis
     * @return \Illuminate\Http\Response
     */
    public function show(QuoteAnamnesis $quoteAnamnesis)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\QuoteAnamnesis  $quoteAnamnesis
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $anamnesis = QuoteAnamnesis::find(\Hashids::decode($id)[0]);

        $log = new LogSistema();
        $log->user_id = auth()->user()->id;
        $log->tx_descripcion = 'El usuario: '.auth()->user()->display_name.' Ha ingresado a editar del Anammnesis #: '.$anamnesis->id.' a las: '
        . date('H:m:i').' del día: '.date('d/m/Y');
        $log->save();
      
        return view('admin.anamnesis.edit', ['anamnesis' => $anamnesis]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\QuoteAnamnesis  $quoteAnamnesis
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $quoteanamnesis = QuoteAnamnesis::find(\Hashids::decode($id)[0]);        
        $quoteanamnesis->update(array_merge($request->all(),['campuse_id' => auth()->user()->campus->id,'profesional_id'=>auth()->user()->id]));

        return json_encode(['success' => true, 'service_id' => $quoteanamnesis->encode_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\QuoteAnamnesis  $quoteAnamnesis
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuoteAnamnesis $quoteAnamnesis)
    {
        //
    }
}
