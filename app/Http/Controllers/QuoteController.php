<?php

namespace App\Http\Controllers;

use App\Models\Quote;
use Illuminate\Http\Request;
use App\Models\Log\LogSistema;
use App\Models\User;
use App\Models\Availability;
use App\Models\Customers;
use App\Models\TypeQuote;
use App\Models\StatesQuote;


use App\Mail\CitaOdontologica;
use Illuminate\Support\Facades\Mail;

class QuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $log = new LogSistema();
        $log->user_id = auth()->user()->id;
        $log->tx_descripcion = 'El usuario: '.auth()->user()->display_name.' Ha ingresado a ver las citas: '
        . date('H:m:i').' del día: '.date('d/m/Y');
        $log->save();

        if(auth()->user()->hasRole('Administrador') || auth()->user()->hasRole('Recepción')){
            $quote = Quote::with('state')->with('user')->with('customer')->with('type')->where('campuse_id',auth()->user()->campus->id)->get();
        }else{
            $quote = Quote::with('state')->with('user')->with('customer')->with('type')->where('campuse_id',auth()->user()->campus->id)->where('user_id',auth()->user()->id)->get();

        }
        
        return view ('admin.quote.index', compact('quote'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $relacionEloquent = 'roles';
        $usersprofesional = User::where('campuse_id',auth()->user()->campus->id)->whereHas($relacionEloquent, function ($query) {
                return $query->where('name', '=', 'Profesional');
        })->get();

        $customers = Customers::get();
        $types = TypeQuote::where('campuse_id',auth()->user()->campus->id)->get();
        $statequote = StatesQuote::get();
       


        return view ('admin.quote.create',compact('usersprofesional','customers','types','statequote'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->input();
        $typequote=TypeQuote::where('id',$data['typequote_id'])->first();

        $duration = $data['typequote_id']=='0' ? 30 : $typequote['duration'];

        $var1 = $data['start_time'];
        $fechaFin = new \DateTime($var1);
        $fechaFin = $fechaFin->modify( '+'.$duration.' minutes' );
        

        $quote = Quote::create(array_merge($request->all(),['campuse_id' => auth()->user()->campus->id,'end_time'=>$fechaFin->format("H:i:s")]));

        
            $quoteupdate = Quote::find($quote->id);
            $quoteupdate->meet_id = 'Tele-consulta-'.$quote->id;
            $quoteupdate->payment = 0;
            $quoteupdate->save();
        

        $log = new LogSistema();
        $log->user_id = auth()->user()->id;
        $log->tx_descripcion = 'El usuario: '.auth()->user()->display_name.' Ha guardado una nueva cita en el sistema: '.$request->name.' a las: '
        . date('H:m:i').' del día: '.date('d/m/Y');
        $log->save();

       

        return json_encode(['success' => true, 'user_id' => $quote->encode_id,'urlwhatsapp'=>null]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Quote  $quote
     * @return \Illuminate\Http\Response
     */
    public function show(Quote $quote)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Quote  $quote
     * @return \Illuminate\Http\Response
     */
    public function edit(Quote $quote)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Quote  $quote
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Quote $quote)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Quote  $quote
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Quote = Quote::find(\Hashids::decode($id)[0])->delete();
        return json_encode(['success' => true]);
    }

    public function getquoteprofesional(Request $request){
        $data = $request->input();
        $typequote=TypeQuote::where('id',$data['typequote_id'])->first();

        
       

        $hours=array();
        $Availability=Availability::where('campuse_id',auth()->user()->campus->id)->where('user_id',$data['user_id'])->where('date',$data['date'])->get();
        foreach($Availability as $item){
            $var1 = $item['start_time'];
            $var2 = $item['end_time'];
            $intervarlo = $data['typequote_id']=='0' ? 30 : $typequote['duration'];
            $duration=$data['typequote_id']=='0' ? 30 : $typequote['duration'];
            $fechaInicio = new \DateTime($var1);
            $fechaFin = new \DateTime($var2);
            $fechaFin = $fechaFin->modify( '+'.$duration.' minutes' );
            $rangoFechas = new \DatePeriod($fechaInicio, new \DateInterval('PT'.$duration.'M'), $fechaFin);
            foreach($rangoFechas as $fecha){
                if($fecha->format("H:i:s")<=$item['end_time']){
                    $var1 = $fecha->format("H:i:s");
                    $fechaFin = new \DateTime($var1);
                    $fechaFin = $fechaFin->modify( '+'.$duration.' minutes' );
                    $horafin=$fechaFin->format("H:i:s");
                    $quote=Quote::where('campuse_id',auth()->user()->campus->id)->where('user_id',$data['user_id'])->where('date',$data['date'])->where('start_time',$fecha->format("H:i:s"))->whereBetween('end_time', [$fecha->format("H:i:s"), $horafin])->where('state_id', '!=' , 2)->first();
                    
                    if($quote==null){
                        $quote2=Quote::where('campuse_id',auth()->user()->campus->id)->where('user_id',$data['user_id'])->where('date',$data['date'])->whereBetween('end_time', [$fecha->format("H:i:s"), $horafin])->where('state_id', '!=' , 2)->first();
                        if($quote2==null){
                            array_push($hours,$fecha->format("H:i:s"));
                        }
                        
                    }

                }

            }

        }

        return with(["hours" => $hours]);

    }
}
