<?php

namespace App\Http\Controllers;

use App\Models\QuoteCustomerOdontogram;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QuoteCustomerOdontogramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $marcas = null;
        if ($request->marcas=='1') {	
            $marcas = [];		
			if (isset($request->Vestibular)) {
				$marcas['Vestibular']['Valor'] = true;
				if (isset($request->VestibularEstado)) {
					$marcas['Vestibular']['Estado'] = $request->VestibularEstado;
				}
			}
			if (isset($request->Palatino)) {
				$marcas['Palatino']['Valor'] = true;
				if (isset($request->PalatinoEstado)) {
					$marcas['Palatino']['Estado'] = $request->PalatinoEstado;
				}
			}
			if (isset($request->Lingual)) {
				$marcas['Lingual']['Valor'] = true;
				if (isset($request->LingualEstado)) {
					$marcas['Lingual']['Estado'] = $request->LingualEstado;
				}
			}
			if (isset($request->Distal)) {
				$marcas['Distal']['Valor'] = true;
				if (isset($request->DistalEstado)) {
					$marcas['Distal']['Estado'] = $request->DistalEstado;
				}
			}
			if (isset($request->Mesial)) {
				$marcas['Mesial']['Valor'] = true;
				if (isset($request->MesialEstado)) {
					$marcas['Mesial']['Estado'] = $request->MesialEstado;
				}
			}
			if (isset($request->Oclusal)) {
				$marcas['Oclusal']['Valor'] = true;
				if (isset($request->OclusalEstado)) {
					$marcas['Oclusal']['Estado'] = $request->OclusalEstado;
				}
			}

			$marcas = json_encode($marcas);

		}

        $cronogram = QuoteCustomerOdontogram::create([
            'customer_id'=>$request->customer_id,
            'campuse_id'=>auth()->user()->campus->id,
            'pacodo_tipo'=>$request->tipoOdontograma,
            'id_hal'=>$request->hallazgo,
            'pacodo_categoria'=>$request->categoria,
            'pacodo_estado'=>$request->estado,
            'pacodo_marcas'=>$marcas,
            'numero_die'=>$request->diente,
            'pacodo_dientefinal'=>$request->dienteFinal,
            'pacodo_sigla'=>$request->sigla,
            'pacodo_espec'=>$request->especificaciones,
            'profesional_id'=>auth()->user()->id
        ]);

        $hallazgo=$this->getHallazgos($cronogram->id);
        
        return json_encode(['success' => true, 'user_id' => $cronogram->id,'data'=>$hallazgo]);
    }
    public function getHallazgos($id){
        
    
        $hallazgos = DB::table('quote_customer_odontograma')
            ->join('dientes as inicio', 'quote_customer_odontograma.numero_die', '=', 'inicio.id')
            ->leftJoin('dientes as fin', 'quote_customer_odontograma.pacodo_dientefinal', '=', 'fin.id')
            ->select('quote_customer_odontograma.customer_id as id', 'quote_customer_odontograma.id_hal', 'quote_customer_odontograma.pacodo_categoria as categoria','quote_customer_odontograma.pacodo_estado as estado',
            'quote_customer_odontograma.pacodo_sigla as sigla','inicio.orden_die as inicio','fin.orden_die as fin','quote_customer_odontograma.pacodo_marcas as marcas',
            'quote_customer_odontograma.numero_die as diente')
            ->where('quote_customer_odontograma.id', $id)
            ->first();

        return $hallazgos;

        
    }

    public function getOdontograma(Request $request){      
        $hallazgos = DB::table('quote_customer_odontograma')
            ->join('dientes as inicio', 'quote_customer_odontograma.numero_die', '=', 'inicio.id')
            ->leftJoin('dientes as fin', 'quote_customer_odontograma.pacodo_dientefinal', '=', 'fin.id')
            ->select('quote_customer_odontograma.customer_id as id', 'quote_customer_odontograma.id_hal', 'quote_customer_odontograma.pacodo_categoria as categoria','quote_customer_odontograma.pacodo_estado as estado',
            'quote_customer_odontograma.pacodo_sigla as sigla','inicio.orden_die as inicio','fin.orden_die as fin','quote_customer_odontograma.pacodo_marcas as marcas',
            'quote_customer_odontograma.numero_die as diente')
            ->where('quote_customer_odontograma.customer_id', $request->paciente)
            ->where('quote_customer_odontograma.pacodo_tipo', $request->tipoOdontograma)
            ->get();
		return json_encode($hallazgos);
		
    }

    public function getHallazgosDientePaciente(Request $request){       
        $hallazgos = DB::table('quote_customer_odontograma')
            ->join('dientes as inicio', 'quote_customer_odontograma.numero_die', '=', 'inicio.id')
            ->leftJoin('dientes as fin', 'quote_customer_odontograma.pacodo_dientefinal', '=', 'fin.id')
            ->join('hallazgos', 'quote_customer_odontograma.id_hal', '=', 'hallazgos.id')
            ->select('quote_customer_odontograma.id as id,hallazgos.nombre_hal,quote_customer_odontograma.pacodo_categoria as categoria,quote_customer_odontograma.id_hal,quote_customer_odontograma.pacodo_estado as estado,quote_customer_odontograma.pacodo_sigla as sigla,inicio.orden_die as inicio, fin.orden_die as fin, quote_customer_odontograma.numero_die as dienteInicio,quote_customer_odontograma.pacodo_dientefinal as dienteFinal, quote_customer_odontograma.pacodo_espec as especificaciones,quote_customer_odontograma.pacodo_marcas as marcas, quote_customer_odontograma.numero_die as diente')
            ->where('quote_customer_odontograma.customer_id', $request->paciente)
            ->where('quote_customer_odontograma.pacodo_tipo', $request->tipoOdontograma)
            ->where('quote_customer_odontograma.numero_die', $request->diente)
            ->get();
		return json_encode($hallazgos);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\QuoteCustomerOdontogram  $quoteCustomerOdontogram
     * @return \Illuminate\Http\Response
     */
    public function show(QuoteCustomerOdontogram $quoteCustomerOdontogram)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\QuoteCustomerOdontogram  $quoteCustomerOdontogram
     * @return \Illuminate\Http\Response
     */
    public function edit(QuoteCustomerOdontogram $quoteCustomerOdontogram)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\QuoteCustomerOdontogram  $quoteCustomerOdontogram
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QuoteCustomerOdontogram $quoteCustomerOdontogram)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\QuoteCustomerOdontogram  $quoteCustomerOdontogram
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuoteCustomerOdontogram $quoteCustomerOdontogram)
    {
        //
    }
}
