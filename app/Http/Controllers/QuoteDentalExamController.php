<?php

namespace App\Http\Controllers;

use App\Models\QuoteDentalExam;
use Illuminate\Http\Request;
use App\Models\Log\LogSistema;

class QuoteDentalExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $quoteDentalExam=QuoteDentalExam::create(array_merge($request->all(),['campuse_id' => auth()->user()->campus->id,'profesional_id'=>auth()->user()->id])); 
        $log = new LogSistema();

        $log->user_id = auth()->user()->id;
        $log->tx_descripcion = 'El usuario: '.auth()->user()->display_name.' Ha guardado una nuevo examen dental en el sistema: '.$quoteDentalExam->id.' a las: '
        . date('H:m:i').' del día: '.date('d/m/Y');
        $log->save();

        return json_encode(['success' => true, 'user_id' => $quoteDentalExam->encode_id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\QuoteDentalExam  $quoteDentalExam
     * @return \Illuminate\Http\Response
     */
    public function show(QuoteDentalExam $quoteDentalExam)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\QuoteDentalExam  $quoteDentalExam
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dentalexam = QuoteDentalExam::find(\Hashids::decode($id)[0]);

        $log = new LogSistema();
        $log->user_id = auth()->user()->id;
        $log->tx_descripcion = 'El usuario: '.auth()->user()->display_name.' Ha ingresado a editar los datos del examen extra oral #: '.$dentalexam->id.' a las: '
        . date('H:m:i').' del día: '.date('d/m/Y');
        $log->save();
      
        return view('admin.dentalexam.edit', ['dentalexam' => $dentalexam]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\QuoteDentalExam  $quoteDentalExam
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $QuoteDentalExam = QuoteDentalExam::find(\Hashids::decode($id)[0]);        
        $QuoteDentalExam->update(array_merge($request->all(),['campuse_id' => auth()->user()->campus->id,'profesional_id'=>auth()->user()->id]));

        return json_encode(['success' => true, 'service_id' => $QuoteDentalExam->encode_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\QuoteDentalExam  $quoteDentalExam
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuoteDentalExam $quoteDentalExam)
    {
        //
    }
}
