<?php

namespace App\Http\Controllers;

use App\Models\QuoteExtraOralExam;
use Illuminate\Http\Request;
use App\Models\Log\LogSistema;

class QuoteExtraOralExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $quoteExtraOralExam=QuoteExtraOralExam::create(array_merge($request->all(),['campuse_id' => auth()->user()->campus->id,'profesional_id'=>auth()->user()->id])); 
        $log = new LogSistema();

        $log->user_id = auth()->user()->id;
        $log->tx_descripcion = 'El usuario: '.auth()->user()->display_name.' Ha guardado una nuevo examen extraoral en el sistema: '.$quoteExtraOralExam->id.' a las: '
        . date('H:m:i').' del día: '.date('d/m/Y');
        $log->save();

        return json_encode(['success' => true, 'user_id' => $quoteExtraOralExam->encode_id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\QuoteExtraOralExam  $quoteExtraOralExam
     * @return \Illuminate\Http\Response
     */
    public function show(QuoteExtraOralExam $quoteExtraOralExam)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\QuoteExtraOralExam  $quoteExtraOralExam
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $extraoral = QuoteExtraOralExam::find(\Hashids::decode($id)[0]);

        $log = new LogSistema();
        $log->user_id = auth()->user()->id;
        $log->tx_descripcion = 'El usuario: '.auth()->user()->display_name.' Ha ingresado a editar los datos del examen extra oral #: '.$extraoral->id.' a las: '
        . date('H:m:i').' del día: '.date('d/m/Y');
        $log->save();
      
        return view('admin.extraoral.edit', ['extraoral' => $extraoral]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\QuoteExtraOralExam  $quoteExtraOralExam
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $quoteExtraOralExam = QuoteExtraOralExam::find(\Hashids::decode($id)[0]);        
        $quoteExtraOralExam->update(array_merge($request->all(),['campuse_id' => auth()->user()->campus->id,'profesional_id'=>auth()->user()->id]));

        return json_encode(['success' => true, 'service_id' => $quoteExtraOralExam->encode_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\QuoteExtraOralExam  $quoteExtraOralExam
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuoteExtraOralExam $quoteExtraOralExam)
    {
        //
    }
}
