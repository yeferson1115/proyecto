<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use App\Models\Invoice;
use App\Models\Pos;

class ReportInvoicePosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

        $filtro=1;
        
        $datareport = Pos::select(\DB::raw("Month(created_at) as mes,SUM(total) as total, SUM(discount) as discounts, SUM(taxe) as taxes,SUM(ret_fuente) as ret_fuentes"))
        ->whereYear('created_at', date('Y'))
        ->where('state',1)
        ->where('campuse_id',auth()->user()->campus->id)
        ->groupBy(\DB::raw("Month(created_at)"))->get();
        foreach($datareport as $item){
            $fecha = Carbon::createFromFormat('!m', $item->mes); 
            $item->mes=$meses[($fecha->format('n')) - 1];
        }
        return view ('admin.reports.pos', compact('datareport','filtro'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

        $filtro=$request->date_start.' 00:00:00 a '.$request->date_end.' 23:59:59';
        
        $datareport = Pos::select(\DB::raw("Month(created_at) as mes,SUM(total) as total, SUM(discount) as discounts, SUM(taxe) as taxes,SUM(ret_fuente) as ret_fuentes"))
        ->whereYear('created_at', date('Y'))
        ->where('state',1)
        ->where('campuse_id',auth()->user()->campus->id)
        ->whereBetween('created_at', [$request->date_start.' 00:00:00', $request->date_end.' 23:59:59'])
        ->groupBy(\DB::raw("Month(created_at)"))->get();
        foreach($datareport as $item){
            $fecha = Carbon::createFromFormat('!m', $item->mes); 
            $item->mes=$meses[($fecha->format('n')) - 1];
        }
        return view ('admin.reports.pos', compact('datareport','filtro'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
