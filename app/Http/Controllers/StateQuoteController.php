<?php

namespace App\Http\Controllers;

use App\Models\StatesQuote;
use Illuminate\Http\Request;

class StateQuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StatesQuote  $statesQuote
     * @return \Illuminate\Http\Response
     */
    public function show(StatesQuote $statesQuote)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\StatesQuote  $statesQuote
     * @return \Illuminate\Http\Response
     */
    public function edit(StatesQuote $statesQuote)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\StatesQuote  $statesQuote
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StatesQuote $statesQuote)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StatesQuote  $statesQuote
     * @return \Illuminate\Http\Response
     */
    public function destroy(StatesQuote $statesQuote)
    {
        //
    }
}
