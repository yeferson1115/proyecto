<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Log\LogSistema;
use App\Models\Quote;
use App\Models\User;
use App\Models\Customers;
use App\Models\TypeQuote;
use App\Models\StatesQuote;

class TeleConsultationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $log = new LogSistema();
        $log->user_id = auth()->user()->id;
        $log->tx_descripcion = 'El usuario: '.auth()->user()->display_name.' Ha ingresado a ver sus teleconsultas: '
        . date('H:m:i').' del día: '.date('d/m/Y');
        $log->save();
        $customer=Customers::where('user_id',auth()->user()->id)->first();
        $quotes = Quote::with('state')->with('user')->with('customer')->where('customer_id',$customer['id'])->where('typequote_id',0)->get();
        
        return view ('admin.teleconsultation.index', compact('quotes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $relacionEloquent = 'roles';
        $usersprofesional = User::where('campuse_id',auth()->user()->campus->id)->whereHas($relacionEloquent, function ($query) {
                return $query->where('name', '=', 'Profesional');
        })->get();

        $customers = Customers::where('user_id',auth()->user()->id)->get();
        $types = TypeQuote::where('campuse_id',auth()->user()->campus->id)->get();
        $statequote = StatesQuote::get();
       


        return view ('admin.teleconsultation.create',compact('usersprofesional','customers','types','statequote'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $quote = Quote::with('customer')->find(\Hashids::decode($id)[0]);
        return view('admin.teleconsultation.show', ['quote' => $quote]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
