<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Vinkla\Hashids\Facades\Hashids;

class Availability extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'availability';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'campuse_id','user_id','date','start_time','end_time','created_at','updated_at'
    ];


    protected $guard_name = 'web';

    public function getEncodeIDAttribute()
    {
        return Hashids::encode($this->id);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }



}
