<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Vinkla\Hashids\Facades\Hashids;

class Campuses extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'campuses';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 'phone','email','address','token','accountid','number_invoice','prefix','resolution_number','number_note_credit'
    ];



    protected $searchable = [
        'columns' => [
          'name.name' => 5,
          'phone.phone' => 5,
          'email.email' => 5,
        ]
    ];

    protected $guard_name = 'web';

    public function getEncodeIDAttribute()
    {
        return Hashids::encode($this->id);
    }
}
