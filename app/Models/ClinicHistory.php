<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Vinkla\Hashids\Facades\Hashids;

class ClinicHistory extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'clinic_history';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'customer_id', 'created_at','updated_at'
    ];




    protected $guard_name = 'web';

    public function getEncodeIDAttribute()
    {
        return Hashids::encode($this->id);
    }

    public function customer()
    {
        return $this->hasOne(Customers::class, 'id');
    }

    

    

    public function anamnesis()
    {
        return $this->hasMany(QuoteAnamnesis::class,'clinichistory_id');
    }

    public function dentalexam()
    {
        return $this->hasMany(QuoteDentalExam::class,'clinichistory_id');
    }

    public function extraoralexam()
    {
        return $this->hasMany(QuoteExtraOralExam::class,'clinichistory_id');
    }
}
