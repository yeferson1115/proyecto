<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CreditNote_Items extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'note_credit_items';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'sku','quantity','description','price','discount_rate','note_credit_id','service_id','tax_rate','type_tax','ret_fuente'
    ];

    public function service()
    {
        return $this->belongsTo(Services::class,'service_id');
    }

}
