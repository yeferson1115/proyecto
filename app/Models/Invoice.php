<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'invoices';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'issue_date','payment_date','order_reference','invoice_type_code','payment_means','payment_means_type','customer_id','subtotal','discount','total','campuse_id','pdf_url','ret_fuente','taxe','number_invoice','dian_status','email_status','uuid','state'
    ];

    public function items()
    {
        return $this->hasMany(Invoice_Items::class, 'invoice_id');
    }

    public function service()
    {
        return $this->items->belongsTo(Services::class,'service_id');
    }

}
