<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice_Items extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'invoice_items';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'sku','quantity','description','price','discount_rate','invoice_id','service_id','tax_rate','type_tax','ret_fuente'
    ];

    public function service()
    {
        return $this->belongsTo(Services::class,'service_id');
    }

}
