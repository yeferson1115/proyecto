<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pos extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'pos';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'issue_date','payment_date','order_reference','invoice_type_code','payment_means','payment_means_type','customer_id','subtotal','discount','total','campuse_id','pdf_url','ret_fuente','taxe','state'
    ];

}
