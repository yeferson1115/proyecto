<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pos_Items extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'pos_items';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'sku','quantity','description','price','discount','pos_id','service_id','tax_rate','type_tax','ret_fuente'
    ];

}
