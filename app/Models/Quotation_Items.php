<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quotation_Items extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'quotation_items';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'sku','quantity','description','price','discount','quotation_id','service_id','tax_rate','type_tax','ret_fuente'
    ];

    
}
