<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Vinkla\Hashids\Facades\Hashids;

class Quote extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'quotes';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'campuse_id','user_id','customer_id','date','start_time','created_at','updated_at','typequote_id','end_time','state_id','reason','meet_id','payment'
    ];

    protected $guard_name = 'web';

    public function getEncodeIDAttribute()
    {
        return Hashids::encode($this->id);
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function customer()
    {
        return $this->belongsTo(Customers::class, 'customer_id');
    }
    public function type()
    {
        return $this->belongsTo(TypeQuote::class, 'typequote_id');
    }

    public function state()
    {
        return $this->belongsTo(StatesQuote::class, 'state_id');
    }

    public function profesional()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
