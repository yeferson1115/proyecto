<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Vinkla\Hashids\Facades\Hashids;

class QuoteAnamnesis extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'quote_anamnesis';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'quote_id','customer_id','profesional_id','campuse_id','clinichistory_id','bajo_tratamiento','toma_medicamento','enfermedad_venerea','intervencion_quirurgica','problemas_corazon','transfusion','hepatitis','consume_droga','fiebre_reumatica','asma','alergia_penicilina','diabetes','alergia_anestesia','ulcera_gastrica','alergia_aspirinayodo','tiroides','merthiolate','limitacionabrircerrarboca','tensionarterial','ruidomandibula','tensionarterialalta','herpes','tensionarterialbaja','sangraexcesivamente','morderunaslabios','problemasanguineo','no_cigarrillos','fumas','anemia','leucemia','hemofilia','deficit_vit_k','alimentoscitricos','vih','muerdeobjetosdientes','medicamentoretoviral','apretamientodentario','embarazada','respiracionbucal','pastillasanticonceptivas','observaciones'
    ];

    
     
        
    protected $guard_name = 'web';

    public function getEncodeIDAttribute()
    {
        return Hashids::encode($this->id);
    }
}
