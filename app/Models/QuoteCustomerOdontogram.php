<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Vinkla\Hashids\Facades\Hashids;

class QuoteCustomerOdontogram extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'quote_customer_odontograma';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'customer_id','campuse_id','clinichistory_id','pacodo_tipo','id_hal','pacodo_categoria','pacodo_estado','pacodo_marcas','numero_die','pacodo_dientefinal','pacodo_sigla','pacodo_espec','profesional_id'
    ];

    protected $guard_name = 'web';

    public function getEncodeIDAttribute()
    {
        return Hashids::encode($this->id);
    } 
    
   
          
}
