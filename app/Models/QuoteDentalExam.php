<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Vinkla\Hashids\Facades\Hashids;

class QuoteDentalExam extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'quote_dental_exam';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'quote_id','customer_id','profesional_id','campuse_id','clinichistory_id','supernumerarios','placa_blanda','abrasiones','placa_calcificada','erosiones','movilidad_dental','manchas','bolsas_periodontales','patalogia_pulpar','retraccion_gingival','observation'
    ];


    protected $guard_name = 'web';

    public function getEncodeIDAttribute()
    {
        return Hashids::encode($this->id);
    }
}
