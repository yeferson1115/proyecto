<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Vinkla\Hashids\Facades\Hashids;

class QuoteExtraOralExam extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'quote_extra_oral_exam';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'quote_id','customer_id','profesional_id','campuse_id','clinichistory_id','apreciacion_paciente','labios_comisura_labial','cabeza','lengua','cara','carrillos','glandulas_salivales','frenillos','cuello','piso_boca','cadena_danglionar','paladar_duro','atm','paladar_blando','region_retromolar'
    ];

        
    protected $guard_name = 'web';

    public function getEncodeIDAttribute()
    {
        return Hashids::encode($this->id);
    }
}
