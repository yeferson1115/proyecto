<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Vinkla\Hashids\Facades\Hashids;

class Services extends Model
{

    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 'price','sku','state','description','campuse_id'
    ];

    protected $searchable = [
        'columns' => [
          'name.name' => 5,
          'price.lastname' => 5,
          'state.lastname' => 5,
        ]
    ];

    protected $guard_name = 'web';

    public function getEncodeIDAttribute()
    {
        return Hashids::encode($this->id);
    }

    //
}
