<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatesQuote extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'states_quotes';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name'
    ];

    protected $searchable = [
        'columns' => [
          'name.name' => 5,        
        ]
    ];

    protected $guard_name = 'web';

    public function getEncodeIDAttribute()
    {
        return Hashids::encode($this->id);
    }

}
