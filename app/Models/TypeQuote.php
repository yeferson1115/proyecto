<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Vinkla\Hashids\Facades\Hashids;

class TypeQuote extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'typequotes';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'campuse_id','name','duration','created_at','updated_at'
    ];

    protected $guard_name = 'web';

    public function getEncodeIDAttribute()
    {
        return Hashids::encode($this->id);
    }
}
