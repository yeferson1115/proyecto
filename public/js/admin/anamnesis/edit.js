$(document).ready(function(){
   


  $('#main-form-anamnesis').submit(function(){

        $('.missing_alert').css('display', 'none');

       



        var data = $('#main-form-anamnesis').serialize();
        //$('input').iCheck('disable');
        $('#main-form-anamnesis input, #main-form-anamnesis button').attr('disabled','true');
        $('#ajax-icon').removeClass('fa fa-edit').addClass('fa fa-spin fa-refresh');

        Pace.track(function () {
            $.ajax({
              url: $('#main-form-anamnesis #_url').val(),
    		      headers: {'X-CSRF-TOKEN': $('#main-form-anamnesis #_token').val()},
    		      type: 'PUT',
              cache: false,
    	        data: data,
               success: function (response) {
                var json = $.parseJSON(response);
                if(json.success){
                  $('#main-form-anamnesis #submit').hide();
                  $('#main-form-anamnesis #edit-button').attr('href', $('#main-form-anamnesis #_url').val() + '/' + json.user_id + '/edit');
                  $('#main-form-anamnesis #edit-button').removeClass('hide');
                  //toastr.success('Datos modificados exitosamente');
                  _alertGeneric('success','Muy bien! ','Anamnesis guardada Correctamente',1);
                }
              },error: function (data) {
                var errors = data.responseJSON;
                console.log(errors);
                $.each( errors.errors, function( key, value ) {
                  toastr.error(value);
                  return false;
                });
                $('input').iCheck('enable');
                $('#main-form-anamnesis input, #main-form-anamnesis button').removeAttr('disabled');
                $('#ajax-icon').removeClass('fa fa-spin fa-refresh').addClass('fa fa-save');
              }
           });
        });

       return false;

    });
});
