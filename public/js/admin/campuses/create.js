$(document).ready(function(){



  $('#main-form').submit(function(){

        $('.missing_alert').css('display', 'none');

        if ($('#main-form #name').val() === '') {
            $('#main-form #name_alert').text('Ingrese nombre del sede').show();
            $('#main-form #name').focus();
            return false;
        }

        if ($('#main-form #phone').val() === '') {
            $('#main-form #phone_alert').text('Ingrese el teléfono de la sede').show();
            $('#main-form #phone').focus();
            return false;
        }
        if (! $('#main-form #email').val().match(/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/)) {
            $('#main-form #email_alert').text('Ingrese correo electrónico válido').show();
            $('#main-form #email').focus();
            return false;
        }

        if ($('#main-form #address').val() === '') {
            $('#main-form #address_alert').text('Ingrese una dirección').show();
            $('#main-form #address').focus();
            return false;
        }

        if($('#main-form #accountid').val() != '' || $('#main-form #token').val() != '' || $('#main-form #number_invoice').val() != '' || $('#main-form #prefix').val() != '' || $('#main-form #resolution_number').val() != ''){

            if ($('#main-form #accountid').val() === '') {
                $('#main-form #accountid_alert').text('Ingrese el id de la cuenta daitaco').show();
                $('#main-form #accountid').focus();
                return false;
            }
            if ($('#main-form #token').val() === '') {
                $('#main-form #token_alert').text('Ingrese el token de la cuenta daitaco').show();
                $('#main-form #token').focus();
                return false;
            }
            if ($('#main-form #number_invoice').val() === '') {
                $('#main-form #number_invoice_alert').text('Ingrese el último número de factura en daitaco').show();
                $('#main-form #number_invoice').focus();
                return false;
            }

            if ($('#main-form #number_note_credit').val() === '') {
                $('#main-form #number_note_credit_alert').text('Ingrese el último número de nota crédito en daitaco').show();
                $('#main-form #number_note_credit').focus();
                return false;
            }


            if ($('#main-form #prefix').val() === '') {
                $('#main-form #prefix_alert').text('Ingrese el prefijo de la facturación').show();
                $('#main-form #prefix').focus();
                return false;
            }
            if ($('#main-form #resolution_number').val() === '') {
                $('#main-form #resolution_number_alert').text('Ingrese el número de resolución').show();
                $('#main-form #resolution_number').focus();
                return false;
            }


        }



        var data = $('#main-form').serialize();
        //$('input').iCheck('disable');
        $('#main-form input, #main-form button').attr('disabled','true');
        $('#ajax-icon').removeClass('fa fa-save').addClass('fa fa-spin fa-refresh');
        Pace.track(function () {
            $.ajax({
              url: $('#main-form #_url').val(),
    		      headers: {'X-CSRF-TOKEN': $('#main-form #_token').val()},
    		      type: 'POST',
              cache: false,
    	        data: data,
              success: function (response) {
                var json = $.parseJSON(response);
                if(json.success){
                  $('#main-form #submit').hide();
                  $('#main-form #edit-button').attr('href', $('#main-form #_url').val() + '/' + json.user_id + '/edit');
                  $('#main-form #edit-button').removeClass('hide');
                  //notifications.success('Sede ingresada exitosamente');
                  _alertGeneric('success','Muy bien! ','Sede creada Correctamente','/campus');
                }
              },error: function (data) {
                var errors = data.responseJSON;
                console.log(errors);
                $.each( errors.errors, function( key, value ) {
                  notifications.error(value);
                  return false;
                });
                $('input').iCheck('enable');
                $('#main-form input, #main-form button').removeAttr('disabled');
                $('#ajax-icon').removeClass('fa fa-spin fa-refresh').addClass('fa fa-save');
              }
           });
        });

       return false;

    });
});
