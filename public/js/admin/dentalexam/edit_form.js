$(document).ready(function(){
   


  $('#main-form-dentalexam').submit(function(){

        $('.missing_alert').css('display', 'none');

       



        var data = $('#main-form-dentalexam').serialize();
        //$('input').iCheck('disable');
        $('#main-form-dentalexam input, #main-form-dentalexam button').attr('disabled','true');
        $('#ajax-icon').removeClass('fa fa-edit').addClass('fa fa-spin fa-refresh');

        Pace.track(function () {
            $.ajax({
              url: $('#main-form-dentalexam #_url').val(),
    		      headers: {'X-CSRF-TOKEN': $('#main-form-dentalexam #_token').val()},
    		      type: 'PUT',
              cache: false,
    	        data: data,
               success: function (response) {
                var json = $.parseJSON(response);
                if(json.success){
                  $('#main-form-dentalexam #submit').hide();
                  $('#main-form-dentalexam #edit-button').attr('href', $('#main-form-dentalexam #_url').val() + '/' + json.user_id + '/edit');
                  $('#main-form-dentalexam #edit-button').removeClass('hide');
                  //toastr.success('Datos modificados exitosamente');
                  _alertGeneric('success','Muy bien! ','Examen Dental Guardado Correctamente',2);
                }
              },error: function (data) {
                var errors = data.responseJSON;
                console.log(errors);
                $.each( errors.errors, function( key, value ) {
                  toastr.error(value);
                  return false;
                });
                $('input').iCheck('enable');
                $('#main-form-dentalexam input, #main-form-dentalexam button').removeAttr('disabled');
                $('#ajax-icon').removeClass('fa fa-spin fa-refresh').addClass('fa fa-save');
              }
           });
        });

       return false;

    });
});
