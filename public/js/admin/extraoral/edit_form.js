$(document).ready(function(){
   


  $('#main-form-extraoral').submit(function(){

        $('.missing_alert').css('display', 'none');

       



        var data = $('#main-form-extraoral').serialize();
        //$('input').iCheck('disable');
        $('#main-form-extraoral input, #main-form-extraoral button').attr('disabled','true');
        $('#ajax-icon').removeClass('fa fa-edit').addClass('fa fa-spin fa-refresh');

        Pace.track(function () {
            $.ajax({
              url: $('#main-form-extraoral #_url').val(),
    		      headers: {'X-CSRF-TOKEN': $('#main-form-extraoral #_token').val()},
    		      type: 'PUT',
              cache: false,
    	        data: data,
               success: function (response) {
                var json = $.parseJSON(response);
                if(json.success){
                  $('#main-form-extraoral #submit').hide();
                  $('#main-form-extraoral #edit-button').attr('href', $('#main-form-extraoral #_url').val() + '/' + json.user_id + '/edit');
                  $('#main-form-extraoral #edit-button').removeClass('hide');
                  //toastr.success('Datos modificados exitosamente');
                  _alertGeneric('success','Muy bien! ','Examen Extra Oral Guardado Correctamente',2);
                }
              },error: function (data) {
                var errors = data.responseJSON;
                console.log(errors);
                $.each( errors.errors, function( key, value ) {
                  toastr.error(value);
                  return false;
                });
                $('input').iCheck('enable');
                $('#main-form-extraoral input, #main-form-extraoral button').removeAttr('disabled');
                $('#ajax-icon').removeClass('fa fa-spin fa-refresh').addClass('fa fa-save');
              }
           });
        });

       return false;

    });
});
