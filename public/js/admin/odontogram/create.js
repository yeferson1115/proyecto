$(document).ready(function(){
   


  $('#FormHistoriaMovimientoAgregarHallazgo').submit(function(){

        $('.missing_alert').css('display', 'none');

       



        var data = $('#FormHistoriaMovimientoAgregarHallazgo').serialize();
        //$('input').iCheck('disable');
        $('#FormHistoriaMovimientoAgregarHallazgo input, #FormHistoriaMovimientoAgregarHallazgo button').attr('disabled','true');
        $('#ajax-icon').removeClass('fa fa-edit').addClass('fa fa-spin fa-refresh');

        Pace.track(function () {
            $.ajax({
              url: $('#FormHistoriaMovimientoAgregarHallazgo #_url').val(),
    		      headers: {'X-CSRF-TOKEN': $('#FormHistoriaMovimientoAgregarHallazgo #_token').val()},
    		      type: 'PUT',
              cache: false,
    	        data: data,
               success: function (response) {
                var json = $.parseJSON(response);
                if(json.success){
                  $('#FormHistoriaMovimientoAgregarHallazgo #submit').hide();
                  $('#FormHistoriaMovimientoAgregarHallazgo #edit-button').attr('href', $('#FormHistoriaMovimientoAgregarHallazgo #_url').val() + '/' + json.user_id + '/edit');
                  $('#FormHistoriaMovimientoAgregarHallazgo #edit-button').removeClass('hide');
                  //toastr.success('Datos modificados exitosamente');
                  _alertGeneric('success','Muy bien! ','Examen Extra Oral Guardado Correctamente',1);
                }
              },error: function (data) {
                var errors = data.responseJSON;
                console.log(errors);
                $.each( errors.errors, function( key, value ) {
                  toastr.error(value);
                  return false;
                });
                $('input').iCheck('enable');
                $('#FormHistoriaMovimientoAgregarHallazgo input, #FormHistoriaMovimientoAgregarHallazgo button').removeAttr('disabled');
                $('#ajax-icon').removeClass('fa fa-spin fa-refresh').addClass('fa fa-save');
              }
           });
        });

       return false;

    });
});
