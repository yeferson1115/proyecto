$(document).ready(function(){

    $('.customers').on('change', function(e){
        var customer = e.target.value;
        if(customer!=''){
            $.get('customers/' + customer,function(data) {
                $('#infoname').text(data.customer.first_name+' '+data.customer.last_name);
                $('#infodocument').text(data.customer.identification);
                $('#infophone').text(data.customer.phone);
                $('#infoemail').text(data.customer.email);
         })
        }

    });

    $('.service').on('change', function(e){
        var service = e.target.value;
        console.log(service);
        if(service!=''){
            $.get('invoice/service/' + service,function(data) {
                console.log(data);
                //$('#infoname').text(data.customer.first_name+' '+data.customer.last_name);
                //$('#infodocument').text(data.customer.identification);
                //$('#infophone').text(data.customer.phone);
                //$('#infoemail').text(data.customer.email);
         })
        }

    });

  $('#main-form').submit(function(){

        $('.missing_alert').css('display', 'none');

        if ($('#main-form #select2-basic').val() === '') {
            $('#main-form #customer_alert').text('Seleccione un cliente').show();
            $('#main-form #select2-basic').focus();
            return false;
        }


        if ($('#main-form #payment_means').val() === '') {
            $('#main-form #payment_means_alert').text('Seleccione un metodo de pago').show();
            $('#main-form #payment_means').focus();
            return false;
        }

        if ($('#main-form #payment_means_type').val() === '') {
            $('#main-form #payment_means_type_alert').text('Seleccione un tipo de pago ').show();
            $('#main-form #payment_means_type').focus();
            return false;
        }




        var data = $('#main-form').serialize();
        //$('input').iCheck('disable');
        $('#main-form input, #main-form button').attr('disabled','true');
        $('#ajax-icon').removeClass('fa fa-save').addClass('fa fa-spin fa-refresh');
        Pace.track(function () {
            $.ajax({
              url: $('#main-form #_url').val(),
    		      headers: {'X-CSRF-TOKEN': $('#main-form #_token').val()},
    		      type: 'POST',
              cache: false,
    	        data: data,
              success: function (response) {
                var json = $.parseJSON(response);
                if(json.success){
                  $('#main-form #submit').hide();
                  $('#main-form #edit-button').attr('href', $('#main-form #_url').val() + '/' + json.user_id + '/edit');
                  $('#main-form #edit-button').removeClass('hide');
                  //notifications.success('Servicio ingresado exitosamente');
                  //alert('ingresado exitosamente');
                  if(json.pdf!=null){
                    _ConfirmFactura(json.pdf);
                  }else{
                    _alertGeneric('success','Muy bien! ','factura creada Correctamente','/pos');
                  }

                }
              },error: function (data) {
                var errors = data.responseJSON;
                console.log(errors);
                $.each( errors.errors, function( key, value ) {
                  notifications.error(value);
                  return false;
                });
                $('input').iCheck('enable');
                $('#main-form input, #main-form button').removeAttr('disabled');
                $('#ajax-icon').removeClass('fa fa-spin fa-refresh').addClass('fa fa-save');
              }
           });
        });

       return false;

    });
});

var item=0;

function addItem(){
    const formatterPeso = new Intl.NumberFormat('es-CO', {
       style: 'currency',
       currency: 'COP',
       minimumFractionDigits: 0
     });
    var service=$('#services').val();
    if(service==''){
        _alertGeneric('info','Información','Seleccione un servicio',null);
        return false;
    }

    var additem=item+1;
    item=additem;


    var infoservice=service.split(',');
    $('.element').append(' <div class="row"><div class="col-md-2 col-12"><div class="mb-1"><label class="form-label" for="service">Servicio</label><input type="text" class="form-control"  name="service[]" value="'+infoservice[1]+'"  required readonly /><input type="hidden" class="form-control"  name="idservice[]"  value="'+infoservice[0]+'" readonly/></div></div><div class="col-md-2 col-12"><div class="mb-1"><label class="form-label" for="description">Descripción</label><input type="text" class="form-control"  name="description[]" value="'+infoservice[4]+'"  required /></div></div><div class="col-md-2 col-12"><div class="mb-1"><label class="form-label" for="sku">Precio unidad</label><input type="numeric" style="padding: 7px;" class="form-control" id="price-'+additem+'" name="price[]" onchange="changeprice('+additem+');"  value="'+infoservice[3]+'" required /></div></div><div class="col-md-1 col-12"><div class="mb-1"><label for="title" class="form-label">Cantidad</label><input type="numeric"  class="form-control" id="quantity-'+additem+'" name="quantity[]" onchange="calculatepriceitem(this.value,'+additem+');" value="1" required /></div></div><div class="col-md-1 col-12"><div class="mb-1"><label for="title" class="form-label">Descuento %</label><input style="padding: 5px;" type="number" class="form-control start" id="discount-'+additem+'" name="discount[]"  onchange="calculatediscountitem(this.value,'+additem+');" value="0" /></div></div><div class="col-md-1 col-12"><div class="mb-1"><label for="title" class="form-label">Ret. Fuente</label><select style="padding: 5px;"  class="form-control" id="ret_fuente-'+additem+'" name="ret_fuente[]"  onchange="calculateretfuente(this.value,'+additem+');" ><option value="0"></option><option value="0.1">0.1%</option><option value="0.5">0.5%</option><option value="1">1%</option><option value="1.5">1.5%</option><option value="2">2%</option><option value="2.5">2.5%</option><option value="3">3%</option><option value="3.5">3.5%</option><option value="4">4%</option><option value="6">6%</option><option value="7">7%</option><option value="10">10%</option><option value="11">11%</option><option value="20">20%</option></select></div></div><div class="col-md-1 col-12"><div class="mb-1"><label for="title" class="form-label">Impuesto</label><select style="padding: 5px;"  class="form-control impuesto" id="taxes-'+additem+'" name="taxes[]"  onchange="calculatetaxes(this.value,'+additem+');" ><option value="IVA-0"></option><option value="IVA-0">IVA 0%(exento)</option><option value="IVA-5">IVA 5%</option><option value="IVA-16">IVA 16%</option><option value="IVA-19">IVA 19%</option><option value="IMP_CONSUMO-2">Consumo 2%</option><option value="IMP_CONSUMO-4">Consumo 4%</option><option value="IMP_CONSUMO-8">Consumo 8%</option><option value="IMP_CONSUMO-16">Consumo 16%</option></select></div></div><div class="col-md-2 col-12"><div class="mb-1"><label for="title" class="form-label">Sub-Total</label><input type="text" class="form-control end" id="subtotal-'+additem+'" name="subtotal[]"  value="'+formatterPeso.format(infoservice[3])+'" readonly /></div></div></div>');

    calculatefullprice(additem);
}

function changeprice(additem){
    calculatefullprice(additem);
    calculatepriceitem(null,additem);
}


function calculatefullprice(item){
    const formatterPeso = new Intl.NumberFormat('es-CO', {
        style: 'currency',
        currency: 'COP',
        minimumFractionDigits: 0
      });

    var total=0;
    var subtotal=0;
    var descuento=0;
    var retencion=0;
    var impuesto=0;
    $('.iva').remove();
    for (var i=1; i<=item; i++) {
        var desctotal=0;
        var rettotal=0;
        var taxetotal=0;
        var price=$('#price-'+i).val();
        var cantidad=$('#quantity-'+i).val();
        var discount=$('#discount-'+i).val();
        var ret_fuente=$('#ret_fuente-'+i).val();
        var taxesinput=$('#taxes-'+i).val();
        var tax = taxesinput.split('-');

        var taxe=tax[1];
        var sub=price*cantidad;
        var desc=(sub*discount);
        if(desc>0){
            desctotal=desc/100;
        }else{
            desctotal=desc;
        }
        var sub1=(price*cantidad)-desctotal;
        var ret=(sub1*ret_fuente);
        if(ret>0){
            rettotal=ret/100;
        }else{
            rettotal=ret;
        }
        var taxes=(sub1*taxe);
        if(taxes>0){
            taxetotal=taxes/100;
        }else{
            taxetotal=taxes;
        }

        subtotal=(subtotal+sub);
        total=((total+sub)-desctotal)-rettotal+taxetotal;
        descuento=descuento+desctotal;
        retencion=retencion+rettotal;
        impuesto=impuesto+taxetotal;
        $('#impuestos').append('<p class="iva invoice-total-title">'+tax[0]+'('+tax[1]+'%) '+formatterPeso.format(taxetotal)+'</p>')
    }


     $('#fulltotal').text('Total: '+formatterPeso.format(total));
     $('#fulldiscount').text('Descuento: '+formatterPeso.format(descuento));
     $('#fullret').text('Ret. Fuente: -'+formatterPeso.format(retencion));
     $('#fullsubtotal').text('Subtotal: '+formatterPeso.format(subtotal));


}

function calculatepriceitem(val,inputitem){
    const formatterPeso = new Intl.NumberFormat('es-CO', {
       style: 'currency',
       currency: 'COP',
       minimumFractionDigits: 0
     });
     var price=$('#price-'+inputitem).val();
     var cantidad=$('#quantity-'+inputitem).val();
     var discount=$('#discount-'+inputitem).val();
    var totalitem=price*cantidad;
    console.log(totalitem);
    $('#subtotal-'+inputitem).val(formatterPeso.format(totalitem));
    calculatefullprice(item);
}
function calculatediscountitem(val,iteminput){
    calculatefullprice(item);
}
function calculateretfuente(val,iteminput){
    calculatefullprice(item);
}
function calculatetaxes(val,iteminput){
    calculatefullprice(item);
}
