$(document).ready(function(){

    $('#departament_id').on('change', function(e){
        var department = e.target.value;
        $.get('cities/' + department,function(data) {
        $('#city_id').empty();
        $.each(data, function(fetch, city){
            $('#city_id').append('<option value="">Seleccione</option>');
            for(i = 0; i < city.length; i++){
            $('#city_id').append('<option value="'+ city[i].id +'">'+ city[i].name +'</option>');
            }
        })
     })
    });

  $('#main-form-customer').submit(function(){

        $('.missing_alert').css('display', 'none');

        if ($('#main-form-customer #first_name').val() === '') {
            $('#main-form-customer #first_name_alert').text('Ingrese nombre del vliente').show();
            $('#main-form-customer #first_name').focus();
            return false;
        }

        if ($('#main-form-customer #last_name').val() === '') {
            $('#main-form-customer #last_name_alert').text('Ingrese el apellido del cliente').show();
            $('#main-form-customer #last_name').focus();
            return false;
        }

        if ($('#main-form-customer #type').val() === '') {
            $('#main-form-customer #type_alert').text('Seleccione un tipo').show();
            $('#main-form-customer #type').focus();
            return false;
        }

        if ($('#main-form-customer #identification_type').val() === '') {
            $('#main-form-customer #identification_type_alert').text('Seleccione un tipo de identificación').show();
            $('#main-form-customer #identification_type').focus();
            return false;
        }

        if ($('#main-form-customer #identification').val() === '') {
            $('#main-form-customer #identification_alert').text('Ingrese identificación').show();
            $('#main-form-customer #identification').focus();
            return false;
        }

        if ($('#main-form-customer #regimen').val() === '') {
            $('#main-form-customer #regimen_alert').text('Seleccione un regimen').show();
            $('#main-form-customer #regimen').focus();
            return false;
        }

        if ($('#main-form-customer #departament_id').val() === '') {
            $('#main-form-customer #departament_id_alert').text('Seleccione un departamento').show();
            $('#main-form-customer #departament_id').focus();
            return false;
        }

        if ($('#main-form-customer #city_id').val() === '') {
            $('#main-form-customer #city_id_alert').text('Seleccione una ciudad').show();
            $('#main-form-customer #city_id').focus();
            return false;
        }

        if ($('#main-form-customer #phone').val() === '') {
            $('#main-form-customer #phone_alert').text('Ingrese un télefono').show();
            $('#main-form-customer #phone').focus();
            return false;
        }

        if (! $('#main-form-customer #email').val().match(/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/)) {
            $('#main-form-customer #email_alert').text('Ingrese correo electrónico válido').show();
            $('#main-form-customer #email').focus();
            return false;
        }

        if ($('#main-form-customer #address_line').val() === '') {
            $('#main-form-customer #address_line_alert').text('Ingrese una dirección').show();
            $('#main-form-customer #address_line').focus();
            return false;
        }


        var data = $('#main-form-customer').serialize();
        //$('input').iCheck('disable');
        $('#main-form-customer input, #main-form-customer button').attr('disabled','true');
        $('#ajax-icon').removeClass('fa fa-save').addClass('fa fa-spin fa-refresh');
        Pace.track(function () {
            $.ajax({
              url: $('#main-form-customer #_url').val(),
    		      headers: {'X-CSRF-TOKEN': $('#main-form-customer #_token').val()},
    		      type: 'POST',
              cache: false,
    	        data: data,
              success: function (response) {
                var json = $.parseJSON(response);
                if(json.success){
                  $('#main-form-customer #submit').hide();
                  $('#main-form-customer #edit-button').attr('href', $('#main-form-customer #_url').val() + '/' + json.user_id + '/edit');
                  $('#main-form-customer #edit-button').removeClass('hide');
                  //notifications.success('Servicio ingresado exitosamente');
                  _alertGeneric('success','Muy bien! ','Cliente creado Correctamente',1);
                }
              },error: function (data) {
                var errors = data.responseJSON;
                console.log(errors);
                $.each( errors.errors, function( key, value ) {
                  notifications.error(value);
                  return false;
                });
                $('input').iCheck('enable');
                $('#main-form-customer input, #main-form-customer button').removeAttr('disabled');
                $('#ajax-icon').removeClass('fa fa-spin fa-refresh').addClass('fa fa-save');
              }
           });
        });

       return false;

    });
});
