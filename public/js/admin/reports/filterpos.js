$(document).ready(function(){



  $('#main-form').submit(function(){

        $('.missing_alert').css('display', 'none');

        if ($('#main-form #date_start').val() === '') {
            $('#main-form #date_start_alert').text('Seleccione una fecha inicial').show();
            $('#main-form #date_start').focus();
            return false;
        }


        if ($('#main-form #date_end').val() === '') {
            $('#main-form #date_end_alert').text('Seleccione una fecha final').show();
            $('#main-form #date_end').focus();
            return false;
        }



    });
});
