@extends('layouts.admin')

@section('title', 'Disponibilidad')
@section('page_title', 'Disponibilidad')
@section('page_subtitle', 'Guardar')
@section('content')

<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-start mb-0">Nueva Disponibilidad</h2>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="/home">Inicio &nbsp; &nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="/availability">Disponibilidades &nbsp; &nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                        </li>
                        <li class="breadcrumb-item active">Nueva disponibilidad</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    <section id="multiple-column-form">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Crear disponibilidad</h4>
                    </div>
                    <div class="card-body">
                        <form class="form" role="form" action="javascript:void(0)" id="main-form" autocomplete="off">
                            <input type="hidden" id="_url" value="{{ url('availability') }}">
                            <input type="hidden" id="_token" value="{{ csrf_token() }}">
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="name">Profesional</label>
                                        @if(Auth::user()->hasRole('Administrador'))
                                        <select class="invoiceto1 form-select customer" id="user_id" name="user_id">
                                            <option value="">Seleccione</option>
                                            @foreach ($usersprofesional as $user)
                                                <option value="{{ $user->id }}">{{ $user->name }} {{ $user->lastname }}</option>
                                            @endforeach
                                        </select>
                                        @else
                                        <select class="invoiceto1 form-select customer" id="user_id" name="user_id">
                                            @foreach ($usersprofesional as $user)
                                                <option value="{{ $user->id }}">{{ $user->name }} {{ $user->lastname }}</option>
                                            @endforeach
                                        </select>


                                        @endif
                                        <span class="missing_alert text-danger" id="user_id_alert"></span>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="date">Fecha</label>
                                        <input type="date" id="date" name="date" class="form-control" placeholder="Fecha">
                                        <span class="missing_alert text-danger" id="date_alert"></span>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="start_time">Hora inicio</label>
                                        <input type="time" id="start_time" name="start_time" class="form-control" placeholder="Hora inicio">
                                        <span class="missing_alert text-danger" id="start_time_alert"></span>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="end_time">Hora fin</label>
                                        <input type="time" id="end_time" name="end_time" class="form-control" placeholder="Hora fin">
                                        <span class="missing_alert text-danger" id="end_time_alert"></span>
                                    </div>
                                </div>


                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary me-1 waves-effect waves-float waves-light ajax" id="submit"><i id="ajax-icon" class="fa fa-save"></i> Guardar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection

@push('scripts')

    <script src="{{ asset('js/admin/availability/create.js') }}"></script>
@endpush
