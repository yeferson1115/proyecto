@extends('layouts.admin')

@section('title', 'Usuarios')
@section('page_title', 'Usuarios')
@section('page_subtitle', 'Editar')
@section('content')


<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-start mb-0">Editar: {{ $campuses->name }}</h2>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="/home">Inicio &nbsp; &nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="/campus">Sedes &nbsp; &nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                        </li>
                        <li class="breadcrumb-item active">Editar: {{ $campuses->name }}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    <section id="multiple-column-form">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Editar Sede</h4>
                    </div>
                    <div class="card-body">
                        <form class="form" role="form" id="main-form" autocomplete="off">
                            <input type="hidden" id="_url" value="{{ url('campus',[$campuses->encode_id]) }}">
                            <input type="hidden" id="_token" value="{{ csrf_token() }}">
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="name">Nombre</label>
                                        <input type="text" class="form-control" id="name" name="name" value="{{ $campuses->name }}" placeholder="Nombre Sede">
                                        <span class="missing_alert text-danger" id="name_alert"></span>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="phone">Teléfono</label>
                                        <input type="number" id="phone" name="phone" class="form-control" value="{{ $campuses->phone }}" placeholder="Teléfono">
                                        <span class="missing_alert text-danger" id="phone_alert"></span>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="email">E-mail</label>
                                        <input type="email" id="email" name="email" class="form-control" value="{{ $campuses->email }}" placeholder="Email">
                                        <span class="missing_alert text-danger" id="email_alert"></span>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="address">Dirección</label>
                                        <input type="text" id="address" name="address" class="form-control" value="{{ $campuses->address }}" placeholder="Dirección">
                                        <span class="missing_alert text-danger" id="address_alert"></span>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="token">Auth token</label>
                                        <input type="text" id="token" name="token" class="form-control" value="{{ $campuses->token }}" placeholder="Auth token daitaco">
                                        <span class="missing_alert text-danger" id="token_alert"></span>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="accountid">Id cuenta daitaco</label>
                                        <input type="text" id="accountid" name="accountid" class="form-control" value="{{ $campuses->accountid }}" placeholder="Id cuenta daitaco">
                                        <span class="missing_alert text-danger" id="accountid_alert"></span>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="number_invoice">Número de la última factura de dataico</label>
                                        <input type="text" id="number_invoice" name="number_invoice" class="form-control" value="{{ $campuses->number_invoice }}" placeholder="1111111">
                                        <span class="missing_alert text-danger" id="number_invoice_alert"></span>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="number_note_credit">Número de la última nota crédito de dataico</label>
                                        <input type="number" id="number_note_credit" name="number_note_credit" class="form-control" value="{{ $campuses->number_note_credit }}" placeholder="1111111">
                                        <span class="missing_alert text-danger" id="number_note_credit_alert"></span>
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="prefix">Prefijo</label>
                                        <input type="text" id="prefix" name="prefix" class="form-control" value="{{ $campuses->prefix }}" placeholder="Prefijo">
                                        <span class="missing_alert text-danger" id="prefix_alert"></span>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="resolution_number">Número resolución</label>
                                        <input type="text" id="resolution_number" name="resolution_number" class="form-control" value="{{ $campuses->resolution_number }}" placeholder="Número resolución">
                                        <span class="missing_alert text-danger" id="resolution_number_alert"></span>
                                    </div>
                                </div>



                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary me-1 waves-effect waves-float waves-light ajax" id="submit"><i id="ajax-icon" class="fa fa-save"></i> Actualizar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection
@push('scripts')

    <script src="{{ asset('js/admin/campuses/edit.js') }}"></script>
@endpush
