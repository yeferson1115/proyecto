<form class="form" role="form" id="main-form-anamnesis" autocomplete="off">
    <input type="hidden" id="_url" value="{{ url('quoteanamnesis',[$anamnesis->encode_id]) }}">
    <input type="hidden" id="_token" value="{{ csrf_token() }}">
        <div class="row">
        <table class="table">
            <thead>
                <tr>
                <th scope="col"></th>
                <th scope="col">Si</th>
                <th scope="col">No</th>
                <th scope="col"></th>
                <th scope="col">Si</th>
                <th scope="col">No</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">Está usteded bajo tratamiento?</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="bajo_tratamiento" id="questionsi1" value="Si" {{ ($anamnesis->bajo_tratamiento=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="questionsi1">Si</label>
                        </div>
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="bajo_tratamiento" id="questionno1" value="No" {{ ($anamnesis->bajo_tratamiento=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="questionno1">No</label>
                        </div>
                    </td>
                    <th  scope="row">Sufre o ha sufrido:</th >
                    <td></td>
                    <td></td>
                </tr>

                <tr>
                    <th scope="row">Toma actualmente algún medicamento?</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="toma_medicamento" id="questionsi2" value="Si" {{ ($anamnesis->toma_medicamento=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="questionsi2">Si</label>
                        </div>                      
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="toma_medicamento" id="questionno2" value="No" {{ ($anamnesis->toma_medicamento=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="questionno2">No</label>
                        </div>
                    </td>
                    <th  scope="row">- Enfermedad venérea</th >
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="enfermedad_venerea" id="enfermedad_venereasi" value="Si" {{ ($anamnesis->enfermedad_venerea=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="enfermedad_venereasi">Si</label>
                        </div> 
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="enfermedad_venerea" id="enfermedad_venereano" value="No" {{ ($anamnesis->enfermedad_venerea=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="enfermedad_venereano">No</label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Le han practicado alguna intervención quirúrgica?</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="intervencion_quirurgica" id="questionsi3" value="Si" {{ ($anamnesis->intervencion_quirurgica=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="questionsi3">Si</label>
                        </div>                       
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="intervencion_quirurgica" id="questionno3" value="No" {{ ($anamnesis->intervencion_quirurgica=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="questionno3">No</label>
                        </div>
                    </td>
                    <th scope="row">- Problemas de Corazón</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="problemas_corazon" id="problemascorazonsi" value="Si" {{ ($anamnesis->problemas_corazon=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="problemascorazonsi">Si</label>
                        </div>
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="problemas_corazon" id="problemascorazonno" value="No" {{ ($anamnesis->problemas_corazon=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="problemascorazonno">No</label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Ha recibido alguna transfusión sanguínea?</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="transfusion" id="questionsi4" value="Si" {{ ($anamnesis->transfusion=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="questionsi4">Si</label>
                        </div>                      
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="transfusion" id="questionno4" value="No" {{ ($anamnesis->transfusion=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="questionno4">No</label>
                        </div>
                    </td>
                    <th scope="row">- Hepatitis</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="hepatitis" id="hepatitissi" value="Si" {{ ($anamnesis->hepatitis=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="hepatitissi">Si</label>
                        </div>  
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="hepatitis" id="hepatitisno" value="No" {{ ($anamnesis->hepatitis=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="hepatitisno">No</label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Ha consumido o consume droga?</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="consume_droga" id="questionsi5" value="Si" {{ ($anamnesis->consume_droga=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="questionsi5">Si</label>
                        </div>                       
                    </td>
                    <td> 
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="consume_droga" id="questionno5" value="No" {{ ($anamnesis->consume_droga=="No")? "checked" : "" }} >
                            <label class="form-check-label" for="questionno5">No</label>
                        </div>
                    </td>
                    <th scope="row">- Fiebre reumática</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="fiebre_reumatica" id="fiebrereumaticasi" value="Si" {{ ($anamnesis->fiebre_reumatica=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="fiebrereumaticasi">Si</label>
                        </div>   
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="fiebre_reumatica" id="fiebrereumaticano" value="No" {{ ($anamnesis->fiebre_reumatica=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="fiebrereumaticano">No</label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Ha presentado reacción alérgica a:</th>
                    <td></td>
                    <td></td>
                    <th scope="row">- Asma</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="asma" id="asmasi" value="Si" {{ ($anamnesis->asma=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="asmasi">Si</label>
                        </div> 
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="asma" id="asmano" value="No" {{ ($anamnesis->asma=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="asmano">No</label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">- Penicilina</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="alergia_penicilina" id="questionsi6" value="Si" {{ ($anamnesis->alergia_penicilina=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="questionsi6">Si</label>
                        </div>                        
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="alergia_penicilina" id="questionno6" value="No" {{ ($anamnesis->alergia_penicilina=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="questionno6">No</label>
                        </div>
                    </td>
                    <th scope="row">- Diabetes</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="diabetes" id="diabetessi" value="Si" {{ ($anamnesis->diabetes=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="diabetessi">Si</label>
                        </div>   
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="diabetes" id="diabetesno" value="No" {{ ($anamnesis->diabetes=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="diabetesno">No</label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">- Anestesia</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="alergia_anestesia" id="questionsi7" value="Si" {{ ($anamnesis->alergia_anestesia=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="questionsi7">Si</label>
                        </div>
                        
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="alergia_anestesia" id="questionno7" value="No" {{ ($anamnesis->alergia_anestesia=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="questionno7">No</label>
                        </div>
                    </td>
                    <th scope="row">- Úlcera gástrica</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="ulcera_gastrica" id="ulceragatricasi" value="Si" {{ ($anamnesis->ulcera_gastrica=="Si")? "checked" : "" }} >
                            <label class="form-check-label" for="ulceragatricasi">Si</label>
                        </div>
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="ulcera_gastrica" id="ulceragatricano" value="No" {{ ($anamnesis->ulcera_gastrica=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="ulceragatricano">No</label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">- Aspirina yodo</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="alergia_aspirinayodo" id="questionsi8" value="Si" {{ ($anamnesis->alergia_aspirinayodo=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="questionsi8">Si</label>
                        </div>                      
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="alergia_aspirinayodo" id="questionno8" value="No" {{ ($anamnesis->alergia_aspirinayodo=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="questionno8">No</label>
                        </div>
                    </td>
                    <th scope="row">- Tiroides</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="tiroides" id="tiroidessi" value="Si" {{ ($anamnesis->tiroides=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="tiroidessi">Si</label>
                        </div> 
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="tiroides" id="tiroidesno" value="No" {{ ($anamnesis->tiroides=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="tiroidesno">No</label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <th scope="row">- Merthiolate; Otros</th>
                    <td colspan="2">
                        <textarea class="form-control" name="merthiolate" id="merthiolate">{{ $anamnesis->merthiolate }}</textarea>
                    </td>
                    <th scope="row">Ha tenido limitación al abrir o cerrar la boca?</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="limitacionabrircerrarboca" id="limitacionabrircerrarbocasi" value="Si" {{ ($anamnesis->limitacionabrircerrarboca=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="limitacionabrircerrarbocasi">Si</label>
                        </div> 
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="limitacionabrircerrarboca" id="limitacionabrircerrarbocano" value="No" {{ ($anamnesis->limitacionabrircerrarboca=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="limitacionabrircerrarbocano">No</label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <th scope="row">Sufre de tensión arterial?</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="tensionarterial" id="tensionarterialsi" value="Si" {{ ($anamnesis->tensionarterial=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="tensionarterialsi">Si</label>
                        </div> 
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="tensionarterial" id="tensionarterialno" value="No" {{ ($anamnesis->tensionarterial=="No")? "checked" : "" }} >
                            <label class="form-check-label" for="tensionarterialno">No</label>
                        </div>
                    </td>
                    <th scope="row">Siente ruidos en la mandibula al abrir o cerrar la boca?</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="ruidomandibula" id="ruidomandibulasi" value="Si" {{ ($anamnesis->ruidomandibula=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="ruidomandibulasi">Si</label>
                        </div> 
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="ruidomandibula" id="ruidomandibulano" value="No" {{ ($anamnesis->ruidomandibula=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="ruidomandibulano">No</label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">- Alta</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="tensionarterialalta" id="tensionarterialaltasi" value="Si" {{ ($anamnesis->tensionarterialalta=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="tensionarterialaltasi">Si</label>
                        </div> 
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="tensionarterialalta" id="tensionarterialaltano" value="No" {{ ($anamnesis->tensionarterialalta=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="tensionarterialaltano">No</label>
                        </div>
                    </td>
                    <th scope="row">Sufre de herpes o afta recurrentes?</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="herpes" id="herpessi" value="Si" {{ ($anamnesis->herpes=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="herpessi">Si</label>
                        </div> 
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="herpes" id="herpesno" value="No" {{ ($anamnesis->herpes=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="herpesno">No</label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">- Baja</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="tensionarterialbaja" id="tensionarterialbajasi" value="Si" {{ ($anamnesis->tensionarterialbaja=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="tensionarterialbajasi">Si</label>
                        </div> 
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="tensionarterialbaja" id="tensionarterialbajano" value="No" {{ ($anamnesis->tensionarterialbaja=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="tensionarterialbajano">No</label>
                        </div>
                    </td>
                    <th scope="row">Presenta alguno de los siguientes hábitos:</th>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th scope="row">Sangra excesivamente al cortase?</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="sangraexcesivamente" id="sangraexcesivamentesi" value="Si" {{ ($anamnesis->sangraexcesivamente=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="sangraexcesivamentesi">Si</label>
                        </div> 
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="sangraexcesivamente" id="sangraexcesivamenteno" value="No" {{ ($anamnesis->sangraexcesivamente=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="sangraexcesivamenteno">No</label>
                        </div>
                    </td>
                    <th scope="row">- Morderse las uñas o labios?</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="morderunaslabios" id="morderunaslabiossi" value="Si" {{ ($anamnesis->morderunaslabios=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="morderunaslabiossi">Si</label>
                        </div> 
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="morderunaslabios" id="morderunaslabiosno" value="No" {{ ($anamnesis->morderunaslabios=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="morderunaslabiosno">No</label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Padece o a padecido algún problema sanguíneo?</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="problemasanguineo" id="problemasanguineosi" value="Si" {{ ($anamnesis->problemasanguineo=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="problemasanguineosi">Si</label>
                        </div>
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="problemasanguineo" id="problemasanguineono" value="No" {{ ($anamnesis->problemasanguineo=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="problemasanguineono">No</label>
                        </div>
                    </td>
                    <th scope="row">- Fumas ? # Cigarrillos diarios: <input type="number" id="no_cigarrillos" name="no_cigarrillos" class="form-control" placeholder="# cigarrillos diarios" value="{{ $anamnesis->no_cigarrillos }}"></th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="fumas" id="fumassi" value="Si" {{ ($anamnesis->fumas=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="fumassi">Si</label>
                        </div>
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="fumas" id="fumasno" value="No" {{ ($anamnesis->fumas=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="fumasno">No</label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="1" id="anemia" name="anemia" {{ ($anamnesis->anemia=="1")? "checked" : "" }}>
                            <label class="form-check-label" for="anemia">-Anemia</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="1" id="leucemia" name="leucemia" {{ ($anamnesis->leucemia=="1")? "checked" : "" }}>
                            <label class="form-check-label" for="leucemia">-Leucemia</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="1" id="hemofilia" name="hemofilia" {{ ($anamnesis->hemofilia=="1")? "checked" : "" }}>
                            <label class="form-check-label" for="hemofilia">-Hemofilia</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="1" id="deficit_vit_k" name="deficit_vit_k" {{ ($anamnesis->deficit_vit_k=="1")? "checked" : "" }}>
                            <label class="form-check-label" for="deficit_vit_k">-Déficit Vit. k</label>
                        </div>
                    </th>
                    <td></td>
                    <td></td>
                    <th scope="row">Consume alimentos cítricos?</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="alimentoscitricos" id="alimentoscitricossi" value="Si" {{ ($anamnesis->alimentoscitricos=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="alimentoscitricossi">Si</label>
                        </div>
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="alimentoscitricos" id="alimentoscitricosno" value="No" {{ ($anamnesis->alimentoscitricos=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="alimentoscitricosno">No</label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Es udted V.I.H. +?</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="vih" id="vihsi" value="Si" {{ ($anamnesis->vih=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="vihsi">Si</label>
                        </div>
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="vih" id="vihno" value="No" {{ ($anamnesis->vih=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="vihno">No</label>
                        </div>
                    </td>
                    <th scope="row">Muerde objetos con los dientes?</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="muerdeobjetosdientes" id="muerdeobjetosdientessi" value="Si" {{ ($anamnesis->muerdeobjetosdientes=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="muerdeobjetosdientessi">Si</label>
                        </div>
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="muerdeobjetosdientes" id="muerdeobjetosdientesno" value="No" {{ ($anamnesis->muerdeobjetosdientes=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="muerdeobjetosdientesno">No</label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Toma algún medicamento retroviral</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="medicamentoretoviral" id="medicamentoretoviralsi" value="Si" {{ ($anamnesis->medicamentoretoviral=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="medicamentoretoviralsi">Si</label>
                        </div>
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="medicamentoretoviral" id="medicamentoretoviralno" value="No" {{ ($anamnesis->medicamentoretoviral=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="medicamentoretoviralno">No</label>
                        </div>
                    </td>
                    <th scope="row">- Apretamiento dentario</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="apretamientodentario" id="apretamientodentariosi" value="Si" {{ ($anamnesis->apretamientodentario=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="apretamientodentariosi">Si</label>
                        </div>
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="apretamientodentario" id="apretamientodentariono" value="No" {{ ($anamnesis->apretamientodentario=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="apretamientodentariono">No</label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Esta usted embarazada?</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="embarazada" id="embarazadasi" value="Si" {{ ($anamnesis->embarazada=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="embarazadasi">Si</label>
                        </div>
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="embarazada" id="embarazadano" value="No" {{ ($anamnesis->embarazada=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="embarazadano">No</label>
                        </div>
                    </td>
                    <th scope="row">- Respiración bucal</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="respiracionbucal" id="respiracionbucalsi" value="Si" {{ ($anamnesis->respiracionbucal=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="respiracionbucalsi">Si</label>
                        </div>
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="respiracionbucal" id="respiracionbucalno" value="No" {{ ($anamnesis->respiracionbucal=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="respiracionbucalno">No</label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Esta tomando actalmente pastillas anticonceptivas?</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="pastillasanticonceptivas" id="pastillasanticonceptivassi" value="Si" {{ ($anamnesis->pastillasanticonceptivas=="Si")? "checked" : "" }}>
                            <label class="form-check-label" for="pastillasanticonceptivassi">Si</label>
                        </div>
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="pastillasanticonceptivas" id="pastillasanticonceptivasno" value="No" {{ ($anamnesis->pastillasanticonceptivas=="No")? "checked" : "" }}>
                            <label class="form-check-label" for="pastillasanticonceptivasno">No</label>
                        </div>
                    </td>
                    <th scope="row">Observaciones</th>
                    <td colspan="2">
                        <textarea class="form-control" name="observaciones" id="observaciones">{{ $anamnesis->observaciones }}</textarea>
                    </td>
                </tr>             
               
               
            </tbody>
        </table>         

        <div class="col-12">
            <button type="submit" class="btn btn-primary me-1 waves-effect waves-float waves-light ajax" id="submit"><i id="ajax-icon" class="fa fa-save"></i> Guardar</button>
        </div>
    </div>
</form>

@push('scripts')

<script src="{{ asset('js/admin/anamnesis/edit.js') }}"></script>
@endpush