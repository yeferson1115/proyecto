<!--Modal Agregar hallazgos-->
<div class="modal fade" id="ModalAgregarHallazgo" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Agregar Hallazgo</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form class="form" id="FormHistoriaMovimientoAgregarHallazgo" action="javascript:void(0)" autocomplete="off">
        <input type="hidden" id="_url" value="{{ url('customerodontogram') }}">
        <input type="hidden" id="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="hallazgo">
        <input type="hidden" name="estado">
        <input type="hidden" name="paciente" value="{{$customer->id}}">
        <input type="hidden" name="marcas" value="0">
        <input type="hidden" name="tipoOdontograma" value="Inicial">
        <input type="hidden" name="customer_id" value="{{$customer->id}}">
       
        <div class="modal-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label">Hallazgo</label>
                <input type="text" id="modalHallazgo" class="form-control" readonly>
              </div>
            </div>
            <div class="col-md-3" id="colCategoria">
              <div class="form-group">
                <label class="control-label">Categoria</label>
                <input type="text" name="categoria" class="form-control" readonly>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label class="control-label">N° de Diente</label>
                <input type="text" name="diente" class="form-control" readonly>
              </div>
            </div>
            <div class="col-md-3" id="colDienteFinal" style="display: none">
              <div class="form-group">
                <label class="control-label">N° Diente Final</label>
                <input type="text" name="dienteFinal" class="form-control" readonly>
              </div>
            </div>
            <div class="col-md-3" id="colEstado">
              <div class="form-group">
                <label class="control-label">Estado</label>
                <input type="text" id="modalEstado" class="form-control" readonly>
              </div>
            </div>
            <div class="col-md-3" id="colSigla">
              <div class="form-group">
                <label class="control-label">Sigla</label>
                <input type="text" name="sigla" class="form-control" readonly>
              </div>
            </div>
          </div>
          <div id="FormMarcarPieza">
            <div class="row">
              <div class="col-md-12">
                <p><b>Seleccione las partes</b></p>
              </div>
            </div> 
            <div class="row">
              <div class="col-md-3">
                <div id="dibujar">
                  <div id="pieza">
                    <img src="{{ asset('assets/images/odontograma/images/pieza_tres.png') }}">
                    <div class="direccionPieza" id="direccionPieza-arriba"></div>
                    <div class="direccionPieza" id="direccionPieza-abajo"></div>
                    <div class="direccionPieza" id="direccionPieza-izquierda"></div>
                    <div class="direccionPieza" id="direccionPieza-derecha"></div>
                    <div class="direccionPieza" id="direccionPieza-centro"></div>
                  </div>
                </div>
              </div>
              <div class="col-md-9">
                <div class="row">
                  <div class="col-md-12">
                    <label class="checkbox-inline">
                      <input id="CheckBoxVestibular" type="checkbox" name="Vestibular" value="Vestibular">
                      Vestibular
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="VestibularEstado" id="RadioVestibularBuenEstado" value="bueno"> Buen Estado
                    </label>
                    <label class="radio-inline" for="RadioVestibularMalEstado">
                      <input type="radio" name="VestibularEstado" id="RadioVestibularMalEstado" value="malo"> Mal Estado
                    </label>
                  </div>
                  <div class="col-md-12">
                    <label class="checkbox-inline" id="ContentCheckBoxPalatino">
                      <input id="CheckBoxPalatino" type="checkbox" name="Palatino" value="Palatino">
                      Palatino
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="PalatinoEstado" value="bueno"> Buen Estado
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="PalatinoEstado" value="malo"> Mal Estado
                    </label>
                  </div>
                  <div class="col-md-12">
                    <label class="checkbox-inline" for="CheckBoxLingual" id="ContentCheckBoxLingual">
                      <input id="CheckBoxLingual" type="checkbox" name="Lingual" value="Lingual">
                      Lingual
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="LingualEstado" value="bueno"> Buen Estado
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="LingualEstado" value="malo"> Mal Estado
                    </label>
                  </div>
                  <div class="col-md-12">
                    <label class="checkbox-inline">
                      <input id="CheckBoxDistal" type="checkbox" name="Distal" value="Distal">
                      Distal
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="DistalEstado" value="bueno"> Buen Estado
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="DistalEstado" value="malo"> Mal Estado
                    </label>
                  </div>
                  <div class="col-md-12">
                    <label class="checkbox-inline">
                      <input id="CheckBoxMesial" type="checkbox" name="Mesial" value="Mesial">
                      Mesial
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="MesialEstado" value="bueno"> Buen Estado
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="MesialEstado" value="malo"> Mal Estado
                    </label>
                  </div>
                  <div class="col-md-12">
                    <label class="checkbox-inline" id="ContentCheckBoxOclusal">
                      <input id="CheckBoxOclusal" type="checkbox" name="Oclusal" value="Oclusal">
                      Oclusal
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="OclusalEstado" value="bueno"> Buen Estado
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="OclusalEstado" value="malo"> Mal Estado
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label class="control-label">Especificaciones</label>
                <textarea name="especificaciones" class="form-control" rows="5"></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
          <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Guardar</button>
        </div>
      </form>
      
    </div>
  </div>
</div>
