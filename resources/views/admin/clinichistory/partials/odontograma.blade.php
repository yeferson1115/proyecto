


<div class="panel-body">
    <div class="row">
      <div class="col-md-12">
         <div id="imprimir" style="width: 800px"></div>
      </div>
      <div class="col-md-8" id="OdontogramaImprimir">
        <div id="odontograma">
          <div id="odontograma-contenido" class="detalle" data-marcaClass="">
          <img src="{{ asset('images/plantilla_odontograma.png') }}" style="max-width: 100%;" />
            <div id="cursoresRecuadros">
              yefer
              
              @include('admin.clinichistory.partials.cursores')
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
      @include('admin.clinichistory.partials.odontogramanav')
      </div>
    </div>
  </div>
</div>



@include('admin.clinichistory.partials.modallistadehallazgos')

@include('admin.clinichistory.partials.modalagregarhallazgo')


<div id="ModalCapturarOdontograma" class="modal fade" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content ">
      <form id="FormGuardarCapturaOdontograma" action="" method="POST">
        <input type="hidden" name="paciente">
        <input type="hidden" name="tipo">
        <input type="hidden" name="imgData">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Odontograma Capturado</h4>
        </div>
        <div class="modal-body">
          <div id="ImagenOdontogramaCapturado">
            
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Cerrar</button>
          <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Guardar</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



@push('scripts')

{{-- Create the chart with javascript using canvas --}}
    <script>
        // Get Canvas element by its id
        employee_chart = document.getElementById('employee').getContext('2d');
        chart = new Chart(employee_chart,{
            type:'line',
            data:{
                labels:[
                    /*
                        this is blade templating.
                        we are getting the date by specifying the submonth
                     */
                    '{{Carbon\Carbon::now()->subMonths(3)->toFormattedDateString()}}',
                    '{{Carbon\Carbon::now()->subMonths(2)->toFormattedDateString()}}',
                    '{{Carbon\Carbon::now()->subMonths(1)->toFormattedDateString()}}',
                    '{{Carbon\Carbon::now()->subMonths(0)->toFormattedDateString()}}'
                    ],
                datasets:[{
                    label:'Usuarios guardados en los últimos 4 meses.',
                    data:[
                        
                        '{{$emp_count_4}}',
                        '{{$emp_count_3}}',
                        '{{$emp_count_2}}',
                        '{{$emp_count_1}}'
                    ],
                    backgroundColor: [
                        'rgba(178,235,242 ,1)'
                    ],
                    borderColor: [
                        'rgba(0,150,136 ,1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
    </script>
@endpush

