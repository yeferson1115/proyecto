<ul class="nav nav-tabs" id="myTab" role="tablist">
  @if($quote->meet_id!=null)
  <li class="nav-item" role="presentation">
    <button class="nav-link active" id="meet-tab" data-bs-toggle="tab" data-bs-target="#meet" type="button" role="tab" aria-controls="meet" aria-selected="true">Tele-consulta</button>
  </li>
  @endif
  <li class="nav-item" role="presentation">
    <button class="nav-link {{ $quote->meet_id === null ? 'active' : ''   }}" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Motivo Consulta</button>
  </li>
  <li class="nav-item" role="presentation">
    <button class="nav-link" id="anamnesis-tab" data-bs-toggle="tab" data-bs-target="#anamnesis" type="button" role="tab" aria-controls="anamnesis" aria-selected="false">Anamnesis</button>
  </li>
  <li class="nav-item" role="presentation">
    <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Examen Extra Oral</button>
  </li>
  <li class="nav-item" role="presentation">
    <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact" type="button" role="tab" aria-controls="contact" aria-selected="false">Examen Dental</button>
  </li>
  <li class="nav-item" role="presentation">
    <button class="nav-link" id="odontograma-tab" data-bs-toggle="tab" data-bs-target="#odontograma" type="button" role="tab" aria-controls="odontograma" aria-selected="false">Odontograma</button>
  </li>
</ul>
<div class="tab-content" id="myTabContent">
@if($quote->meet_id!=null)
  <div class="tab-pane fade show active" id="meet" role="tabpanel" aria-labelledby="meet-tab"> 
    <div id="meet"></div>
  </div>
  @endif
  <div class="tab-pane fade  {{ $quote->meet_id === null ? 'show active' : ''   }} " id="home" role="tabpanel" aria-labelledby="home-tab"> <p>{{$quote->reason}}</p></div>
  <div class="tab-pane fade " id="anamnesis" role="tabpanel" aria-labelledby="anamnesis-tab">
    @include('admin.clinichistory.partials.anamnesis',['anamnesis'=>$anamnesis])
  </div>
  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    @include('admin.clinichistory.partials.examenextraoral',['extraoral'=>$extraoral])
  </div>
  <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
    @include('admin.clinichistory.partials.examdental',['extraoral'=>$extraoral])
  </div>
  <div class="tab-pane fade" id="odontograma" role="tabpanel" aria-labelledby="odontograma-tab">
    @include('admin.clinichistory.partials.odontograma',['extraoral'=>$extraoral])
  </div>
</div>

@push('scripts')

<script src='https://meet.jit.si/external_api.js'></script>
<script>
   const domain = 'meet.jit.si';
    const options = {
        roomName: '{{$quote->meet_id}}',
        height: 700,
        parentNode: document.querySelector('#meet'),
        lang: 'es',
        userInfo: {
            email: '{{$quote->user->email}}',
            displayName: '{{$quote->user->name}} {{$quote->user->lastname}}'
        }
    }; 
    const api = new JitsiMeetExternalAPI(domain, options);
</script>
@endpush