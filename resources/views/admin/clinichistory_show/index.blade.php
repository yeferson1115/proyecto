@extends('layouts.admin')
@section('title','Historia Clinica')
@section('page_title', 'Historia Clinica')
@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-start mb-0">Historia Clinica</h2>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="/customer">Inicio &nbsp; &nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                        </li>
                        <li class="breadcrumb-item active">Historia Clinica</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
        <div class="mb-1 breadcrumb-right">
            <div class="dropdown">
                <a href="{{ url('customers/create') }}" class="btn btn-success waves-effect waves-float waves-light"><i data-feather='user-plus'></i> Nuevo cliente</a>

            </div>
        </div>
    </div>
</div>

<div class="content-body">
    <section id="multiple-column-form">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Historia Clinica</h4>
                    </div>
                    <div class="card-body" id="HistoriaContenido" data-paciente="{{$history->customer->id}}">
                  
                    

                    <div class="d-flex align-items-start" >
                        <div class="nav flex-column nav-pills me-2 w20" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <img src="{{ asset('images/usuario_inicio.png') }}" style="max-width: 100%;" />
                            <p>{{$history->customer->first_name}} {{$history->customer->last_name}}</p>
                            <button class="nav-link active" id="v-pills-customer-tab" data-bs-toggle="pill" data-bs-target="#v-pills-customer" type="button" role="tab" aria-controls="v-pills-customer" aria-selected="true"><i class="fa fa-user" aria-hidden="true"></i> Datos Paciente</button>
                            <button class="nav-link" id="v-pills-profile-tab" data-bs-toggle="pill" data-bs-target="#v-pills-profile" type="button" role="tab" aria-controls="v-pills-profile" aria-selected="false"><i class="fa fa-history" aria-hidden="true"></i> Historia Clínica</button>
                            <button class="nav-link" id="v-pills-formulacion-tab" data-bs-toggle="pill" data-bs-target="#v-pills-formulacion" type="button" role="tab" aria-controls="v-pills-formulacion" aria-selected="false"><i class="fa fa-list-alt" aria-hidden="true"></i> Formulación</button>
                            <button class="nav-link" id="v-pills-settings-tab" data-bs-toggle="pill" data-bs-target="#v-pills-settings" type="button" role="tab" aria-controls="v-pills-settings" aria-selected="false">Tratamientos</button>
                        </div>

                        <div class="tab-content w80" id="v-pills-tabContent">
                            <div class="tab-pane fade show active" id="v-pills-customer" role="tabpanel" aria-labelledby="v-pills-customer-tab">
                                @include('admin.clinichistory_show.partials.form', ['customer' => $history->customer])
                            </div>
                            <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                @include('admin.clinichistory_show.partials.tapshistory', ['customer' => $history->customer,'anamnesis'=>$history->anamnesis,'extraoral'=>$history->extraoralexam,'dentalexam'=>$history->dentalexam,'history'=>$history])
                            </div>
                            <div class="tab-pane fade" id="v-pills-formulacion" role="tabpanel" aria-labelledby="v-pills-formulacion-tab">
                                sadada
                            </div>
                            
                            <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">...</div>
                        </div>
                    </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection

@push('scripts')
<script>
    $('.delete-user').click(function(e){

        e.preventDefault();
        var _target=e.target;
        let href = $(this).attr('data-attr');// Don't post the form, unless confirmed
        let token = $(this).attr('data-token');
        var data=$(e.target).closest('form').serialize();
        Swal.fire({
        title: 'Seguro que desea eliminar el cliente?',
        text: "",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar',
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
              url: href,
              headers: {'X-CSRF-TOKEN': token},
              type: 'DELETE',
              cache: false,
    	      data: data,
              success: function (response) {
                var json = $.parseJSON(response);
                console.log(json);
                Swal.fire(
                    'Muy bien!',
                    'Cliente eliminado correctamente',
                    'success'
                    ).then((result) => {
                        location.reload();
                    });

              },error: function (data) {
                var errors = data.responseJSON;
                console.log(errors);

              }
           });

        }
        })

    });
</script>

@endpush


