<div class="row">

    <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <button class="btn btn-primary active"  id="nav-listanamnesis-tab" data-bs-toggle="tab" data-bs-target="#nav-listanamnesis" type="button" role="tab" aria-controls="nav-listanamnesis" aria-selected="true">Lista de Anamnesis realizados</button>
            <button class="btn btn-primary" style="margin-left: 15px;" id="nav-newanamnesis-tab" data-bs-toggle="tab" data-bs-target="#nav-newanamnesis" type="button" role="tab" aria-controls="nav-newanamnesis" aria-selected="false">Nuevo Anamnesis</button>
           
        </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="nav-listanamnesis" role="tabpanel" aria-labelledby="nav-listanamnesis-tab">
            <div class="table-responsive">
                <table class="table datatables" >
                                <thead class="table-light">
                                    <tr >
                                        <th class="sorting">#</th>
                                        <th class="sorting">Opciones</th>                                        
                                        <th class="sorting">Anamnesis</th>
                                        <th class="sorting">Fecha</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($anamnesis as $key=>$item)
                                    <tr class="odd row{{ $item->id }}">
                                        <td>{{ $key+1 }}</td>
                                        <td>
                                            <a  class="mb-1 btn btn-warning waves-effect waves-float waves-light" href="{{ url('quoteanamnesis', [Hashids::encode($item->id),'edit']) }}" title="Editar"><i data-feather='edit-3'></i> </a>                                          
                                        </td>                                        
                                        <td>Anamnesis - {{ $item->id }}</td>
                                        <td>{{ $item->created_at }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
        </div>
        <div class="tab-pane fade" id="nav-newanamnesis" role="tabpanel" aria-labelledby="nav-newanamnesis-tab">
            <h4>Nuevo Anamnesis</h4>
            <form class="form" role="form" id="main-form-anamnesis" autocomplete="off">
                    <input type="hidden" id="_url" value="{{ url('quoteanamnesis') }}">
                    <input type="hidden" id="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="customer_id" value="{{ $history->customer->id }}">
                    <input type="hidden" name="clinichistory_id" value="{{ $history->id }}">
                        <div class="row">
                            <div class="table-responsive mb-3">
                                <table class="table">
                                    <thead>
                                        <tr>
                                        <th scope="col"></th>
                                        <th scope="col">Si</th>
                                        <th scope="col">No</th>
                                        <th scope="col"></th>
                                        <th scope="col">Si</th>
                                        <th scope="col">No</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">Está usteded bajo tratamiento?</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="bajo_tratamiento" id="questionsi1" value="Si" >
                                                    <label class="form-check-label" for="questionsi1">Si</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="bajo_tratamiento" id="questionno1" value="No" >
                                                    <label class="form-check-label" for="questionno1">No</label>
                                                </div>
                                            </td>
                                            <th  scope="row">Sufre o ha sufrido:</th >
                                            <td></td>
                                            <td></td>
                                        </tr>

                                        <tr>
                                            <th scope="row">Toma actualmente algún medicamento?</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="toma_medicamento" id="questionsi2" value="Si" >
                                                    <label class="form-check-label" for="questionsi2">Si</label>
                                                </div>                      
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="toma_medicamento" id="questionno2" value="No" >
                                                    <label class="form-check-label" for="questionno2">No</label>
                                                </div>
                                            </td>
                                            <th  scope="row">- Enfermedad venérea</th >
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="enfermedad_venerea" id="enfermedad_venereasi" value="Si" >
                                                    <label class="form-check-label" for="enfermedad_venereasi">Si</label>
                                                </div> 
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="enfermedad_venerea" id="enfermedad_venereano" value="No" >
                                                    <label class="form-check-label" for="enfermedad_venereano">No</label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Le han practicado alguna intervención quirúrgica?</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="intervencion_quirurgica" id="questionsi3" value="Si" >
                                                    <label class="form-check-label" for="questionsi3">Si</label>
                                                </div>                       
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="intervencion_quirurgica" id="questionno3" value="No" >
                                                    <label class="form-check-label" for="questionno3">No</label>
                                                </div>
                                            </td>
                                            <th scope="row">- Problemas de Corazón</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="problemas_corazon" id="problemascorazonsi" value="Si" >
                                                    <label class="form-check-label" for="problemascorazonsi">Si</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="problemas_corazon" id="problemascorazonno" value="No" >
                                                    <label class="form-check-label" for="problemascorazonno">No</label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Ha recibido alguna transfusión sanguínea?</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="transfusion" id="questionsi4" value="Si" >
                                                    <label class="form-check-label" for="questionsi4">Si</label>
                                                </div>                      
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="transfusion" id="questionno4" value="No" >
                                                    <label class="form-check-label" for="questionno4">No</label>
                                                </div>
                                            </td>
                                            <th scope="row">- Hepatitis</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="hepatitis" id="hepatitissi" value="Si" >
                                                    <label class="form-check-label" for="hepatitissi">Si</label>
                                                </div>  
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="hepatitis" id="hepatitisno" value="No" >
                                                    <label class="form-check-label" for="hepatitisno">No</label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Ha consumido o consume droga?</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="consume_droga" id="questionsi5" value="Si" >
                                                    <label class="form-check-label" for="questionsi5">Si</label>
                                                </div>                       
                                            </td>
                                            <td> 
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="consume_droga" id="questionno5" value="No"  >
                                                    <label class="form-check-label" for="questionno5">No</label>
                                                </div>
                                            </td>
                                            <th scope="row">- Fiebre reumática</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="fiebre_reumatica" id="fiebrereumaticasi" value="Si" >
                                                    <label class="form-check-label" for="fiebrereumaticasi">Si</label>
                                                </div>   
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="fiebre_reumatica" id="fiebrereumaticano" value="No" >
                                                    <label class="form-check-label" for="fiebrereumaticano">No</label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Ha presentado reacción alérgica a:</th>
                                            <td></td>
                                            <td></td>
                                            <th scope="row">- Asma</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="asma" id="asmasi" value="Si" >
                                                    <label class="form-check-label" for="asmasi">Si</label>
                                                </div> 
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="asma" id="asmano" value="No" >
                                                    <label class="form-check-label" for="asmano">No</label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">- Penicilina</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="alergia_penicilina" id="questionsi6" value="Si" >
                                                    <label class="form-check-label" for="questionsi6">Si</label>
                                                </div>                        
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="alergia_penicilina" id="questionno6" value="No" >
                                                    <label class="form-check-label" for="questionno6">No</label>
                                                </div>
                                            </td>
                                            <th scope="row">- Diabetes</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="diabetes" id="diabetessi" value="Si" >
                                                    <label class="form-check-label" for="diabetessi">Si</label>
                                                </div>   
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="diabetes" id="diabetesno" value="No" >
                                                    <label class="form-check-label" for="diabetesno">No</label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">- Anestesia</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="alergia_anestesia" id="questionsi7" value="Si" >
                                                    <label class="form-check-label" for="questionsi7">Si</label>
                                                </div>
                                                
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="alergia_anestesia" id="questionno7" value="No" >
                                                    <label class="form-check-label" for="questionno7">No</label>
                                                </div>
                                            </td>
                                            <th scope="row">- Úlcera gástrica</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="ulcera_gastrica" id="ulceragatricasi" value="Si" >
                                                    <label class="form-check-label" for="ulceragatricasi">Si</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="ulcera_gastrica" id="ulceragatricano" value="No">
                                                    <label class="form-check-label" for="ulceragatricano">No</label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">- Aspirina yodo</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="alergia_aspirinayodo" id="questionsi8" value="Si" >
                                                    <label class="form-check-label" for="questionsi8">Si</label>
                                                </div>                      
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="alergia_aspirinayodo" id="questionno8" value="No">
                                                    <label class="form-check-label" for="questionno8">No</label>
                                                </div>
                                            </td>
                                            <th scope="row">- Tiroides</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="tiroides" id="tiroidessi" value="Si" >
                                                    <label class="form-check-label" for="tiroidessi">Si</label>
                                                </div> 
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="tiroides" id="tiroidesno" value="No" >
                                                    <label class="form-check-label" for="tiroidesno">No</label>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <th scope="row">- Merthiolate; Otros</th>
                                            <td colspan="2">
                                                <textarea class="form-control" name="merthiolate" id="merthiolate"></textarea>
                                            </td>
                                            <th scope="row">Ha tenido limitación al abrir o cerrar la boca?</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="limitacionabrircerrarboca" id="limitacionabrircerrarbocasi" value="Si" >
                                                    <label class="form-check-label" for="limitacionabrircerrarbocasi">Si</label>
                                                </div> 
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="limitacionabrircerrarboca" id="limitacionabrircerrarbocano" value="No" >
                                                    <label class="form-check-label" for="limitacionabrircerrarbocano">No</label>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <th scope="row">Sufre de tensión arterial?</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="tensionarterial" id="tensionarterialsi" value="Si" >
                                                    <label class="form-check-label" for="tensionarterialsi">Si</label>
                                                </div> 
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="tensionarterial" id="tensionarterialno" value="No"  >
                                                    <label class="form-check-label" for="tensionarterialno">No</label>
                                                </div>
                                            </td>
                                            <th scope="row">Siente ruidos en la mandibula al abrir o cerrar la boca?</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="ruidomandibula" id="ruidomandibulasi" value="Si" >
                                                    <label class="form-check-label" for="ruidomandibulasi">Si</label>
                                                </div> 
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="ruidomandibula" id="ruidomandibulano" value="No" >
                                                    <label class="form-check-label" for="ruidomandibulano">No</label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">- Alta</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="tensionarterialalta" id="tensionarterialaltasi" value="Si" >
                                                    <label class="form-check-label" for="tensionarterialaltasi">Si</label>
                                                </div> 
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="tensionarterialalta" id="tensionarterialaltano" value="No" >
                                                    <label class="form-check-label" for="tensionarterialaltano">No</label>
                                                </div>
                                            </td>
                                            <th scope="row">Sufre de herpes o afta recurrentes?</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="herpes" id="herpessi" value="Si" >
                                                    <label class="form-check-label" for="herpessi">Si</label>
                                                </div> 
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="herpes" id="herpesno" value="No" >
                                                    <label class="form-check-label" for="herpesno">No</label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">- Baja</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="tensionarterialbaja" id="tensionarterialbajasi" value="Si" >
                                                    <label class="form-check-label" for="tensionarterialbajasi">Si</label>
                                                </div> 
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="tensionarterialbaja" id="tensionarterialbajano" value="No" >
                                                    <label class="form-check-label" for="tensionarterialbajano">No</label>
                                                </div>
                                            </td>
                                            <th scope="row">Presenta alguno de los siguientes hábitos:</th>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Sangra excesivamente al cortase?</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="sangraexcesivamente" id="sangraexcesivamentesi" value="Si" >
                                                    <label class="form-check-label" for="sangraexcesivamentesi">Si</label>
                                                </div> 
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="sangraexcesivamente" id="sangraexcesivamenteno" value="No" >
                                                    <label class="form-check-label" for="sangraexcesivamenteno">No</label>
                                                </div>
                                            </td>
                                            <th scope="row">- Morderse las uñas o labios?</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="morderunaslabios" id="morderunaslabiossi" value="Si" >
                                                    <label class="form-check-label" for="morderunaslabiossi">Si</label>
                                                </div> 
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="morderunaslabios" id="morderunaslabiosno" value="No" >
                                                    <label class="form-check-label" for="morderunaslabiosno">No</label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Padece o a padecido algún problema sanguíneo?</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="problemasanguineo" id="problemasanguineosi" value="Si" >
                                                    <label class="form-check-label" for="problemasanguineosi">Si</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="problemasanguineo" id="problemasanguineono" value="No" >
                                                    <label class="form-check-label" for="problemasanguineono">No</label>
                                                </div>
                                            </td>
                                            <th scope="row">- Fumas ? # Cigarrillos diarios: <input type="number" id="no_cigarrillos" name="no_cigarrillos" class="form-control" placeholder="# cigarrillos diarios" ></th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="fumas" id="fumassi" value="Si" >
                                                    <label class="form-check-label" for="fumassi">Si</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="fumas" id="fumasno" value="No" >
                                                    <label class="form-check-label" for="fumasno">No</label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" value="1" id="anemia" name="anemia" >
                                                    <label class="form-check-label" for="anemia">-Anemia</label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" value="1" id="leucemia" name="leucemia" >
                                                    <label class="form-check-label" for="leucemia">-Leucemia</label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" value="1" id="hemofilia" name="hemofilia" >
                                                    <label class="form-check-label" for="hemofilia">-Hemofilia</label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" value="1" id="deficit_vit_k" name="deficit_vit_k" >
                                                    <label class="form-check-label" for="deficit_vit_k">-Déficit Vit. k</label>
                                                </div>
                                            </th>
                                            <td></td>
                                            <td></td>
                                            <th scope="row">Consume alimentos cítricos?</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="alimentoscitricos" id="alimentoscitricossi" value="Si" >
                                                    <label class="form-check-label" for="alimentoscitricossi">Si</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="alimentoscitricos" id="alimentoscitricosno" value="No" >
                                                    <label class="form-check-label" for="alimentoscitricosno">No</label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Es udted V.I.H. +?</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="vih" id="vihsi" value="Si" >
                                                    <label class="form-check-label" for="vihsi">Si</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="vih" id="vihno" value="No" >
                                                    <label class="form-check-label" for="vihno">No</label>
                                                </div>
                                            </td>
                                            <th scope="row">Muerde objetos con los dientes?</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="muerdeobjetosdientes" id="muerdeobjetosdientessi" value="Si" >
                                                    <label class="form-check-label" for="muerdeobjetosdientessi">Si</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="muerdeobjetosdientes" id="muerdeobjetosdientesno" value="No" >
                                                    <label class="form-check-label" for="muerdeobjetosdientesno">No</label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Toma algún medicamento retroviral</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="medicamentoretoviral" id="medicamentoretoviralsi" value="Si" >
                                                    <label class="form-check-label" for="medicamentoretoviralsi">Si</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="medicamentoretoviral" id="medicamentoretoviralno" value="No" >
                                                    <label class="form-check-label" for="medicamentoretoviralno">No</label>
                                                </div>
                                            </td>
                                            <th scope="row">- Apretamiento dentario</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="apretamientodentario" id="apretamientodentariosi" value="Si" >
                                                    <label class="form-check-label" for="apretamientodentariosi">Si</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="apretamientodentario" id="apretamientodentariono" value="No" >
                                                    <label class="form-check-label" for="apretamientodentariono">No</label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Esta usted embarazada?</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="embarazada" id="embarazadasi" value="Si" >
                                                    <label class="form-check-label" for="embarazadasi">Si</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="embarazada" id="embarazadano" value="No" >
                                                    <label class="form-check-label" for="embarazadano">No</label>
                                                </div>
                                            </td>
                                            <th scope="row">- Respiración bucal</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="respiracionbucal" id="respiracionbucalsi" value="Si" >
                                                    <label class="form-check-label" for="respiracionbucalsi">Si</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="respiracionbucal" id="respiracionbucalno" value="No" >
                                                    <label class="form-check-label" for="respiracionbucalno">No</label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Esta tomando actalmente pastillas anticonceptivas?</th>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="pastillasanticonceptivas" id="pastillasanticonceptivassi" value="Si" >
                                                    <label class="form-check-label" for="pastillasanticonceptivassi">Si</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="pastillasanticonceptivas" id="pastillasanticonceptivasno" value="No" >
                                                    <label class="form-check-label" for="pastillasanticonceptivasno">No</label>
                                                </div>
                                            </td>
                                            <th scope="row">Observaciones</th>
                                            <td colspan="2">
                                                <textarea class="form-control" name="observaciones" id="observaciones"></textarea>
                                            </td>
                                        </tr>             
                                    
                                    
                                    </tbody>
                                </table>  
                            </div>       

                        <div class="col-12">
                            <button type="submit" class="btn btn-primary me-1 waves-effect waves-float waves-light ajax" id="submit"><i id="ajax-icon" class="fa fa-save"></i> Guardar</button>
                        </div>
                    </div>
                </form>
        </div>
    </div>


</div>

@push('scripts')

<script src="{{ asset('js/admin/anamnesis/create.js') }}"></script>
@endpush