<div class="row">

    <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <button class="btn btn-primary active"  id="nav-listdentalexam-tab" data-bs-toggle="tab" data-bs-target="#nav-listdentalexam" type="button" role="tab" aria-controls="nav-listdentalexam" aria-selected="true">Lista de examenes dentales realizados</button>
            <button class="btn btn-primary" style="margin-left: 15px;" id="nav-newexamdental-tab" data-bs-toggle="tab" data-bs-target="#nav-newexamdental" type="button" role="tab" aria-controls="nav-newexamdental" aria-selected="false">Nuevo Examén Dental</button>
           
        </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="nav-listdentalexam" role="tabpanel" aria-labelledby="nav-listdentalexam-tab">
            <div class="table-responsive">
                <table class="table datatables" >
                                <thead class="table-light">
                                    <tr >
                                        <th class="sorting">#</th>
                                        <th class="sorting">Opciones</th>                                        
                                        <th class="sorting">Examen Dental</th>
                                        <th class="sorting">Fecha</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($dentalexam as $key=>$item)
                                    <tr class="odd row{{ $item->id }}">
                                        <td>{{ $key+1 }}</td>
                                        <td>
                                            <a  class="mb-1 btn btn-warning waves-effect waves-float waves-light" href="{{ url('dentalexam', [Hashids::encode($item->id),'edit']) }}" title="Editar"><i data-feather='edit-3'></i> </a>
                                          
                                        </td>                                        
                                        <td>Examen Dental - {{ $item->id }}</td>
                                        <td>{{ $item->created_at }}</td>                                      

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
        </div>
        <div class="tab-pane fade" id="nav-newexamdental" role="tabpanel" aria-labelledby="nav-newexamdental-tab">
            <h4>Nuevo examén dental</h4>

            <form class="form" role="form" id="main-form-dentalexam" autocomplete="off">
                <input type="hidden" id="_url" value="{{ url('dentalexam') }}">
                <input type="hidden" id="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="customer_id" value="{{ $history->customer->id }}">
                <input type="hidden" name="clinichistory_id" value="{{ $history->id }}">
                    <div class="row">
                    <table class="table">
                        <thead>
                            <tr>
                            <th scope="col">EXAMEN DENTAL</th>
                            <th scope="col">Si</th>
                            <th scope="col">No</th>
                            <th scope="col">EXAMEN PERIODENTAL</th>
                            <th scope="col">Si</th>
                            <th scope="col">No</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">Supernumerarios</th>
                                <td>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="supernumerarios" id="questionsi1" value="Si" >
                                        <label class="form-check-label" for="questionsi1"></label>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="supernumerarios" id="questionno1" value="No" >
                                        <label class="form-check-label" for="questionno1"></label>
                                    </div>
                                </td>
                                <th  scope="row">Placa Blanda</th >
                                <td>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="placa_blanda" id="questionsi2" value="Si" >
                                        <label class="form-check-label" for="questionsi1"></label>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="placa_blanda" id="questionsi2" value="No" >
                                        <label class="form-check-label" for="questionsi1"></label>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <th scope="row">Abrasiones</th>
                                <td>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="abrasiones" id="questionsi3" value="Si" >
                                        <label class="form-check-label" for="questionsi2"></label>
                                    </div>                      
                                </td>
                                <td>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="abrasiones" id="questionno3" value="No" >
                                        <label class="form-check-label" for="questionno2"></label>
                                    </div>
                                </td>
                                <th  scope="row">Placa Calcificada</th >
                                <td>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="placa_calcificada" id="placa_calcificadasi" value="Si" >
                                        <label class="form-check-label" for="placa_calcificadasi"></label>
                                    </div> 
                                </td>
                                <td>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="placa_calcificada" id="placa_calcificadano" value="No" >
                                        <label class="form-check-label" for="enfermedad_venereano"></label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Erosiones</th>
                                <td>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="erosiones" value="Si" >
                                    </div>                       
                                </td>
                                <td>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="erosiones"  value="No" >
                                    </div>
                                </td>
                                <th scope="row">Movilidad Dental</th>
                                <td>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="movilidad_dental"  value="Si" >
                                    </div>
                                </td>
                                <td>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="movilidad_dental"  value="No" >
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Manchas</th>
                                <td>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="manchas"  value="Si" >
                                    </div>                      
                                </td>
                                <td>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="manchas"  value="No" >
                                    </div>
                                </td>
                                <th scope="row">Bolsas Periodontales</th>
                                <td>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="bolsas_periodontales" id="bolsas_periodontalessi" value="Si" >
                                    </div>  
                                </td>
                                <td>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="bolsas_periodontales" id="bolsas_periodontalesno" value="No" >
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Patología Pulpar</th>
                                <td>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="patalogia_pulpar" id="questionsi5" value="Si">
                                    </div>                       
                                </td>
                                <td> 
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="patalogia_pulpar" id="questionno5" value="No" >
                                    </div>
                                </td>
                                <th scope="row">Retracción Gingival</th>
                                <td>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="retraccion_gingival" id="fiebrereumaticasi" value="Si" >
                                    </div>   
                                </td>
                                <td>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="retraccion_gingival" id="fiebrereumaticano" value="No" >
                                    </div>
                                </td>
                            </tr>
                        
                        
                        </tbody>
                    </table>         
                    <div class="col-md-12 col-12">
                        <div class="mb-1">
                            <label class="form-label" for="last_name">Observaciones</label>
                            <textarea  id="observation" name="observation" class="form-control" ></textarea>
                            <span class="missing_alert text-danger" id="last_name_alert"></span>
                        </div>
                    </div>
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary me-1 waves-effect waves-float waves-light ajax" id="submit"><i id="ajax-icon" class="fa fa-save"></i> Guardar</button>
                    </div>
                </div>
            </form>
            
        </div>
    </div>


</div>


@push('scripts')

<script src="{{ asset('js/admin/dentalexam/create.js') }}"></script>
@endpush