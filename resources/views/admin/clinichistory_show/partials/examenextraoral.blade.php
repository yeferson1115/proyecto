<div class="row">

    <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <button class="btn btn-primary active"  id="nav-listaextraoral-tab" data-bs-toggle="tab" data-bs-target="#nav-listaextraoral" type="button" role="tab" aria-controls="nav-listaextraoral" aria-selected="true">Lista de examenes extra orales realizados</button>
            <button class="btn btn-primary" style="margin-left: 15px;" id="nav-newextraoral-tab" data-bs-toggle="tab" data-bs-target="#nav-newextraoral" type="button" role="tab" aria-controls="nav-newextraoral" aria-selected="false">Nuevo Examén Extra Oral</button>
           
        </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="nav-listaextraoral" role="tabpanel" aria-labelledby="nav-listaextraoral-tab">
            <div class="table-responsive">
                <table class="table datatables" >
                                <thead class="table-light">
                                    <tr >
                                        <th class="sorting">#</th>
                                        <th class="sorting">Opciones</th>                                        
                                        <th class="sorting">Extra oral</th>
                                        <th class="sorting">Fecha</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($extraoral as $key=>$item)
                                    <tr class="odd row{{ $item->id }}">
                                        <td>{{ $key+1 }}</td>
                                        <td>
                                            <a  class="mb-1 btn btn-warning waves-effect waves-float waves-light" href="{{ url('extraoralexam', [Hashids::encode($item->id),'edit']) }}" title="Editar"><i data-feather='edit-3'></i> </a>
                                          
                                        </td>                                        
                                        <td>Extra Oral - {{ $item->id }}</td>
                                        <td>{{ $item->created_at }}</td>                                      

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
        </div>
        <div class="tab-pane fade" id="nav-newextraoral" role="tabpanel" aria-labelledby="nav-newextraoral-tab">
            <h4>Nuevo examén extra oral</h4>

            <form class="form" role="form" id="main-form-extraoral" autocomplete="off">
                <input type="hidden" id="_url" value="{{ url('extraoralexam') }}">
                <input type="hidden" id="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="customer_id" value="{{ $history->customer->id }}">
                <input type="hidden" name="clinichistory_id" value="{{ $history->id }}">
                    <div class="row">
                    <div class="table-responsive mb-3">
                        <table class="table">
                            <thead>
                                <tr>
                                <th scope="col">EXAMEN EXTRAORAL</th>
                                <th scope="col">Normal</th>
                                <th scope="col">Anormal</th>
                                <th scope="col">EXAMEN INTRAORAL</th>
                                <th scope="col">Normal</th>
                                <th scope="col">Anormal</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">Apreciación generalizada del paciente</th>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="apreciacion_paciente" id="questionsi1" value="Normal" >
                                            <label class="form-check-label" for="questionsi1"></label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="apreciacion_paciente" id="questionno1" value="Anormal" >
                                            <label class="form-check-label" for="questionno1"></label>
                                        </div>
                                    </td>
                                    <th  scope="row">Labios y comisura labial</th >
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="labios_comisura_labial" id="questionsi2" value="Normal" >
                                            <label class="form-check-label" for="questionsi1"></label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="labios_comisura_labial" id="questionsi2" value="Anormal" >
                                            <label class="form-check-label" for="questionsi1"></label>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <th scope="row">Cabeza</th>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="cabeza" id="questionsi3" value="Normal" >
                                            <label class="form-check-label" for="questionsi2"></label>
                                        </div>                      
                                    </td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="cabeza" id="questionno3" value="Anormal" >
                                            <label class="form-check-label" for="questionno2"></label>
                                        </div>
                                    </td>
                                    <th  scope="row">Lengua</th >
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="lengua" id="lenguasi" value="Normal" >
                                            <label class="form-check-label" for="lenguasi"></label>
                                        </div> 
                                    </td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="lengua" id="lenguano" value="Anormal" >
                                            <label class="form-check-label" for="enfermedad_venereano"></label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Cara</th>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="cara" value="Normal" >
                                        </div>                       
                                    </td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="cara"  value="Anormal" >
                                        </div>
                                    </td>
                                    <th scope="row">Carrillos</th>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="carrillos"  value="Normal" >
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="carrillos"  value="Anormal" >
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Glándulas salivales (Parotida,submaxilar,sublingual)</th>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="glandulas_salivales"  value="Normal">
                                        </div>                      
                                    </td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="glandulas_salivales"  value="Anormal" >
                                        </div>
                                    </td>
                                    <th scope="row">Frenillos</th>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="frenillos" id="frenillossi" value="Normal" >
                                        </div>  
                                    </td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="frenillos" id="frenillosno" value="Anormal" >
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Cuello: Renglón hiodea y tiroidea</th>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="cuello" id="questionsi5" value="Normal" >
                                        </div>                       
                                    </td>
                                    <td> 
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="cuello" id="questionno5" value="Anormal" >
                                        </div>
                                    </td>
                                    <th scope="row">Piso de boca</th>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="piso_boca" id="fiebrereumaticasi" value="Normal" >
                                        </div>   
                                    </td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="piso_boca" id="fiebrereumaticano" value="Anormal" >
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Cadena ganglionar (auricular posterios,preauricular submandibular,cervical anterior, cervical posterior)</th>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="cadena_danglionar"  value="Normal" >
                                        </div> 
                                    </td>
                                    <td>
                                    <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="cadena_danglionar"  value="Anormal" >
                                        </div> 
                                    </td>
                                    <th scope="row">Paladar duro</th>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="paladar_duro" id="paladar_durosi" value="Normal" >
                                        </div> 
                                    </td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="paladar_duro" id="paladar_durono" value="Anormal" >
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">ATM</th>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="atm" id="questionsi6" value="Normal" >
                                        </div>                        
                                    </td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="atm" id="questionno6" value="Anormal" >
                                        </div>
                                    </td>
                                    <th scope="row">Paladar blando</th>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="paladar_blando"  value="Normal" >
                                        </div>   
                                    </td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="paladar_blando"  value="Anormal">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row"></th>
                                    <td>                       
                                    </td>
                                    <td>
                                    </td>
                                    <th scope="row">Región retromolar</th>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="region_retromolar" value="Normal"  >
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="region_retromolar"  value="Anormal" >
                                        </div>
                                    </td>
                                </tr>
                            
                            
                            </tbody>
                        </table> 
                    </div>        

                    <div class="col-12">
                        <button type="submit" class="btn btn-primary me-1 waves-effect waves-float waves-light ajax" id="submit"><i id="ajax-icon" class="fa fa-save"></i> Guardar</button>
                    </div>
                </div>
            </form>
            
        </div>
    </div>


</div>

@push('scripts')

<script src="{{ asset('js/admin/extraoral/create.js') }}"></script>
@endpush