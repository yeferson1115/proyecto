<!-- Modal -->
<div class="modal fade" id="ModalOdontogramaDetalle" tabindex="-1" aria-labelledby="ModalOdontogramaDetalleLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ModalOdontogramaDetalleLabel">Lista de Hallazgos</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                    <th>Nombre</th>
                    <th>Categoria</th>
                    <th>Diente</th>
                    <th>Diente Final</th>
                    <th>Estado</th>
                    <th>Dibujo</th>
                    <th>Especificaciones</th>
                    <th></th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
