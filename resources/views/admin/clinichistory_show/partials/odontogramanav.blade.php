<style>
.nav>li>a{
  padding: 5px 10px;
}
.odontograma-navegacion{
  list-style: none;
  padding: 0;
  margin: 0;
  margin-top: 10px;
  cursor: pointer;
}

.odontograma-navegacion a,.dropdown-menu>li>a{
  color: #333;
}

.odontograma-navegacion>li{
  padding: 2px;
  background: none;
}

.odontograma-navegacion>li button{
  border: none;
  background: none;
  padding: 0;
}

.odontograma-navegacion .dropdown-menu{
  padding: 0;
}

.odontograma-navegacion .dropdown-menu li{
  padding: 0;
}

.odontograma-navegacion .dropdown-menu li>a>i.buen{
  color: #00D900;
}
.odontograma-navegacion .dropdown-menu li>a>i.mal{
  color: #FF0000;
}

.dropdown-submenu {
    position: relative;
}

.dropdown-submenu>.dropdown-menu {
    top: 0;
    left: -160px;
    margin-top: -6px;
    margin-left: -1px;
    -webkit-border-radius: 0 6px 6px 6px;
    -moz-border-radius: 0 6px 6px;
    border-radius: 0 6px 6px 6px;
}

.dropdown-submenu:hover>.dropdown-menu {
    display: block;
}

.dropdown-submenu>a:after {
    display: block;
    content: " ";
    float: left;
    width: 0;
    height: 0;
    left: 10px;
    top: 3px;
    border-color: transparent;
    border-style: solid;
    border-width: 5px 5px 5px 0;
    border-right-color: #333;
    margin-top: 5px;
    margin-right: -10px;
    position: absolute;
}

.dropdown-submenu:hover>a:after {
    border-left-color: #333;
}

.dropdown-submenu.pull-left {
    float: none;
}

.dropdown-submenu.pull-left>.dropdown-menu {
    left: -100%;
    margin-left: 10px;
    -webkit-border-radius: 6px 0 6px 6px;
    -moz-border-radius: 6px 0 6px 6px;
    border-radius: 6px 0 6px 6px;
}
</style>
<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item" role="presentation">
    <button class="nav-link active" id="a_i-tab" data-bs-toggle="tab" data-bs-target="#a_i" type="button" role="tab" aria-controls="a_i" aria-selected="true">A-I</button>
  </li>
  <li class="nav-item" role="presentation">
    <button class="nav-link" id="l_p-tab" data-bs-toggle="tab" data-bs-target="#l_p" type="button" role="tab" aria-controls="l_p" aria-selected="false">L-P</button>
  </li>
  <li class="nav-item" role="presentation">
    <button class="nav-link" id="r_t-tab" data-bs-toggle="tab" data-bs-target="#r_t" type="button" role="tab" aria-controls="r_t" aria-selected="false">R-T</button>
  </li>
  <li class="nav-item" role="presentation">
    <button class="nav-link" id="detail-tab" data-bs-toggle="tab" data-bs-target="#detail" type="button" role="tab" aria-controls="detail" aria-selected="false">Detalle</button>
  </li>
</ul>
<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="a_i" role="tabpanel" aria-labelledby="a_i-tab">
    
  <ul class="odontograma-navegacion">
        <li class="dropdown">          
          <a class="btn btn-light dropdown-toggle rango nombreHallazgo" id="dropdownOortoFijo" data-hallazgo="1" data-bs-toggle="dropdown" aria-expanded="false">
            <img src="{{ asset('assets/images/odontograma/images/AparatoOrtoIcono.png') }}"> Aparato Orto. Fijo <i class="fa fa-caret-down" aria-hidden="true"></i>
          </a>
          <ul class="dropdown-menu" aria-labelledby="dropdownOortoFijo">
            <li><a href="#" data-estado="bueno" class="odontograma-item dropdown-item "><i class="fa fa-thumbs-o-up buen"></i>Buen Estado</a></li>
            <li><a href="#" data-estado="malo" class="odontograma-item dropdown-item "><i class="fa fa-thumbs-o-down mal"></i>Mal Estado</a></li>
          </ul>
        </li>
        <li class="dropdown">          
          <a class="btn btn-light dropdown-toggle rango nombreHallazgo" data-hallazgo="2" id="dropdownOrtoRemovible" data-bs-toggle="dropdown" aria-expanded="false">
            <img src="{{ asset('assets/images/odontograma/images/AparatoOrtodonticoRemovibleIcono.png') }}"> Aparato Orto. Removible <i class="fa fa-caret-down" aria-hidden="true"></i>
          </a>
          <ul class="dropdown-menu" aria-labelledby="dropdownOrtoRemovible">
            <li><a href="#" data-estado="bueno" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-up buen"></i>Buen Estado</a></li>
            <li><a href="#" data-estado="malo" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-down mal"></i>Mal Estado</a></li>
          </ul>
        </li>
        <li class="dropdown">          
          <a class="btn btn-light dropdown-toggle nombreHallazgo hallazgoMarcar" data-marcaclass="lesionCaries" id="dropdownLesiónCaries" data-hallazgo="4" data-bs-toggle="dropdown" aria-expanded="false">
            <img src="{{ asset('assets/images/odontograma/images/imgariosa.png') }}"> Caries Dental <i class="fa fa-caret-down" aria-hidden="true"></i>
          </a>
          <ul class="dropdown-menu" aria-labelledby="dropdownLesiónCaries">
            <li><a href="#" class="siglas odontograma-item dropdown-item" data-sigla="MB">MB</a></li>
            <li><a href="#" class="siglas odontograma-item dropdown-item" data-sigla="CE">CE</a></li>
            <li><a href="#" class="siglas odontograma-item dropdown-item" data-sigla="CD">CD</a></li>
            <li><a href="#" class="siglas odontograma-item dropdown-item" data-sigla="CDP">CDP</a></li>
          </ul>
        </li>
        
        <li class="dropdown">          
          <a class="btn btn-light dropdown-toggle" id="dropdownCoronaDefinitiva" data-bs-toggle="dropdown" aria-expanded="false">
            <img src="{{ asset('assets/images/odontograma/images/CoronaDefinitivaIcono.png') }}"> Corona <i class="fa fa-caret-down" aria-hidden="true"></i>
          </a>
          <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownCoronaDefinitiva">
            <li class="dropdown">
              <a tabindex="-1" id="corona1" class="nombreHallazgo dropdown-item" data-hallazgo="3" data-sigla="CM" href="#" data-bs-toggle="dropdown" aria-expanded="false"><b>CM:</b> Corona Metálica <i class="fa fa-caret-down" aria-hidden="true"></i></a>
              <ul class="dropdown-menu" aria-labelledby="corona1">
                <li><a href="#" data-estado="bueno" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-up buen"></i>Buen Estado</a></li>
                <li><a href="#" data-estado="malo" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-down mal"></i>Mal Estado</a></li>
              </ul>
            </li>

            <li class="dropdown">
              <a tabindex="-1" id="corona2" class="nombreHallazgo dropdown-item" data-hallazgo="3" data-sigla="CF"  href="#" data-bs-toggle="dropdown" aria-expanded="false"><b>CF:</b> Corona Fenestrada <i class="fa fa-caret-down" aria-hidden="true"></i></a>
              <ul class="dropdown-menu" aria-labelledby="corona2">
                <li><a href="#" data-estado="bueno" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-up buen"></i>Buen Estado</a></li>
                <li><a href="#" data-estado="malo" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-down mal"></i>Mal Estado</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a tabindex="-1" id="corona3" class="nombreHallazgo dropdown-item" data-hallazgo="3" data-sigla="CMC" href="#" data-bs-toggle="dropdown" aria-expanded="false"><b>CMC:</b> Corona Metal Cerámica <i class="fa fa-caret-down" aria-hidden="true"></i></a>
              <ul class="dropdown-menu" aria-labelledby="corona3">
                <li><a href="#" data-estado="bueno" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-up buen"></i>Buen Estado</a></li>
                <li><a href="#" data-estado="malo" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-down mal"></i>Mal Estado</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a tabindex="-1" id="corona4"  class="nombreHallazgo dropdown-item" data-hallazgo="3" data-sigla="CV" href="#" data-bs-toggle="dropdown" aria-expanded="false"><b>CV:</b> Corona Veneer <i class="fa fa-caret-down" aria-hidden="true"></i></a>
              <ul class="dropdown-menu" aria-labelledby="corona4">
                <li><a href="#" data-estado="bueno" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-up buen"></i>Buen Estado</a></li>
                <li><a href="#" data-estado="malo" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-down mal"></i>Mal Estado</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a tabindex="-1" id="corona5" class="nombreHallazgo dropdown-item" data-hallazgo="3" data-sigla="CJ" href="#" data-bs-toggle="dropdown" aria-expanded="false"><b>CJ:</b> Corona Jacket <i class="fa fa-caret-down" aria-hidden="true"></i></a>
              <ul class="dropdown-menu" aria-labelledby="corona5">
                <li><a href="#" data-estado="bueno" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-up buen"></i>Buen Estado</a></li>
                <li><a href="#" data-estado="malo"  class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-down mal"></i>Mal Estado</a></li>
              </ul>
            </li>
          </ul>
        </li>
        <li class="dropdown">          
          <a class="btn btn-light dropdown-toggle nombreHallazgo" id="dropdownCoronaTemporal" data-hallazgo="37" data-sigla="CT" data-bs-toggle="dropdown" aria-expanded="false">
            <img src="{{ asset('assets/images/odontograma/images/Coronatemp.png') }}">Corona Temporal  <i class="fa fa-caret-down" aria-hidden="true"></i>
          </a>
          <ul class="dropdown-menu" aria-labelledby="dropdownCoronaTemporal">
            <li><a href="#" data-estado="bueno" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-up buen"></i>Buen Estado</a></li>
            <li><a href="#" data-estado="malo" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-down mal"></i>Mal Estado</a></li>
          </ul>
        </li>
        <li class="dropdown">          
          <a class="btn btn-light dropdown-toggle" id="dropdownCoronaDefinitiva" data-bs-toggle="dropdown" aria-expanded="false">
          <img src="{{ asset('assets/images/odontograma/images/interrogacion.png') }}"> Defectos de Desarrollo del Esmalte  <i class="fa fa-caret-down" aria-hidden="true"></i>
          </a>
          <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownCoronaDefinitiva">
            <li class="dropdown">
              <a tabindex="-1" id="defectoesmalte1" class="nombreHallazgo dropdown-item" data-hallazgo="5" data-sigla="HP" href="#" data-bs-toggle="dropdown" aria-expanded="false"><b>HP:</b> Hipoplasia <i class="fa fa-caret-down" aria-hidden="true"></i></a>
              <ul class="dropdown-menu" aria-labelledby="defectoesmalte1">
                <li><a href="#" data-estado="bueno" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-up buen"></i>Buen Estado</a></li>
                <li><a href="#" data-estado="malo" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-down mal"></i>Mal Estado</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a tabindex="-1" id="defectoesmalte2" class="nombreHallazgo dropdown-item" data-hallazgo="5" data-sigla="HM"  href="#" data-bs-toggle="dropdown" aria-expanded="false"><b>HM:</b> Hipo Mineralización <i class="fa fa-caret-down" aria-hidden="true"></i></a>
              <ul class="dropdown-menu" aria-labelledby="defectoesmalte2">
                <li><a href="#" data-estado="bueno" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-up buen"></i>Buen Estado</a></li>
                <li><a href="#" data-estado="malo" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-down mal"></i>Mal Estado</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a tabindex="-1" id="defectoesmalte3" class="nombreHallazgo dropdown-item" data-hallazgo="5" data-sigla="O" href="#" data-bs-toggle="dropdown" aria-expanded="false"><b>O:</b> Opacidades del Esmalte <i class="fa fa-caret-down" aria-hidden="true"></i></a>
              <ul class="dropdown-menu" aria-labelledby="defectoesmalte3">
                <li><a href="#" data-estado="bueno" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-up buen"></i>Buen Estado</a></li>
                <li><a href="#" data-estado="malo" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-down mal"></i>Mal Estado</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a tabindex="-1" id="defectoesmalte4" class="nombreHallazgo dropdown-item" data-hallazgo="5" data-sigla="D" href="#" data-bs-toggle="dropdown" aria-expanded="false"><b>D:</b> Decoloración del Esmalte <i class="fa fa-caret-down" aria-hidden="true"></i></a>
              <ul class="dropdown-menu" aria-labelledby="defectoesmalte4">
                <li><a href="#" data-estado="bueno" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-up buen"></i>Buen Estado</a></li>
                <li><a href="#" data-estado="malo" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-down mal"></i>Mal Estado</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a tabindex="-1" id="defectoesmalte5" class="nombreHallazgo dropdown-item" data-hallazgo="5" data-sigla="Fluorosis" href="#" data-bs-toggle="dropdown" aria-expanded="false"><b>Fluorosis:</b> Fluorosis <i class="fa fa-caret-down" aria-hidden="true"></i></a>
              <ul class="dropdown-menu" aria-labelledby="defectoesmalte5">
                <li><a href="#" data-estado="bueno" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-up buen"></i>Buen Estado</a></li>
                <li><a href="#" data-estado="malo" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-down mal"></i>Mal Estado</a></li>
              </ul>
            </li>
          </ul>
        </li>
        <li>          
          <a class="btn btn-light dropdown-toggle nombreHallazgo odontograma-item" data-hallazgo="17">
          <img src="{{ asset('assets/images/odontograma/images/imgdiastema.png') }}"> Diastema
          </a>
        </li>
        
        <li>          
          <a class="btn btn-light dropdown-toggle rango nombreHallazgo odontograma-item" data-hallazgo="13">
          <img src="{{ asset('assets/images/odontograma/images/imgendtotal.png') }}"> Edentulo Total
          </a>
        </li>
        <li class="dropdown">          
          <a class="btn btn-light dropdown-toggle nombreHallazgo" id="dropdownMunon" data-hallazgo="30"  data-bs-toggle="dropdown" aria-expanded="false">
          <img src="{{ asset('assets/images/odontograma/images/resized_imgdiente.PNG') }}"> Espigo Muñon <i class="fa fa-caret-down" aria-hidden="true"></i>
          </a>
          <ul class="dropdown-menu" aria-labelledby="dropdownMunon">
            <li><a href="#" data-estado="bueno" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-up buen"></i>Buen Estado</a></li>
            <li><a href="#" data-estado="malo" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-down mal"></i>Mal Estado</a></li>
          </ul>
        </li>
        <li>         
          <a class="btn btn-light dropdown-toggle nombreHallazgo odontograma-item" data-hallazgo="8">
            <img src="{{ asset('assets/images/odontograma/images/resized_imgdiente.PNG') }}"> Fosas y Fisuras Profundas
          </a>
        </li>
        <li class="dropdown">          
          <a class="btn btn-light dropdown-toggle nombreHallazgo" id="dropdownFractura" data-hallazgo="7" data-bs-toggle="dropdown" aria-expanded="false">
           <img src="{{ asset('assets/images/odontograma/images/imgfractura.png') }}"> Fractura <i class="fa fa-caret-down" aria-hidden="true"></i>
          </a>
          <ul class="dropdown-menu" aria-labelledby="dropdownFractura">

            <li><a href="#" class="odontograma-item dropdown-item" data-categoria="Coronal"> <img src="{{ asset('assets/images/odontograma/images/imgfracturacorona.png') }}"> Fractura Coronal</a></li>
            <li><a href="#" class="odontograma-item dropdown-item" data-categoria="Incisal"><img src="{{ asset('assets/images/odontograma/images/imgfracturaincisal.png') }}"> Fractura Incisal</a></li>
            <li><a href="#" class="odontograma-item dropdown-item" data-categoria="Raiz y Coronal"><img src="{{ asset('assets/images/odontograma/images/imgfacturaiz.png') }}"> Fractura Raiz y Coronal</a></li>
          </ul>
        </li>

        <li>          
          <a class="btn btn-light dropdown-toggle nombreHallazgo odontograma-item" data-hallazgo="24">
            <img src="{{ asset('assets/images/odontograma/images/resized_imgdiente.PNG') }}"> Fusión
          </a>
        </li>
        <li>          
          <a class="btn btn-light dropdown-toggle nombreHallazgo odontograma-item" data-hallazgo="25">
            <img src="{{ asset('assets/images/odontograma/images/interrogacion.png') }}"> Geminasión
          </a>
        </li>
        <li class="dropdown">          
          <a class="btn btn-light dropdown-toggle nombreHallazgo" id="dropdownGiroversion" data-hallazgo="18" data-bs-toggle="dropdown" aria-expanded="false">
           <img src="{{ asset('assets/images/odontograma/images/resized_imgiroversion.PNG') }}"> Giroversión <i class="fa fa-caret-down" aria-hidden="true"></i>
          </a>
          <ul class="dropdown-menu" aria-labelledby="dropdownGiroversion">
            <li><a href="#" class="odontograma-item dropdown-item" data-categoria="Distal">Giroversion Distal</a></li>
            <li><a href="#" class="odontograma-item dropdown-item" data-categoria="Mesial">Giroversion Mesial</a></li>
          </ul>
        </li>
        <li>          
          <a class="btn btn-light dropdown-toggle nombreHallazgo odontograma-item" data-hallazgo="26">
           <img src="{{ asset('assets/images/odontograma/images/imgimpactacion.png') }}"> Impactación
          </a>
        </li>
        <li class="dropdown">          
          <a class="btn btn-light dropdown-toggle nombreHallazgo" id="dropdownImplanteDental" data-hallazgo="31"  data-bs-toggle="dropdown" aria-expanded="false">
           <img src="{{ asset('assets/images/odontograma/images/imgimplant.png') }}"> Implante Dental
            <i class="fa fa-caret-down" aria-hidden="true"></i>
          </a>
          <ul class="dropdown-menu" aria-labelledby="dropdownImplanteDental">
            <li><a href="#" data-estado="bueno" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-up buen"></i>Buen Estado</a></li>
            <li><a href="#" data-estado="malo" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-down mal"></i>Mal Estado</a></li>
          </ul>
        </li>
      </ul>
  </div>
  <div class="tab-pane fade" id="l_p" role="tabpanel" aria-labelledby="l_p-tab">
  <ul class="odontograma-navegacion">      
      <li>        
        <a class="btn btn-light dropdown-toggle nombreHallazgo odontograma-item" data-hallazgo="22">
          <img src="{{ asset('assets/images/odontograma/images/imgmacrodoncia.png') }}"> Macrodoncia
        </a>
      </li>
      <li>        
        <a class="btn btn-light dropdown-toggle nombreHallazgo odontograma-item" data-hallazgo="23">
         <img src="{{ asset('assets/images/odontograma/images/imgmicrodoncia.png') }}"> Microdoncia
        </a>
      </li>
      <li class="dropdown">        
        <a class="btn btn-light dropdown-toggle nombreHallazgo" id="dropdownMovilidadPatologica" data-hallazgo="29" data-bs-toggle="dropdown" aria-expanded="false">
         <img src="{{ asset('assets/images/odontograma/images/imgmovilidadpat.png') }}"> Movilidad Patológica <i class="fa fa-caret-down" aria-hidden="true"></i>
        </a>
        <ul class="dropdown-menu" aria-labelledby="dropdownMovilidadPatologica">
          <li><a href="#" class="siglas odontograma-item dropdown-item" data-sigla="M1">M1</a></li>
          <li><a href="#" class="siglas odontograma-item dropdown-item" data-sigla="M2">M2</a></li>
          <li><a href="#" class="siglas odontograma-item dropdown-item" data-sigla="M3">M3</a></li>
          <li><a href="#" class="siglas odontograma-item dropdown-item" data-sigla="M4">M4</a></li>
          <li><a href="#" class="siglas odontograma-item dropdown-item" data-sigla="M5">M5</a></li>
        </ul>
      </li>
      <li>        
        <a class="btn btn-light dropdown-toggle nombreHallazgo odontograma-item" data-hallazgo="9">
          <img src="{{ asset('assets/images/odontograma/images/resized_imgpiezausente.png') }}"> Pieza Dentaria Ausente
        </a>
      </li>
      <li>        
        <a class="btn btn-light dropdown-toggle nombreHallazgo odontograma-item " data-hallazgo="21" data-sigla="E">
          <img src="{{ asset('assets/images/odontograma/images/resized_imgpiezaocto.png') }}"> Pieza Dentaria Ectópica
        </a>
      </li>
      <li>        
        <a class="btn btn-light dropdown-toggle nombreHallazgo odontograma-item" data-hallazgo="20">
          <img src="{{ asset('assets/images/odontograma/images/imgdienteclavija.png') }}"> Pieza Dentaria en Clavija
        </a>
      </li>
      <li>        
        <a class="btn btn-light dropdown-toggle nombreHallazgo odontograma-item" data-hallazgo="10">
          <img src="{{ asset('assets/images/odontograma/images/imgdienterupcion.png') }}"> Pieza Dentaria en Erupción
        </a>
      </li>
      <li>
        <a class="btn btn-light dropdown-toggle nombreHallazgo odontograma-item " data-hallazgo="15">          
          <img src="{{ asset('assets/images/odontograma/images/imgdientextruido.png') }}">Pieza Dentaria  Extruida
        </a>
      </li>
      <li>        
        <a class="btn btn-light dropdown-toggle nombreHallazgo odontograma-item " data-hallazgo="16">
          <img src="{{ asset('assets/images/odontograma/images/imgdientintruido.png') }}"> Pieza Dentaria Intruida
        </a>
      </li>
      <li>        
        <a class="btn btn-light dropdown-toggle nombreHallazgo odontograma-item " data-hallazgo="14">
          <img src="{{ asset('assets/images/odontograma/images/imgpupernumerario.png') }}"> Pieza Dentaria Supernumeraria
        </a>
      </li>
      <li class="dropdown">        
        <a class="btn btn-light dropdown-toggle nombreHallazgo" id="dropdownPosicionDentaria" data-hallazgo="19" data-bs-toggle="dropdown" aria-expanded="false">
          <img src="{{ asset('assets/images/odontograma/images/interrogacion.png') }}"> Posicion Dentaria <i class="fa fa-caret-down" aria-hidden="true"></i>
        </a>
        <ul class="dropdown-menu" aria-labelledby="dropdownPosicionDentaria">
          <li><a href="#" class="siglas odontograma-item dropdown-item" data-sigla="M"><b>M:</b> Mesializado</a></li>
          <li><a href="#" class="siglas odontograma-item dropdown-item" data-sigla="D"><b>D:</b> Distalizado</a></li>
          <li><a href="#" class="siglas odontograma-item dropdown-item" data-sigla="V"><b>V:</b> Vetibularizado</a></li>
          <li><a href="#" class="siglas odontograma-item dropdown-item" data-sigla="P"><b>P:</b> Palatinizado</a></li>
          <li><a href="#" class="siglas odontograma-item dropdown-item" data-sigla="L"><b>L:</b> Lingualizado</a></li>
        </ul>
      </li>
      <li class="dropdown">        
        <a class="btn btn-light dropdown-toggle rango nombreHallazgo" data-hallazgo="32" id="dropdownProtesisFija" data-bs-toggle="dropdown" aria-expanded="false">
          <img src="{{ asset('assets/images/odontograma/images/imgProtesisFija.png') }}"> Prótesis Fija <i class="fa fa-caret-down" aria-hidden="true"></i>
        </a>
        <ul class="dropdown-menu" aria-labelledby="dropdownProtesisFija">
          <li><a href="#" data-estado="bueno" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-up buen"></i>Buen Estado</a></li>
          <li><a href="#" data-estado="malo" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-down mal"></i>Mal Estado</a></li>
        </ul>
      </li>
      <li class="dropdown">        
        <a class="btn btn-light dropdown-toggle rango nombreHallazgo" data-hallazgo="33" id="dropdownProtesisRemovible" data-bs-toggle="dropdown" aria-expanded="false">
          <img src="{{ asset('assets/images/odontograma/images/imgprotesisremovible.png') }}"> Prótesis Removible  <i class="fa fa-caret-down" aria-hidden="true"></i>
        </a>
        <ul class="dropdown-menu" aria-labelledby="dropdownProtesisRemovible">
          <li><a href="#" data-estado="bueno" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-up buen"></i>Buen Estado</a></li>
          <li><a href="#" data-estado="malo" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-down mal"></i>Mal Estado</a></li>
        </ul>
      </li>
      <li class="dropdown">        
        <a class="btn btn-light dropdown-toggle rango nombreHallazgo" data-hallazgo="34" id="dropdownProtesisTotal" data-bs-toggle="dropdown" aria-expanded="false">
           <img src="{{ asset('assets/images/odontograma/images/imgprotesistotal.png') }}"> Prótesis Total <i class="fa fa-caret-down" aria-hidden="true"></i>
        </a>
        <ul class="dropdown-menu" aria-labelledby="dropdownProtesisTotal">
          <li><a href="#" data-estado="bueno" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-up buen"></i>Buen Estado</a></li>
          <li><a href="#" data-estado="malo" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-down mal"></i>Mal Estado</a></li>
        </ul>
      </li>
    </ul>
  </div>
  <div class="tab-pane fade" id="r_t" role="tabpanel" aria-labelledby="r_t-tab">
  <ul class="odontograma-navegacion">
        <li>          
          <a class="btn btn-light dropdown-toggle nombreHallazgo odontograma-item" data-hallazgo="28">
            <img src="{{ asset('assets/images/odontograma/images/imgremanenteradicular.png') }}">Remanente Radicular
          </a>
        </li>
        <li class="dropdown">         
          <a class="btn btn-light dropdown-toggle nombreHallazgo hallazgoMarcar hallazgoMarcarEstado" data-marcaclass="restauracionDefinitiva" id="dropdownPosicionDentaria" data-hallazgo="11" data-bs-toggle="dropdown" aria-expanded="false">
            <img src="{{ asset('assets/images/odontograma/images/imgrestauraciondefinitiva.png') }}"> Restauración Definitiva <i class="fa fa-caret-down" aria-hidden="true"></i>
          </a>
          <ul class="dropdown-menu" aria-labelledby="dropdownPosicionDentaria">
            <li><a href="#" class="siglas odontograma-item dropdown-item" data-sigla="AM"><b>AM:</b> Amalgama Dental</a></li>
            <li><a href="#" class="siglas odontograma-item dropdown-item" data-sigla="R"><b>R:</b> Resina</a></li>
            <li><a href="#" class="siglas odontograma-item dropdown-item" data-sigla="IV"><b>IV:</b> Ionómero de Vidrio</a></li>
            <li><a href="#" class="siglas odontograma-item dropdown-item" data-sigla="IM"><b>IM:</b> Incrutacion Metálica</a></li>
            <li><a href="#" class="siglas odontograma-item dropdown-item" data-sigla="IE"><b>IE:</b> Incrustación Estética</a></li>
            <li><a href="#" class="siglas odontograma-item dropdown-item" data-sigla="C"><b>C:</b> Carilla Estética</a></li>
          </ul>
        </li>
        <li>          
          <a data-marcaclass="restauracionTemporal" class="btn btn-light dropdown-toggle nombreHallazgo hallazgoMarcar odontograma-item " data-hallazgo="12">
            <img src="{{ asset('assets/images/odontograma/images/imgrestauraciontemporal.png') }}">Restauración Temporal
          </a>
        </li>
        <li>          
          <a data-marcaclass="sellantes" class="btn btn-light dropdown-toggle nombreHallazgo hallazgoMarcar hallazgoMarcarEstado odontograma-item" data-hallazgo="6">
            <img src="{{ asset('assets/images/odontograma/images/imgsellantes.png') }}">Sellantes
          </a>
        </li>
        <li>          
          <a class="btn btn-light dropdown-toggle nombreHallazgo odontograma-item" data-hallazgo="27">
            <img src="{{ asset('assets/images/odontograma/images/interrogacion.png') }}">Superficie Desgastada
          </a>
        </li>
        <li>          
          <a class="btn btn-light dropdown-toggle nombreHallazgo odontograma-item" data-hallazgo="36">
            <img src="{{ asset('assets/images/odontograma/images/imgtransposicion.png') }}">Transposición
          </a>
        </li>
        <li class="dropdown">          
          <a class="btn btn-light dropdown-toggle" id="dropdownTratamientoPulpar" data-bs-toggle="dropdown" aria-expanded="false">
            <img src="{{ asset('assets/images/odontograma/images/imgtratamientopulpal.png') }}"> Tratamiento Pulpar<i class="fa fa-caret-down" aria-hidden="true"></i>
          </a>
          <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownTratamientoPulpar">
            <li class="dropdown">
              <a tabindex="-1" id="tratamientopulpar1" class="nombreHallazgo dropdown-item" data-hallazgo="35" data-sigla="TC" href="#" data-bs-toggle="dropdown" aria-expanded="false"><b>TC:</b> Tratamiento de conductos <i class="fa fa-caret-down" aria-hidden="true"></i></a>
              <ul class="dropdown-menu" aria-labelledby="tratamientopulpar1">
                <li><a href="#" data-estado="bueno" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-up buen"></i>Buen Estado</a></li>
                <li><a href="#" data-estado="malo" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-down mal"></i>Mal Estado</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a tabindex="-1" id="tratamientopulpar2" class="nombreHallazgo dropdown-item" data-hallazgo="35" data-sigla="PC"  href="#" data-bs-toggle="dropdown" aria-expanded="false"><b>PC:</b> Pulpectomía <i class="fa fa-caret-down" aria-hidden="true"></i></a>
              <ul class="dropdown-menu" aria-labelledby="tratamientopulpar2">
                <li><a href="#" data-estado="bueno" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-up buen"></i>Buen Estado</a></li>
                <li><a href="#" data-estado="malo" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-down mal"></i>Mal Estado</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a tabindex="-1" id="tratamientopulpar3" class="nombreHallazgo dropdown-item" data-hallazgo="35" data-sigla="PP" href="#" data-bs-toggle="dropdown" aria-expanded="false"><b>PP:</b> Pulpotomía <i class="fa fa-caret-down" aria-hidden="true"></i></a>
              <ul class="dropdown-menu" aria-labelledby="tratamientopulpar3">
                <li><a href="#" data-estado="bueno" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-up buen"></i>Buen Estado</a></li>
                <li><a href="#" data-estado="malo" class="odontograma-item dropdown-item"><i class="fa fa-thumbs-o-down mal"></i>Mal Estado</a></li>
              </ul>
            </li>

          </ul>
        </li>
      </ul>
  </div>
  <div class="tab-pane fade" id="detail" role="tabpanel" aria-labelledby="detail-tab">
    <label class="control-label">Detalle</label>
    <textarea id="TextAreaDetalle" name="detalle" class="form-control" rows="10"></textarea>
    <div class="col-md-12 mt-1">
      <div class="form-group">
        <button type="button" id="GuardarDetalle" class="btn btn-success btn btn-light-success btn btn-light-md">Guardar Detalle</button>
      </div>
    </div>
  </div>
</div>
<div class="mt-5">
  <button id="BotonNombreSeleccionado" class="btn btn-light btn btn-light-flat btn btn-light-block" style="display: none"></button>
  <button id="BotonSeleccion" class="btn btn-link  btn-link-flat btn-link-lg  btn-link-info  btn-link-block">Seleccione</button>
</div>