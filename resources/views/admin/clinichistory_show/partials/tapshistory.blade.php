<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item" role="presentation">
    <button class="nav-link active" id="consultas-tab" data-bs-toggle="tab" data-bs-target="#consultas" type="button" role="tab" aria-controls="consultas" aria-selected="true">Consultas</button>
  </li>
  <li class="nav-item" role="presentation">
    <button class="nav-link" id="anamnesis-tab" data-bs-toggle="tab" data-bs-target="#anamnesis" type="button" role="tab" aria-controls="anamnesis" aria-selected="false">Anamnesis</button>
  </li>
  <li class="nav-item" role="presentation">
    <button class="nav-link" id="extraoral-tab" data-bs-toggle="tab" data-bs-target="#extraoral" type="button" role="tab" aria-controls="extraoral" aria-selected="false">Examen Extra Oral</button>
  </li>
  <li class="nav-item" role="presentation">
    <button class="nav-link" id="examendental-tab" data-bs-toggle="tab" data-bs-target="#examendental" type="button" role="tab" aria-controls="examendental" aria-selected="false">Examen Dental</button>
  </li>
  <li class="nav-item" role="presentation">
    <button class="nav-link" id="odontograma-tab" data-bs-toggle="tab" data-bs-target="#odontograma" type="button" role="tab" aria-controls="odontograma" aria-selected="false">Odontograma</button>
  </li>
</ul>
<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="consultas" role="tabpanel" aria-labelledby="consultas-tab">
    <div class="table-responsive">
      <table class="table datatables" >
        <thead class="table-light">
          <tr >
            <th class="sorting">#</th>                                      
            <th class="sorting">Motivo</th>
            <th class="sorting">Tipo de cita</th>
            <th class="sorting">Fecha</th> 
            <th class="sorting">Hora</th> 
            <th class="sorting">Doctor(a)</th> 
            <th class="sorting">Estado</th> 
          </tr>
        </thead>
        <tbody>
          @foreach ($customer->quotes as $key=>$item)
            <tr class="odd row{{ $item->id }}">
              <td>{{ $item->id }}</td>                                                                               
              <td>{{ $item->reason }}</td>
              <td>@if($item->typequote_id==0)
                  Tele-consulta
                  @else
                    {{ $item->type->name }}
                    @endif
              </td>
              <td>{{ $item->date }}</td>
              <td>{{ $item->start_time }}</td>  
              <td>{{ $item->profesional->name }} {{ $item->profesional->lastname }}</td>                                       
              <td>{{ $item->state->name }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>      
  </div>
  <div class="tab-pane fade" id="anamnesis" role="tabpanel" aria-labelledby="anamnesis-tab">
    @include('admin.clinichistory_show.partials.anamnesis',['anamnesis'=>$anamnesis,'history'=>$history])
  </div>
  <div class="tab-pane fade" id="extraoral" role="tabpanel" aria-labelledby="extraoral-tab">
      @include('admin.clinichistory_show.partials.examenextraoral',['extraoral'=>$extraoral,'history'=>$history])
  </div>
  <div class="tab-pane fade" id="examendental" role="tabpanel" aria-labelledby="examendental-tab">
    @include('admin.clinichistory_show.partials.examdental',['dentalexam'=>$dentalexam,'history'=>$history])
  </div>
  <div class="tab-pane fade" id="odontograma" role="tabpanel" aria-labelledby="odontograma-tab">
      @include('admin.clinichistory_show.partials.odontograma',['extraoral'=>$extraoral])
  </div>
</div>






