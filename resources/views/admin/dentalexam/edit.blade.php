@extends('layouts.admin')

@section('title', 'Examen Dental')
@section('page_title', 'Examen Dental')
@section('page_subtitle', 'Editar')
@section('content')


<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-start mb-0">Editar: {{ $dentalexam->id }}</h2>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="/home">Inicio &nbsp; &nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="/clinic-history">Historias Clinicas &nbsp; &nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                        </li>
                        <li class="breadcrumb-item active">Editar: {{ $dentalexam->id }}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    <section id="multiple-column-form">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Editar Examen Dental</h4>
                    </div>
                    <div class="card-body">
                    <form class="form" role="form" id="main-form-dentalexam" autocomplete="off">
                        <input type="hidden" id="_url" value="{{ url('dentalexam',[$dentalexam->encode_id]) }}">
                        <input type="hidden" id="_token" value="{{ csrf_token() }}">
                            <div class="row">
                            <table class="table">
                                <thead>
                                    <tr>
                                    <th scope="col">EXAMEN DENTAL</th>
                                    <th scope="col">Si</th>
                                    <th scope="col">No</th>
                                    <th scope="col">EXAMEN PERIODENTAL</th>
                                    <th scope="col">Si</th>
                                    <th scope="col">No</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">Supernumerarios</th>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="supernumerarios" id="questionsi1" value="Si" {{ ($dentalexam->supernumerarios=="Si")? "checked" : "" }}>
                                                <label class="form-check-label" for="questionsi1"></label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="supernumerarios" id="questionno1" value="No" {{ ($dentalexam->apreciacion_paciente=="No")? "checked" : "" }}>
                                                <label class="form-check-label" for="questionno1"></label>
                                            </div>
                                        </td>
                                        <th  scope="row">Placa Blanda</th >
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="placa_blanda" id="questionsi2" value="Si" {{ ($dentalexam->placa_blanda=="Si")? "checked" : "" }}>
                                                <label class="form-check-label" for="questionsi1"></label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="placa_blanda" id="questionsi2" value="No" {{ ($dentalexam->placa_blanda=="No")? "checked" : "" }}>
                                                <label class="form-check-label" for="questionsi1"></label>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th scope="row">Abrasiones</th>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="abrasiones" id="questionsi3" value="Si" {{ ($dentalexam->abrasiones=="Si")? "checked" : "" }}>
                                                <label class="form-check-label" for="questionsi2"></label>
                                            </div>                      
                                        </td>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="abrasiones" id="questionno3" value="No" {{ ($dentalexam->abrasiones=="No")? "checked" : "" }}>
                                                <label class="form-check-label" for="questionno2"></label>
                                            </div>
                                        </td>
                                        <th  scope="row">Placa Calcificada</th >
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="placa_calcificada" id="placa_calcificadasi" value="Si" {{ ($dentalexam->placa_calcificada=="Si")? "checked" : "" }}>
                                                <label class="form-check-label" for="placa_calcificadasi"></label>
                                            </div> 
                                        </td>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="placa_calcificada" id="placa_calcificadano" value="No" {{ ($dentalexam->placa_calcificada=="No")? "checked" : "" }}>
                                                <label class="form-check-label" for="enfermedad_venereano"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Erosiones</th>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="erosiones" value="Si" {{ ($dentalexam->erosiones=="Si")? "checked" : "" }}>
                                            </div>                       
                                        </td>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="erosiones"  value="No" {{ ($dentalexam->erosiones=="No")? "checked" : "" }}>
                                            </div>
                                        </td>
                                        <th scope="row">Movilidad Dental</th>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="movilidad_dental"  value="Si" {{ ($dentalexam->movilidad_dental=="Si")? "checked" : "" }}>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="movilidad_dental"  value="No" {{ ($dentalexam->movilidad_dental=="No")? "checked" : "" }}>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Manchas</th>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="manchas"  value="Si" {{ ($dentalexam->manchas=="Si")? "checked" : "" }}>
                                            </div>                      
                                        </td>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="manchas"  value="No" {{ ($dentalexam->manchas=="No")? "checked" : "" }}>
                                            </div>
                                        </td>
                                        <th scope="row">Bolsas Periodontales</th>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="bolsas_periodontales" id="bolsas_periodontalessi" value="Si" {{ ($dentalexam->bolsas_periodontales=="Si")? "checked" : "" }}>
                                            </div>  
                                        </td>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="bolsas_periodontales" id="bolsas_periodontalesno" value="No" {{ ($dentalexam->bolsas_periodontales=="No")? "checked" : "" }}>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Patología Pulpar</th>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="patalogia_pulpar" id="questionsi5" value="Si" {{ ($dentalexam->patalogia_pulpar=="Si")? "checked" : "" }}>
                                            </div>                       
                                        </td>
                                        <td> 
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="patalogia_pulpar" id="questionno5" value="No" {{ ($dentalexam->patalogia_pulpar=="No")? "checked" : "" }} >
                                            </div>
                                        </td>
                                        <th scope="row">Retracción Gingival</th>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="retraccion_gingival" id="fiebrereumaticasi" value="Si" {{ ($dentalexam->retraccion_gingival=="Si")? "checked" : "" }}>
                                            </div>   
                                        </td>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="retraccion_gingival" id="fiebrereumaticano" value="No" {{ ($dentalexam->retraccion_gingival=="No")? "checked" : "" }}>
                                            </div>
                                        </td>
                                    </tr>
                                
                                
                                </tbody>
                            </table>         
                            <div class="col-md-12 col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="last_name">Observaciones</label>
                                    <textarea  id="observation" name="observation" class="form-control"  >{{ $dentalexam->observation }}</textarea>
                                    <span class="missing_alert text-danger" id="last_name_alert"></span>
                                </div>
                            </div>
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary me-1 waves-effect waves-float waves-light ajax" id="submit"><i id="ajax-icon" class="fa fa-save"></i> Guardar</button>
                            </div>
                        </div>
                    </form>

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection
@push('scripts')

    <script src="{{ asset('js/admin/dentalexam/edit_form.js') }}"></script>
@endpush
