@extends('layouts.admin')

@section('title', 'Examen Extra Oral')
@section('page_title', 'Examen Extra Oral')
@section('page_subtitle', 'Editar')
@section('content')


<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-start mb-0">Editar: {{ $extraoral->id }}</h2>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="/home">Inicio &nbsp; &nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="/clinic-history">Historias Clinicas &nbsp; &nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                        </li>
                        <li class="breadcrumb-item active">Editar: {{ $extraoral->id }}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    <section id="multiple-column-form">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Editar Examen extra oral</h4>
                    </div>
                    <div class="card-body">
                    <form class="form" role="form" id="main-form-extraoral" autocomplete="off">
    <input type="hidden" id="_url" value="{{ url('extraoralexam',[$extraoral->encode_id]) }}">
    <input type="hidden" id="_token" value="{{ csrf_token() }}">
        <div class="row">
        <table class="table">
            <thead>
                <tr>
                <th scope="col">EXAMEN EXTRAORAL</th>
                <th scope="col">Normal</th>
                <th scope="col">Anormal</th>
                <th scope="col">EXAMEN INTRAORAL</th>
                <th scope="col">Normal</th>
                <th scope="col">Anormal</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">Apreciación generalizada del paciente</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="apreciacion_paciente" id="questionsi1" value="Normal" {{ ($extraoral->apreciacion_paciente=="Normal")? "checked" : "" }}>
                            <label class="form-check-label" for="questionsi1"></label>
                        </div>
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="apreciacion_paciente" id="questionno1" value="Anormal" {{ ($extraoral->apreciacion_paciente=="Anormal")? "checked" : "" }}>
                            <label class="form-check-label" for="questionno1"></label>
                        </div>
                    </td>
                    <th  scope="row">Labios y comisura labial</th >
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="labios_comisura_labial" id="questionsi2" value="Normal" {{ ($extraoral->labios_comisura_labial=="Normal")? "checked" : "" }}>
                            <label class="form-check-label" for="questionsi1"></label>
                        </div>
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="labios_comisura_labial" id="questionsi2" value="Anormal" {{ ($extraoral->labios_comisura_labial=="Anormal")? "checked" : "" }}>
                            <label class="form-check-label" for="questionsi1"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <th scope="row">Cabeza</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="cabeza" id="questionsi3" value="Normal" {{ ($extraoral->cabeza=="Normal")? "checked" : "" }}>
                            <label class="form-check-label" for="questionsi2"></label>
                        </div>                      
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="cabeza" id="questionno3" value="Anormal" {{ ($extraoral->cabeza=="Anormal")? "checked" : "" }}>
                            <label class="form-check-label" for="questionno2"></label>
                        </div>
                    </td>
                    <th  scope="row">Lengua</th >
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="lengua" id="lenguasi" value="Normal" {{ ($extraoral->lengua=="Normal")? "checked" : "" }}>
                            <label class="form-check-label" for="lenguasi"></label>
                        </div> 
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="lengua" id="lenguano" value="Anormal" {{ ($extraoral->lengua=="Anormal")? "checked" : "" }}>
                            <label class="form-check-label" for="enfermedad_venereano"></label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Cara</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="cara" value="Normal" {{ ($extraoral->cara=="Normal")? "checked" : "" }}>
                        </div>                       
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="cara"  value="Anormal" {{ ($extraoral->cara=="Anormal")? "checked" : "" }}>
                        </div>
                    </td>
                    <th scope="row">Carrillos</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="carrillos"  value="Normal" {{ ($extraoral->carrillos=="Normal")? "checked" : "" }}>
                        </div>
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="carrillos"  value="Anormal" {{ ($extraoral->carrillos=="Anormal")? "checked" : "" }}>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Glándulas salivales (Parotida,submaxilar,sublingual)</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="glandulas_salivales"  value="Normal" {{ ($extraoral->glandulas_salivales=="Normal")? "checked" : "" }}>
                        </div>                      
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="glandulas_salivales"  value="Anormal" {{ ($extraoral->glandulas_salivales=="Anormal")? "checked" : "" }}>
                        </div>
                    </td>
                    <th scope="row">Frenillos</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="frenillos" id="frenillossi" value="Normal" {{ ($extraoral->frenillos=="Normal")? "checked" : "" }}>
                        </div>  
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="frenillos" id="frenillosno" value="Anormal" {{ ($extraoral->frenillos=="Anormal")? "checked" : "" }}>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Cuello: Renglón hiodea y tiroidea</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="cuello" id="questionsi5" value="Normal" {{ ($extraoral->cuello=="Normal")? "checked" : "" }}>
                        </div>                       
                    </td>
                    <td> 
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="cuello" id="questionno5" value="Anormal" {{ ($extraoral->cuello=="Anormal")? "checked" : "" }} >
                        </div>
                    </td>
                    <th scope="row">Piso de boca</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="piso_boca" id="fiebrereumaticasi" value="Normal" {{ ($extraoral->piso_boca=="Normal")? "checked" : "" }}>
                        </div>   
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="piso_boca" id="fiebrereumaticano" value="Anormal" {{ ($extraoral->piso_boca=="Anormal")? "checked" : "" }}>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Cadena ganglionar (auricular posterios,preauricular submandibular,cervical anterior, cervical posterior)</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="cadena_danglionar"  value="Normal" {{ ($extraoral->cadena_danglionar=="Normal")? "checked" : "" }}>
                        </div> 
                    </td>
                    <td>
                    <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="cadena_danglionar"  value="Anormal" {{ ($extraoral->cadena_danglionar=="Anormal")? "checked" : "" }}>
                        </div> 
                    </td>
                    <th scope="row">Paladar duro</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="paladar_duro" id="paladar_durosi" value="Normal" {{ ($extraoral->paladar_duro=="Normal")? "checked" : "" }}>
                        </div> 
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="paladar_duro" id="paladar_durono" value="Anormal" {{ ($extraoral->paladar_duro=="Anormal")? "checked" : "" }}>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">ATM</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="atm" id="questionsi6" value="Normal" {{ ($extraoral->atm=="Normal")? "checked" : "" }}>
                        </div>                        
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="atm" id="questionno6" value="Anormal" {{ ($extraoral->atm=="Anormal")? "checked" : "" }}>
                        </div>
                    </td>
                    <th scope="row">Paladar blando</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="paladar_blando"  value="Normal" {{ ($extraoral->paladar_blando=="Normal")? "checked" : "" }}>
                        </div>   
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="paladar_blando"  value="Anormal" {{ ($extraoral->paladar_blando=="Anormal")? "checked" : "" }}>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row"></th>
                    <td>                       
                    </td>
                    <td>
                    </td>
                    <th scope="row">Región retromolar</th>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="region_retromolar" value="Normal" {{ ($extraoral->region_retromolar=="Normal")? "checked" : "" }} >
                        </div>
                    </td>
                    <td>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="region_retromolar"  value="Anormal" {{ ($extraoral->region_retromolar=="Anormal")? "checked" : "" }}>
                        </div>
                    </td>
                </tr>
             
               
            </tbody>
        </table>         

        <div class="col-12">
            <button type="submit" class="btn btn-primary me-1 waves-effect waves-float waves-light ajax" id="submit"><i id="ajax-icon" class="fa fa-save"></i> Guardar</button>
        </div>
    </div>
</form>

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection
@push('scripts')

    <script src="{{ asset('js/admin/extraoral/edit_form.js') }}"></script>
@endpush
