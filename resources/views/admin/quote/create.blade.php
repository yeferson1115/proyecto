@extends('layouts.admin')

@section('title', 'Agendar Cita')
@section('page_title', 'Agendar Cita')
@section('page_subtitle', 'Guardar')
@section('content')

<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-start mb-0">Nueva Cita</h2>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="/home">Inicio &nbsp; &nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="/quote">Citas Agendadas &nbsp; &nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                        </li>
                        <li class="breadcrumb-item active">Nueva Cita</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    <section id="multiple-column-form">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Crear Cita</h4>
                    </div>
                    <div class="card-body">
                        <form class="form" role="form" action="javascript:void(0)" id="main-form" autocomplete="off">
                            <input type="hidden" id="_url" value="{{ url('quote') }}">
                            <input type="hidden" id="_token" value="{{ csrf_token() }}">
                            <input type="hidden" id="_url_quote" value="{{ url('quote/pofesional') }}">
                            <div class="row">
                                <div class="col-md-3 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="name">Tipo de Cita</label>
                                        <select class="invoiceto1 form-select profesional" id="typequote_id" name="typequote_id">
                                            <option value="">Seleccione</option>
                                            @foreach ($types as $type)
                                                <option value="{{ $type->id }}">{{ $type->name }}</option>
                                            @endforeach
                                        </select>
                                        <span class="missing_alert text-danger" id="typequote_id_alert"></span>
                                    </div>
                                </div>
                                <div class="col-md-4 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="name">Profesional</label>
                                        <select class="invoiceto1 form-select profesional" id="user_id" name="user_id">
                                            <option value="">Seleccione</option>
                                            @foreach ($usersprofesional as $user)
                                                <option value="{{ $user->id }}">{{ $user->name }} {{ $user->lastname }}</option>
                                            @endforeach
                                        </select>
                                        <span class="missing_alert text-danger" id="user_id_alert"></span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="date">Fecha</label>
                                        <input type="date" id="date" name="date" class="form-control" placeholder="Fecha">
                                        <span class="missing_alert text-danger" id="date_alert"></span>
                                    </div>
                                </div>
                                <div class="col-md-2 col-2">
                                    <a  class="mt-2 btn btn-primary me-1 waves-effect waves-float waves-light serarquote" id="submit"><i id="ajax-icon" class="fa fa-save"></i> consultar</a>
                                </div>
                                <div class="col-md-12 col-12">
                                    <h4 class="card-title">Horarios Disponibles</h4>
                                    <div class="mb-1 row" style="margin-left: 15px;margin-right: 15px;" id="shedule">
                                    <span class="missing_alert text-danger" id="schedule_alert"></span>
                                </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="name">Cliente</label>
                                        <select class="invoiceto1 form-select customer" id="customer_id" name="customer_id">
                                            <option value="">Seleccione</option>
                                            @foreach ($customers as $customer)
                                                <option value="{{ $customer->id }}">{{ $customer->first_name }} {{ $customer->last_name }} - {{ $customer->identification }}</option>
                                            @endforeach
                                        </select>
                                        <span class="missing_alert text-danger" id="customer_id_alert"></span>
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="name">Estado</label>
                                        <select class="invoiceto1 form-select customer" id="state_id" name="state_id">
                                            <option value="">Seleccione</option>
                                            @foreach ($statequote as $state)
                                                <option value="{{ $state->id }}">{{ $state->name }}</option>
                                            @endforeach
                                        </select>
                                        <span class="missing_alert text-danger" id="state_id_alert"></span>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="reason">Motivo</label>
                                        <textarea class="form-control" name="reason"></textarea>
                                        <span class="missing_alert text-danger" id="reason_alert"></span>
                                    </div>
                                </div>


                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary me-1 waves-effect waves-float waves-light ajax" id="submit"><i id="ajax-icon" class="fa fa-save"></i> Guardar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection

@push('scripts')

    <script src="{{ asset('js/admin/quote/create.js') }}"></script>
@endpush
