@extends('layouts.admin')
@section('title','Citas Agendadas')
@section('page_title', 'Listado de Citas Agendadas')
@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-start mb-0">Citas Agendadas</h2>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="/home">Inicio &nbsp; &nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                        </li>
                        <li class="breadcrumb-item active">Citas Agendadas</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    @can('Crear Cita')
    <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
        <div class="mb-1 breadcrumb-right">
            <div class="dropdown">
                <a href="{{ url('quote/create') }}" class="btn btn-success waves-effect waves-float waves-light"><i class="fa fa-stethoscope" aria-hidden="true"></i> Nueva Cita</a>

            </div>
        </div>
    </div>
    @endcan
</div>

<div class="content-body">
    <section id="multiple-column-form">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Citas</h4>
                    </div>
                    <div class="card-body">
                    <div class="table-responsive">
                            <div id='calendar'></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        
    </section>
</div>


@endsection

@push('scripts')
<script>
    $(document).ready(function() {
        // page is now ready, initialize the calendar...
        $('#calendar').fullCalendar({
            locale: 'es',
            // put your options and callbacks here
            defaultView: 'agendaWeek',
            
            events : [
                @foreach($quote as $item)
                {
                    title : '{{ $item->customer->first_name . ' ' .  $item->customer->last_name }}',
                    start : '{{ $item->date.' '.$item->start_time }}',
                    end: '{{ $item->date.' '.$item->end_time }}',
                    id:'{{ $item->encode_id}}',
                    color:'{{ $item->state->color }}',
                   
                },
                @endforeach
                
            ],
            
            eventClick: function(calEvent, jsEvent, view) {
                $('#start_time').val(moment(calEvent.start).format('YYYY-MM-DD HH:mm:ss'));
                $('#finish_time').val(moment(calEvent.end).format('YYYY-MM-DD HH:mm:ss'));
                window.location.href='/clinic-history/customer/'+calEvent.id;
                //$('#editModal').modal();
            }
        });
    });
    $('.delete-user').click(function(e){

        e.preventDefault();
        var _target=e.target;
        let href = $(this).attr('data-attr');// Don't post the form, unless confirmed
        let token = $(this).attr('data-token');
        var data=$(e.target).closest('form').serialize();
        Swal.fire({
        title: 'Seguro que desea eliminar la cita?',
        text: "",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar',
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
              url: href,
              headers: {'X-CSRF-TOKEN': token},
              type: 'DELETE',
              cache: false,
    	      data: data,
              success: function (response) {
                var json = $.parseJSON(response);
                console.log(json);
                Swal.fire(
                    'Muy bien!',
                    'cita eliminada correctamente',
                    'success'
                    ).then((result) => {
                        location.reload();
                    });

              },error: function (data) {
                var errors = data.responseJSON;
                console.log(errors);

              }
           });

        }
        })

    });
</script>

@endpush


