@extends('layouts.admin')
@section('title','Tele-consultas')
@section('page_title', 'Listado de tele-consultas')
@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-start mb-0">Tele-consultas</h2>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="/home">Inicio &nbsp; &nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                        </li>
                        <li class="breadcrumb-item active">Tele-consultas</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
        <div class="mb-1 breadcrumb-right">
            <div class="dropdown">
                <a href="{{ url('tele-consultation/create') }}" class="btn btn-success waves-effect waves-float waves-light"><i data-feather='user-plus'></i> Nueva teleconsulta</a>

            </div>
        </div>
    </div>
</div>

<div class="content-body">
    <section id="multiple-column-form">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Tele-consultas</h4>
                    </div>
                    <div class="card-body">
                    <div class="table-responsive">
                            <table class="table" id="datatables" >
                                <thead class="table-light">
                                    <tr >
                                        <th class="sorting" >Opciones</th>
                                        <th class="sorting"  >#</th>
                                        <th class="sorting" >Estado Cita</th>
                                        <th class="sorting" >Fecha</th>
                                        <th class="sorting" >Hora</th>
                                        <th class="sorting" >Médico</th>
                                        <th class="sorting" >Ir a Cita</th>

                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($quotes as $item)
                                    <tr class="odd row{{ $item->id }}">
                                        <td>                                            
                                            <form method="POST" action="">
                                                <div class="form-group">
                                                    <button type="submit" data-token="{{ csrf_token() }}" data-attr="{{ url('services',[$item->encode_id]) }}" class="btn btn-danger waves-effect waves-float waves-light delete-user" value="Delete user"><i data-feather='trash-2'></i></button>
                                                </div>
                                            </form>
                                        </td>

                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->state->name }}</td>
                                        <td>{{ $item->date }}</td>
                                        <td>{{ $item->start_time }}</td> 
                                        <td>{{ $item->user->lastname }} {{ $item->user->username }}</td>
                                        <td>@if($item->payment==1)
                                            <a  class="mb-1 btn btn-info waves-effect waves-float waves-light" href="{{  route('show', ['meet' => $item->encode_id])  }}" title="Editar"><i class="fa fa-video-camera" aria-hidden="true"></i> Cita </a>
                                            @else
                                            <a  class="mb-1 btn btn-danger waves-effect waves-float waves-light payment" onclick="pay({{ $item->id }},'{{$item->customer->email}}','{{$item->customer->first_name}} {{$item->customer->last_name}}','{{$item->customer->phone}}','{{$item->customer->identification}}',{{$item->customer->id}},'{{ csrf_token() }}');" title="Editar"><i class="fa fa-usd" aria-hidden="true"></i> Pagar </a>
                                            @endif
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection

@push('scripts')
<script type="text/javascript" src="https://checkout.wompi.co/widget.js"></script>

<script>
    function pay(id,email,fullname,phone,document,customerid,token){
        var checkout = new WidgetCheckout({
            currency: 'COP',
            amountInCents: 2490000,
            reference: 'teledfssssdf',
            publicKey: 'pub_test_WlBj8hnbbFcHbGZEVKVWkPkDBpWsVOoH',
            //redirectUrl: 'https://transaction-redirect.wompi.co/check', // Opcional
            taxInCents: { // Opcional
                vat: 1900,
                consumption: 800
            },
            customerData: { // Opcional
                email:email,
                fullName: fullname,
                phoneNumber: phone,
                phoneNumberPrefix: '+57',
                legalId: document,
                legalIdType: 'CC'
            }
            });
            checkout.open(function ( result ) {
            var transaction = result.transaction
            console.log('Transaction ID: ', transaction.id)
            console.log('Transaction object: ', transaction)
            if(transaction.status=='APPROVED'){
                $.ajax({
                    url: 'checkoutpayment',
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'POST',
                    cache: false,
                    data: {'id':id,'status':transaction.status,
                        'paymentMethodType':transaction.paymentMethodType,
                        'reference':transaction.reference,
                        'transactionid':transaction.id,
                        'customerid':customerid
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        console.log(json);
                        Swal.fire(
                            'Muy bien!',
                            'Servicio eliminado correctamente',
                            'success'
                            ).then((result) => {
                                location.reload();
                            });

                    },error: function (data) {
                        var errors = data.responseJSON;
                        console.log(errors);

                    }
                });
            }
            

            })
    };

    $('.delete-user').click(function(e){

        e.preventDefault();
        var _target=e.target;
        let href = $(this).attr('data-attr');// Don't post the form, unless confirmed
        let token = $(this).attr('data-token');
        var data=$(e.target).closest('form').serialize();
        Swal.fire({
        title: 'Seguro que desea eliminar el servicio?',
        text: "",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar',
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
              url: href,
              headers: {'X-CSRF-TOKEN': token},
              type: 'DELETE',
              cache: false,
    	      data: data,
              success: function (response) {
                var json = $.parseJSON(response);
                console.log(json);
                Swal.fire(
                    'Muy bien!',
                    'Servicio eliminado correctamente',
                    'success'
                    ).then((result) => {
                        location.reload();
                    });

              },error: function (data) {
                var errors = data.responseJSON;
                console.log(errors);

              }
           });

        }
        })

    });
</script>

@endpush


