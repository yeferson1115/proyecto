@extends('layouts.admin')

@section('title', 'Usuarios')
@section('page_title', 'Usuarios')
@section('page_subtitle', 'Editar')
@section('content')


<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-start mb-0">Tele-consulta</h2>
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="/home">Inicio &nbsp; &nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="/tele-consultation">Tele-consultas &nbsp; &nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                        </li>
                        <li class="breadcrumb-item active">Tele-consulta</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    <section id="multiple-column-form">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Tele-consulta</h4>
                    </div>
                    <div class="card-body">
                        <div id="meet"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection
@push('scripts')

<script src='https://meet.jit.si/external_api.js'></script>
<script>
   const domain = 'meet.jit.si';
    const options = {
        roomName: '{{$quote->meet_id}}',
        height: 700,
        parentNode: document.querySelector('#meet'),
        lang: 'es',
        userInfo: {
            email: '{{$quote->customer->email}}',
            displayName: '{{$quote->customer->first_name}} {{$quote->customer->last_name}}'
        }
    }; 
    const api = new JitsiMeetExternalAPI(domain, options);
</script>
@endpush
