<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8" />
        <title>Proyecto TdeA - @yield('title')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
        <meta name="keywords" content="bootstrap 4, premium, marketing, multipurpose" />
        <meta content="Themesdesign" name="author" />
        <!-- favicon -->
        <link rel="shortcut icon" href="{{ asset('app-assets/images/favicon.ico') }}" />
        <!-- css -->
        <link href="{{ asset('app-assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('app-assets/css/materialdesignicons.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- magnific pop-up -->
        <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/magnific-popup.css') }}" />
        <!-- magnific pop-up -->
        <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/ion.rangeSlider.min.css') }}" />
        <!-- Pe-icon-7 icon -->
        <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pe-icon-7-stroke.css') }}" />
        <!-- Swiper CSS -->
        <link rel="stylesheet" href="{{ asset('app-assets/css/swiper.min.css') }}" />
        <link href="{{ asset('app-assets/css/style.css') }}" rel="stylesheet" type="text/css" />
    </head>

    <body>
        <!--Navbar Start-->
        <nav  style="background: #fff;" class="navbar navbar-expand-lg fixed-top navbar-custom sticky">
            @include('layouts.partials.appmenu')
        </nav>
        <!-- Navbar End -->

        @yield('content')




        <!-- START FOOTER -->
        <section class="section bg-light pb-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="footer-info mt-4">
                            <img src="images/logo-dark.png" alt="" height="22" />
                            <h5 class="f-18 mt-4 pt-1 line-height_1_6">
                                Lorem Ipson <br />
                                Lorem Ipson <br />
                                Lorem Ipson...
                            </h5>
                        </div>
                    </div>

                    <div class="col-lg-5">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="mt-4">
                                    <ul class="list-unstyled footer-link mt-3">
                                        <li><a href="">Inicio</a></li>
                                        <li><a href="">Quines Somos</a></li>
                                        <li><a href="">Teleconferencia</a></li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="mt-4 pl-0 pl-lg-4">
                            <h5 class="f-18">Seguenos</h5>
                            <ul class="list-inline social-icons-list pt-3">
                                <li class="list-inline-item">
                                    <a href="#"><i class="mdi mdi-facebook"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#"><i class="mdi mdi-twitter"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#"><i class="mdi mdi-linkedin"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#"><i class="mdi mdi-google-plus"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <hr class="my-5" />

                <div class="row">
                    <div class="col-12">
                        <div class="text-center">
                            <p class="text-muted mb-0">2021 © . </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END FOOTER -->

        <!-- javascript -->
        <script src="{{ asset('app-assets/js/jquery.min.js') }}"></script>
        <script src="{{ asset('app-assets/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('app-assets/js/jquery.easing.min.js') }}"></script>
        <script src="{{ asset('app-assets/js/scrollspy.min.js') }}"></script>

        <!-- Portfolio -->
        <script src="{{ asset('app-assets/js/ion.rangeSlider.min.js') }}"></script>

        <!-- Portfolio -->
        <script src="{{ asset('app-assets/js/jquery.magnific-popup.min.js') }}"></script>
        <script src="{{ asset('app-assets/js/isotope.js') }}"></script>

        <!-- counter init -->
        <script src="{{ asset('app-assets/js/counter.init.js') }}"></script>

        <!-- Swiper JS -->
        <script src="{{ asset('app-assets/js/swiper.min.js') }}"></script>

        <!--flex slider plugin-->
        <script src="{{ asset('app-assets/js/jquery.flexslider-min.js') }}"></script>

        <!-- yt player -->
        <script src="{{ asset('app-assets/js/jquery.mb.YTPlayer.js') }}"></script>

        <!-- contact init -->
        <script src="{{ asset('app-assets/js/contact.init.js') }}"></script>

        <!-- Main Js -->
        <script src="{{ asset('app-assets/js/app.js') }}"></script>

        <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script>
        <script src="{{ asset('app-assets/vendors/js/forms/repeater/jquery.repeater.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/jquery.validate.min.js') }}"></script>

        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script>
    
        function _alertGeneric(type,title,text,reload=null){
            Swal.fire({
                //icon: type,
                icon: type,
                title: title,
                text: text,
                confirmButtonText:'Aceptar'
            }).then((result) => {
                //$('#page-loader').fadeOut(100);
                if(reload!='' && reload!=null && reload!=1){
                window.location.href = reload;
                }
                if(reload===1){
                location.reload();
                }
                if(reload===2){
                    window.history.go(-1);
                }
            });
        }
function _ConfirmFactura(url,message='Servicio Facturado Correctamente'){
    Swal.fire({
      title: 'Muy bien!',
      icon: 'success',
      text: message,
      showCancelButton: true,
      confirmButtonClass: 'btn btn-success btn-fill',
      cancelButtonClass: 'btn btn-danger btn-fill',
      confirmButtonText: 'Imprimir factura',
      cancelButtonText: 'Continuar sin factura',
      buttonsStyling: false
      }).then(function(e) {
        window.open(url, '_blank');
        location.reload();

      }).catch(function(e) {
        location.reload();

      });

  }

  function _alerturlexterna(url,message='Servicio Facturado Correctamente'){
    Swal.fire({
      title: 'Muy bien!',
      icon: 'success',
      text: message,
      showCancelButton: true,
      confirmButtonClass: 'btn btn-success btn-fill',
      cancelButtonClass: 'btn btn-danger btn-fill',
      confirmButtonText: 'Enviar por WhatsApp',
      cancelButtonText: 'Continuar sin enviar',
      buttonsStyling: false
      }).then(function(e) {
        window.open(url, '_blank');
        location.reload();

      }).catch(function(e) {
        location.reload();

      });

  }  


</script>

    @stack('scripts')
    </body>
</html>
