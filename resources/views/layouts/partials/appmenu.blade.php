<div class="container">
    <!-- LOGO -->
    <a class="navbar-brand logo text-uppercase" href="/">
        <img src="{{ asset('images/logo/logo-tdea.jpg') }}"  alt="" height="45" />
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <i class="mdi mdi-menu"></i>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">

        <ul class="navbar-nav ml-auto navbar-center" id="mySidenav">
            <li class="nav-item active">
                <a href="/" class="nav-link">Inicio</a>
            </li>            
            <li class="nav-item">
                <a href="/home" class="nav-link">Teleconsulta</a>
            </li>
            <li class="nav-item">
                <a href="/registro" class="nav-link">Crear Cuenta</a>
            </li>

        </ul>
        <div class="navbar-button d-none d-lg-inline-block">
            <a href="/home" class="btn btn-sm btn-primary btn-round">Ingresar</a>
        </div>
    </div>
</div>
